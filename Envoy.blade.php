@servers(['web' => 'administrador@mpsolucoes.com.br'])

@setup
	$repo = 'git@bitbucket.org:bsardinha/rhsystem.git';
	$release_dir = '/home/administrador/apps/seronline/deploy/';
	$app_dir = '/home/administrador/apps/seronline/';
	$release = 'release_'.date('YmdHis');
@endsetup

@story('deploy')
	fetch_repo
	run_composer
	copy_env
	migrations
    update_chmod_storage
	update_symlinks
	{{-- final_setup --}}
@endstory

@task('fetch_repo')
	[ -d {{ $release_dir }} ] || mkdir {{ $release_dir }};
	cd {{ $release_dir }};
	git clone {{ $repo }} {{ $release }};
@endtask

@task('run_composer')
	cd {{ $release_dir }}/{{ $release }};
	composer install --prefer-dist;
@endtask

@task('copy_env')
	cd {{ $app_dir }};
	cp -f env {{ $release_dir}}/{{ $release }}/.env;
@endtask

@task('migrations')
	cd {{ $release_dir }}/{{ $release }};
	php artisan migrate --force;
	php artisan db:seed --force;
@endtask

@task('update_chmod_storage')
	cd {{ $release_dir }}/{{ $release }};
	chmod 777 -R -f storage;
@endtask

@task('update_symlinks')
	ln -nfs {{ $release_dir }}/{{ $release }} {{ $release_dir }}/current;
@endtask

@task('final_setup')
	cd {{ $release_dir }}/{{ $release }};
	php artisan passport:keys;
	php artisan storage:link;
@endtask
