<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cobranca extends Model
{
    protected $table = 'cobrancas';

    public function cliente(){
        return $this->hasOne('App\Models\Empresa', 'asaas_id', 'asaas_id');
    }
}
