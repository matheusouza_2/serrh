<?php

namespace App\Http\Controllers;

use Auth;
use Carbon\Carbon;

use App\User;

use App\Models\Actuation;
use App\Models\Curriculo;
use App\Models\Historicocargo;

use App\Models\Configuration;

use Illuminate\Http\Request;

class ActuationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('cadastro.actuation.index');
    }

    public function filtrar(Request $request){
        $descricao = $request->input('name');

        $unioesComuns = ["/\sd\w\s+/i"];
        $actuationToCompare = preg_replace('/\s+/','%',preg_replace($unioesComuns," ",$descricao));

        $config =Configuration::first();
        $paginationNumber = isset($config) ? Configuration::first()->pagination : 20;


        $actuations = Actuation::whereRaw("upper(descricao) like upper('%".$actuationToCompare."%')")->orderBy("descricao")->paginate($paginationNumber);
        return view('cadastro.actuation._list', compact('actuations'));
    }

    public function quickModal($id){
        if($id == "new"){
            $actuation = new Actuation;
        } else {
            $actuation = Actuation::find($id);
            if($actuation == null){
                $actuation = new Actuation;
            }
        }
        return view('cadastro.actuation.quickModal', compact('actuation'));
    }

    public function viability(Request $request){
        $actuation = $request->input('actuation');
        $prev = $request->input('lastValue');

        $comparation = $this->viabilityGeneral($actuation, $prev);

        return $comparation;
    }

    public function salvar(Request $request){
        $actuation = $request->input('actuation');
        $actuationId = $request->input('id');

        $unioesComuns = ["/\sd\w\s+/i"];
        $identificador = ucwords($actuation);
        $identificador = preg_replace('/\s+/','',preg_replace($unioesComuns," ",$identificador));

        $newActuation = Actuation::find($actuationId);

        if($newActuation == null){
            $newActuation = new Actuation;
            $newActuation->created_at = Carbon::now();
        }

        $newActuation->identificador = $identificador;
        $newActuation->descricao = $actuation;
        $newActuation->updated_at = Carbon::now();

        if(Auth::user()->perfil->identificador == "Candidato" || Auth::user()->perfil->identificador == "Empresario"){
            $newActuation->aprovada = false;
        }
        $newActuation->save();

        return $newActuation->toJson();
    }

    private function viabilityGeneral($actuation, $id){
        $unioesComuns = ["/\sd\w\s+/i"];
        $actuationToCompare = preg_replace('/\s+/','%',preg_replace($unioesComuns," ",$actuation));

        $comparation = Actuation::whereRaw("upper(descricao) like upper('%".$actuationToCompare."%')")->where("id","<>",$id)->get()->toJson();

        return $comparation;
    }

    public function changeStatus($id){
        $actuation = Actuation::find($id);
        if($actuation->aprovada == 0){
            $actuation->aprovada = 1;
        } else {
            $actuation->aprovada = 0;
        }
        $actuation->save();
    }

    public function deletar($id){
        $actuation = Actuation::find($id);
        $actuation->delete();
    }
}
