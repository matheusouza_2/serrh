<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Helpers\CustomFuncs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $data['cpf'] = CustomFuncs::limparFormatacao($data['cpf']);
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'data_nascimento' => 'required',
            'telefone' => 'required',
            // 'login' => 'required|unique:users',
            'cpf' => 'required|unique:users',
            'password' => 'required|confirmed|min:6'
        ], [
            'name.required' => 'Campo de nome obrigatório',
            'email.required' => 'Campo de e-mail obrigatório',
            'email.unique' => 'E-mail já cadastrado',
            'telefone.required' => 'Campo telefone obrigatório',
            'data_nascimento.required' => 'Campo de data de nascimento obrigatório',
            // 'login.required' => 'Campo de login é obrigatório',
            // 'login.unique' => 'Login já cadastrado',
            'cpf.required' => 'Campo CPF obrigatório',
            'cpf.unique' => 'CPF já cadastrado',
            'password.required' => 'Campo senha é obrigatório',
            'password.confirmed' => 'As senhas não se correspondem',
            'password.min' => 'Senha deve conter no mínimo 6 caracteres'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = new User();
        $user->id = null;
        $user->nome = $data["name"];
        $user->email = $data["email"];
        $user->celular = $data["telefone"];
        $user->cpf  = CustomFuncs::limparFormatacao($data["cpf"]);
        $user->data_nascimento = Carbon::createFromFormat('d/m/Y', $data["data_nascimento"]);
        // $user->login = $data["login"];
        $user->statususuario_id = 1;
        $user->perfisusuario_id = $data['empresario'] == 'y' ? 4 : 3;
        $user->sexo_id = 3;
        $user->password = Hash::make($data["password"]);
        $user->created_at = Carbon::now();
        $user->updated_at = Carbon::now();

        $user->save();
        return $user;
    }


    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        $user = $this->create($request->all());

        if ($user) {
            Auth::login($user);
        }

        if (Auth::check()) {
            return redirect($this->redirectPath());
        } else {
            return redirect('/register');
        }
    }
}
