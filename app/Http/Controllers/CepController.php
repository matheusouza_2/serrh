<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use GuzzleHttp\Client;

use Debugbar;

class CepController extends Controller
{

    public function encontraCep($cep){
      $client = new Client(['verify' => false]);
      $url='https://viacep.com.br/ws/'.$cep.'/json/';
      try {
        $response = $client->get($url);
        $resposta = $response->getBody()->getContents();
      } catch (\Exception $e){
        $resposta["erro"]=true;
        $resposta = json_encode($resposta);
      }

      return $resposta;
    }
}
