<?php

namespace App\Http\Controllers;

use App\Cobranca;
use Illuminate\Http\Request;

class CobrancaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cobranca  $cobranca
     * @return \Illuminate\Http\Response
     */
    public function show(Cobranca $cobranca)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cobranca  $cobranca
     * @return \Illuminate\Http\Response
     */
    public function edit(Cobranca $cobranca)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cobranca  $cobranca
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cobranca $cobranca)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cobranca  $cobranca
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cobranca $cobranca)
    {
        //
    }
}
