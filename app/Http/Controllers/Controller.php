<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use View;
use Auth;
use App\Models\Perfisusuario;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $perfilRegras = null;
    protected $teste = "bar";

    public function __construct(){
        $this->teste = "TESTES";
        $this->perfilRegras = collect();

        $perfilRegra = app()->make('stdClass');
        $perfilRegra->perfil_pai = Perfisusuario::where('identificador','=','Master')->first()->id;
        $perfilRegra->perfil_permitidos = Perfisusuario::all()->pluck('id');
        $this->perfilRegras->push($perfilRegra);

        $perfilRegra = app()->make('stdClass');
        $perfilRegra->perfil_pai = Perfisusuario::where('identificador','=','Administrador')->first()->id;
        $perfilRegra->perfil_permitidos = Perfisusuario::where('identificador','<>','Master')->pluck('id');
        $this->perfilRegras->push($perfilRegra);

        $perfilRegra = app()->make('stdClass');
        $perfilRegra->perfil_pai = Perfisusuario::where('identificador','=','Atendente')->first()->id;
        $perfilRegra->perfil_permitidos = Perfisusuario::where('identificador','<>','Master')->where('identificador','<>','Administrador')->pluck('id');
        $this->perfilRegras->push($perfilRegra);

        $perfilRegra = app()->make('stdClass');
        $perfilRegra->perfil_pai = Perfisusuario::where('identificador','=','Candidato')->first()->id;
        $perfilRegra->perfil_permitidos = Perfisusuario::where('identificador','=','Candidato')->pluck('id');
        $this->perfilRegras->push($perfilRegra);

        $perfilRegra = app()->make('stdClass');
        $perfilRegra->perfil_pai = Perfisusuario::where('identificador','=','Empresario')->first()->id;
        $perfilRegra->perfil_permitidos = Perfisusuario::where('identificador','=','Empresario')->pluck('id');
        $this->perfilRegras->push($perfilRegra);

        View::share('perfilRegras',$this->perfilRegras);
    }

    public function getRegrasPerfilAutenticado(){
        $regras = $this->perfilRegras->where('perfil_pai','=', Auth::user()->perfisusuario_id)->first();

        if($regras == null){
            return null;
        }
        return $regras->perfil_permitidos;
    }
}
