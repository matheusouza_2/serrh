<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Carbon\Carbon;
use App\Helpers\CustomFuncs;

use App\User;
use App\Models\Resultado;

use App\Models\Actuation;
use App\Models\Curriculo;
use App\Models\Historicoactuation;

use App\Models\Configuration;

use Illuminate\Http\Request;

use PDF;

class CurriculoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('cadastro.curriculo.index');
    }

    public function filtrar(Request $request){
        $actuations = $request->input('actuations');
        $lastActuationFlag = $request->input('lastActuationFlag');
        $startRange = $request->input('startRange');
        $endRange = $request->input('endRange');
        $cargo = $request->input('cargo');
        $usuario = $request->input('usuario');

        $config =Configuration::first();
        $paginationNumber = isset($config) ? Configuration::first()->pagination : 20;

        if(Auth::user()->perfil->identificador == "Candidato"){
            $curriculos = Curriculo::where('user_id','=',Auth::user()->id)->paginate($paginationNumber);
        } else {
            $curriculos = Curriculo::where(function($query) use ($startRange, $endRange){
                if($startRange != null){
                    $query->whereRaw("date(created_at) >= '".$startRange."'");
                }
                if($endRange != null){
                    $query->whereRaw("date(created_at) <= '".$endRange."'");
                }
            })
            ->whereHas('candidato', function($query) use ($usuario){
                $query->where('nome','like','%'.$usuario.'%');
            })
            ->get();

            $curriculos = $curriculos->keyBy('id');

            if($actuations != null){
                foreach($curriculos as $curriculo){
                    $toFind = $curriculo->historico->obterHistoricoCompleto()->pluck('actuation_id');
                    if($lastActuationFlag){
                        $toFind = $toFind[0];
                        if(!in_array($toFind, $actuations)){
                            $curriculos->forget($curriculo->id);
                        }
                    } else {
                        if(!array_intersect($toFind->toArray(), $actuations)){
                            $curriculos->forget($curriculo->id);
                        }
                    }
                }
            }

            if(!empty($cargo)){
                foreach($curriculos as $curriculo){
                    $toFind = $curriculo->historico->obterHistoricoCompleto()->pluck('cargo');
                    $founded = false;

                    foreach($toFind as $item){
                        if(strpos($item, $cargo) !== false){
                            $founded = true;
                            break;
                        }
                    }

                    if(!$founded){
                        $curriculos->forget($curriculo->id);
                    }
                }
            }

            $curriculos = Curriculo::whereIn('id',$curriculos->pluck('id'))->orderBy('created_at', 'desc')->paginate($paginationNumber);
        }

        return view('cadastro.curriculo._list', compact('curriculos'));
    }

    public function formulario($id){

        if($id == "new"){
            $inicial = null;
            $main = new User;
        } else {
            $main =  User::find($id);
            $inicial = $id;
        }

        if(Auth::user()->perfil->identificador == "Candidato"){
            $main = Auth::user();
            $inicial = $main->id;
        }

        $users = User::whereHas('perfil', function($query){
            $query->where('identificador','=','Candidato');
        })
        // ->whereNotExists(function($query){
        //     $query->select(DB::raw(1))
        //         ->from('curriculos')
        //         ->whereRaw('curriculos.user_id = users.id');
        // })
        ->get();

        return view('cadastro.curriculo._form', compact('main', 'users', 'inicial'));
    }

    public function visualizar($id){
        $curriculo = Curriculo::find($id);

        return view('cadastro.curriculo.visualizar', compact('curriculo'));
    }

    public function exportar($id){
        $curriculo = Curriculo::find($id);

        $data = ['curriculo' => $curriculo];
        $pdf = PDF::loadView('cadastro.curriculo.exportar', $data);

        return $pdf->download('curriculo.pdf');
    }

    public function novoHistorico(){
        return view('cadastro.curriculo._moreHistory');
    }

    public function loadCurriculo($id){
        $user = User::find($id);
        if($user == null){
            $user = new User;
        }
        if($user->curriculo == null){
            $user->curriculo = new Curriculo;
        }

        if($user->curriculo->historico == null){
            $user->curriculo->historico = new Historicoactuation;
        }
        return view('cadastro.curriculo._formCurriculo', compact('user'));
    }

    public function salvar(Request $request){
        $empresas = $request->input('empresasValores');
        $actuations = $request->input('actuationsValores');
        $cargos = $request->input('cargoValores');
        $dataEntrada = $request->input('dataEntradaValores');
        $dataSaida = $request->input('dataSaidaValores');
        $userId = $request->input('userId');
        $objetivoProfissional = $request->input('objetivoProfissional');
        $funcoesRealizadas = $request->input('funcoesRealizadas');
        $extracurricular = $request->input('extracurricular');
        $observacoesFinais = $request->input('observacoesFinais');
        $historicos = $request->input('historicoIdValores');
        $curriculoId = $request->input('curriculoId');

        $curriculo = new Curriculo;
        if($curriculoId != null){
            $curriculo = Curriculo::find($curriculoId);
        }

        $curriculo->objetivoProfissional = $objetivoProfissional;
        $curriculo->funcoesRealizadas = $funcoesRealizadas;
        $curriculo->extracurricular = $extracurricular;
        $curriculo->observacoesFinais = $observacoesFinais;

        $listHistoricoIds = array();
        foreach($historicos as $index=>$historicoId){

            $historico = new Historicoactuation;
            if($historicoId != null){
                $historico = Historicoactuation::find($historicoId);
            }

            $historico->empresa = $empresas[$index];
            $historico->actuation_id = $actuations[$index];
            $historico->cargo = $cargos[$index];
            $historico->data_entrada = $dataEntrada[$index];
            $historico->data_saida = $dataSaida[$index];
            $historico->pai = null;
            $historico->save();

            $listHistoricoIds[$index] = $historico->id;
        }

        $fatherId = Historicoactuation::defineFather($listHistoricoIds);

        $curriculo->historicoactuation_id = $fatherId;
        $curriculo->user_id = $userId;
        $curriculo->created_at = Carbon::now();
        $curriculo->updated_at = Carbon::now();
        $curriculo->save();

        $resultado=new Resultado(false,"",$curriculo);

        return json_encode($resultado);

    }
}
