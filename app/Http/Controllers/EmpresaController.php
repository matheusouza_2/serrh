<?php

namespace App\Http\Controllers;

use Auth;
use Hash;
use Validator;
use Barryvdh\DomPDF\Facade\Pdf;


use App\User;
use App\Models\Empresa;

use App\Models\Configuration;

use Illuminate\Http\Request;

class EmpresaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('cadastro.empresa.index');
    }

    public function filtrar(Request $request)
    {
        $razao_social = $request->input('razao_social');
        $nome_fantasia = $request->input('nome_fantasia');
        $cnpj = $request->input('cnpj');

        $config = Configuration::first();
        $paginationNumber = isset($config) ? Configuration::first()->pagination : 20;

        $empresas = Empresa::where(function ($query) use ($razao_social, $nome_fantasia, $cnpj) {
            if ($razao_social != null) {
                $query->where('razao_social', 'like', '%' . $razao_social . '%');
            }
            if ($nome_fantasia != null) {
                $query->where('nome_fantasia', 'like', '%' . $nome_fantasia . '%');
            }
            if ($cnpj != null) {
                $query->where('cnpj', 'like', $cnpj . '%');
            }
            if (Auth::user()->perfil->identificador == "Empresario") {
                $query->where('criador_id', '=', Auth::user()->id);
            }
        })->orderBy('razao_social')->paginate($paginationNumber);
        return view('cadastro.empresa._list', compact('empresas'));
    }

    public function formulario($id)
    {
        if ($id == "new") {
            $inicial = null;
            $main = new Empresa;
        } else {
            $main =  Empresa::find($id);
            $inicial = $id;
        }

        $empresa = $main;
        $empresarios = User::whereHas('perfil', function ($query) {
            $query->where('identificador', '=', 'Empresario');
        })->get();

        return view('cadastro.empresa.formMain', compact('empresa', 'empresarios'));
    }

    public function formById(Request $request)
    {
        $id = $request->input('id');

        $user = User::find($id);

        if ($user == null) {
            $user = new User;
        }

        return view('cadastro.usuarios._form', compact('user'));
    }

    public function salvar(Request $request)
    {
        $empresaId = $request->input('empresaId');
        $nomeFantasia = $request->input('nomeFantasia');
        $razaoSocial = $request->input('razaoSocial');
        $cnpj = $request->input('cnpj');
        $email = $request->input('email');
        $telefone = $request->input('telefone');
        $empresarioId = $request->input('empresarioId');

        $cep = $request->input('cep');
        $logradouro = $request->input('logradouro');
        $numero = $request->input('numero');
        $complemento = $request->input('complemento');
        $bairro = $request->input('bairro');
        $cidade = $request->input('cidade');
        $uf = $request->input('uf');
        $contato = $request->input('contato');

        $empresa = Empresa::find($empresaId);
        if ($empresa == null) {
            $empresa = new Empresa;
        }

        if ($empresaId == null) {
            $validator = Validator::make(
                $request->all(),
                [
                    'cnpj' => 'required|unique:empresas',
                ],
                [
                    'cnpj.unique' => 'Já existe um CNPJ cadastrado na base.',
                ]
            );

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()->all()]);
            }
        }


        $empresa->nome_fantasia = $nomeFantasia;
        $empresa->razao_social = $razaoSocial;
        $empresa->email = $email;
        $empresa->cnpj = $cnpj;
        $empresa->telefone = $telefone;
        $empresa->cep = $cep;
        $empresa->logradouro = $logradouro;
        $empresa->numero = $numero;
        $empresa->complemento = $complemento;
        $empresa->bairro = $bairro;
        $empresa->cidade = $cidade;
        $empresa->uf_id = $uf;
        $empresa->criador_id = $empresarioId;
        $empresa->contato = $contato;

        $empresa->save();

        return $empresa->id;
    }
}
