<?php

namespace App\Http\Controllers;

use Auth;
use Hash;
use Carbon\Carbon;

use App\User;
use App\Models\Vaga;
use App\Models\Ocupacaovaga;

use Illuminate\Http\Request;

class OcupacaovagaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    public function candidatar(Request $request){
        $id = $request->input('id');

        $ocupacaovaga = Ocupacaovaga::where('vaga_id', '=', $id)->where('candidato_id', Auth::user()->id)->get();

        if($ocupacaovaga->count() == 0){
        	$ocupacaovaga = new Ocupacaovaga;
        	$ocupacaovaga->created_at = Carbon::now();
        }

        $ocupacaovaga->vaga_id = $id;
        $ocupacaovaga->candidato_id = Auth::user()->id;
        $ocupacaovaga->statuscandidatovaga_id = 2;
        $ocupacaovaga->updated_at = Carbon::now();

        $ocupacaovaga->save();
    }


}


