<?php

namespace App\Http\Controllers;

use Auth;
use Hash;

use App\User;
use App\Models\Perfisusuario;

use App\Models\Configuration;

use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perfis = Perfisusuario::whereIn('id',$this->getRegrasPerfilAutenticado())->get();
        return view('cadastro.usuarios.index', compact('perfis'));
    }

    public function cadastrar(){
        return redirect('/login');
    }

    public function filtrar(Request $request){
        $name = $request->input('name');
        $cpf = $request->input('cpf');
        $perfil = $request->input('perfil');

        $config =Configuration::first();
        $paginationNumber = isset($config) ? Configuration::first()->pagination : 20;

        if(Auth::user()->perfil->descricao == "Candidato"){
            $users = User::where('id','=',Auth::user()->id)->paginate($paginationNumber);
        } else {
            $users = User::where(function($query) use ($name, $cpf, $perfil){
                if($name != null){
                    $query->where('nome','like','%'.$name.'%');
                }
                if($cpf != null){
                    $query->where('cpf','like',$cpf.'%');
                }
                if($perfil != null && count($perfil) > 0){
                    $query->whereHas('perfil', function($q) use ($perfil){
                        $q->whereIn('id',$perfil);
                    });
                }
            })->whereHas('perfil', function($query) {
                $query->whereIn('id',$this->getRegrasPerfilAutenticado());
            })->orderBy('created_at', 'desc')->paginate($paginationNumber);
        }

        return view('cadastro.usuarios._list',compact('users'));
    }

    public function formulario($id){
        if($id == "new"){
            $inicial = null;
            $main = new User;
        } else {
            $main =  User::find($id);
            $inicial = $id;
        }

        if(Auth::user()->perfil->identificador == "Candidato"){
            $main = Auth::user();
            $inicial = $main->id;
        }

        $user = $main;

        return view('cadastro.usuarios.formMain',compact('user'));
    }

    public function formById(Request $request){
        $id = $request->input('id');

        $user = User::find($id);

        if($user == null){
            $user = new User;
        }

        return view('cadastro.usuarios._form', compact('user'));
    }

    public function salvar(Request $request){
        $userId = $request->input('userId');
        $nome = $request->input('nome');
        // $login = $request->input('login');
        $senha = $request->input('senha');
        $cpf = $request->input('cpf');
        $email = $request->input('email');
        $dataNascimento = $request->input('dataNascimento');
        $telefone = $request->input('telefone');
        $celular = $request->input('celular');
        $sexo = $request->input('sexo');
        $perfil = $request->input('perfil');
        $escolaridade = $request->input('escolaridade');
        $formacao = $request->input('formacao');
        $civilstate = $request->input('civilstate');
        info($civilstate);
        if($civilstate == 'nd'){
            $civilstate = null;
        }

        $cep = $request->input('cep');
        $logradouro = $request->input('logradouro');
        $numero = $request->input('numero');
        $complemento = $request->input('complemento');
        $bairro = $request->input('bairro');
        $cidade = $request->input('cidade');
        $uf = $request->input('uf');

        $user = User::find($userId);
        if($user == null){
            $user = new User;
            $user->password = Hash::make($senha);
        } else {
            if($senha != null){
                $user->password = Hash::make($senha);
            }
        }



        $user->nome = $nome;
        $user->email = $email;
        // $user->login = $login;
        $user->cpf = $cpf;
        $user->perfisusuario_id = $perfil;
        $user->sexo_id = $sexo;
        $user->escolaridade_id = $escolaridade;
        $user->formacao = $formacao;
        $user->statususuario_id = 1;
        $user->data_nascimento = $dataNascimento;
        $user->telefone = $telefone;
        $user->celular = $celular;
        $user->cep = $cep;
        $user->logradouro = $logradouro;
        $user->numero = $numero;
        $user->complemento = $complemento;
        $user->bairro = $bairro;
        $user->cidade = $cidade;
        $user->uf_id = $uf;
        $user->civilstate_id = $civilstate;

        $user->save();
        return $user->id;
    }

    public function alterarsenhaindex(){
        return view('cadastro.usuarios.changepassword');
    }

    public function alterarsenha(Request $request){
        $user = Auth::user();
        $user->password = Hash::make($request->input('senha'));
        $user->save();
    }
}
