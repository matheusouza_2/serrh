<?php

namespace App\Http\Controllers;

use App\User;
use App\Cobranca;
use App\Models\Vaga;
use App\Models\Empresa;
use App\Models\Perfisusuario;
use App\Models\Ocupacaovaga;

use App\Models\Configuration;
use Barryvdh\DomPDF\PDF;
use Carbon\Carbon;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VagaController extends Controller
{
    //----CONSTS DA API----
    const API_URL = "https://sandbox.asaas.com/api/v3/";
    const KEY = "02c274828b64c13623d1d9ea4c9ab5774c89be63e7c22d54d1efdcd6fec80ad2";
    //----CONSTS DA API-----

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $authProfile = Auth::user()->perfil->identificador;
        if ($authProfile == "Candidato") {
            if (Auth::user()->curriculo == null) {
                return redirect('/cadastro/curriculos/' . Auth::user()->id . '/edicao')->with('warning', 'Necessário finalizar o cadastro para ter acesso à área de vagas!');
            }

            $own_vaga = Vaga::whereHas('candidatos', function ($query) {
                $query->where('candidato_id', '=', Auth::user()->id);
            })->get();

            foreach ($own_vaga as $vaga) {
                $vaga->isCandidato = true;
            }
        } else if ($authProfile == "Empresa") {
        } else {
            $own_vaga = Vaga::all();
        }
        $empresas = "";
        if (Auth::user()->perfisusuario_id == 1 || Auth::user()->perfisusuario_id == 2 || Auth::user()->perfisusuario_id == 5) {
            $empresas = Empresa::all();
        } else {
            $empresas = Empresa::where('criador_id', Auth::user()->id)->get();
        }

        return view('vaga.index', compact('own_vaga', 'empresas'));
    }

    public function indexVagaIntegrada()
    {
        $empresas = Empresa::all();
        return view('vagaintegrada.index', compact('empresas'));
    }

    public function filtrar(Request $request)
    {
        $actuation = $request->input('actuation');
        $empresa = $request->input('empresa');
        $statusvaga = $request->input('status');
        $name = $request->input('name');

        $config = Configuration::first();
        $paginationNumber = isset($config) ? Configuration::first()->pagination : 20;

        $vagas = Vaga::whereHas('actuation', function ($query) use ($actuation) {
            $query->where('descricao', 'like', '%' . $actuation . '%');
        })
            ->where('titulo_vaga', 'like', '%' . $name . '%')
            ->where('statusvaga_id', '=', $statusvaga)
            ->where(function ($query) use ($empresa) {
                if ($empresa != null) {
                    $query->whereIn('empresa_id', $empresa);
                }
            })
            ->where(function ($query) {
                if (Auth::user()->perfil->identificador == "Empresario") {
                    $query->whereIn('empresa_id', Auth::user()->empresas->pluck('id'));
                }
            })
            ->orderBy('titulo_vaga')->orderBy('created_at', 'desc')->paginate($paginationNumber);
        return view('vaga._list', compact('vagas'));
    }

    public function filtrarIntegrada(Request $request)
    {
        $rp = $request->input('rp');
        $name = $request->input('name');
        info($rp);
        $config = Configuration::first();
        $paginationNumber = isset($config) ? Configuration::first()->pagination : 20;

        $vagas = Vaga::where('titulo_vaga', 'like', '%' . $name . '%')->where(function ($q) use ($rp) {
            if ($rp != null || $rp != '') {
                $q->where('rp', 'like', '%' . $rp . '%');
            }
        })->where('api_available', '=', 0)->orderBy('titulo_vaga')->orderBy('created_at', 'desc')->paginate($paginationNumber);
        $vagasIntegradas = Vaga::where('api_available', '=', 1)->orderBy('created_at', 'desc')->get();

        return view('vagaintegrada.vagasIntegradas', compact('vagas', 'vagasIntegradas'));
    }

    public function filtrarCandidatos(Request $request)
    {
        $lista = $request->input('candidatos');
        $vagaId = $request->input('vagaId');

        $filtrados = Ocupacaovaga::where('vaga_id', '=', $vagaId)
            ->where(function ($query) use ($lista) {
                if ($lista != null && count($lista) > 0) {
                    $query->whereIn('candidato_id', $lista);
                }
            })->get();

        info($filtrados);
        return view('vaga._listCandidatos', compact('filtrados'));
    }

    public function formulario($id)
    {
        if ($id == "new") {
            $inicial = null;
            $main = new Vaga;
        } else {
            $main =  Vaga::find($id);
            $inicial = $id;
        }

        if (Auth::user()->perfil->identificador == "Candidato") {
            $main = Auth::user();
            $inicial = $main->id;
        }

        $vaga = $main;
        $empresas = "";
        if (Auth::user()->perfisusuario_id == 1 || Auth::user()->perfisusuario_id == 2 || Auth::user()->perfisusuario_id == 5) {
            $empresas = Empresa::all();
        } else {
            $empresas = Empresa::where('criador_id', Auth::user()->id)->get();
        }
        return view('vaga.formMain', compact('vaga', 'empresas'));
    }

    public function view($id)
    {
        $vaga = Vaga::find($id);

        $vagaSelfInfo = $vaga->candidatos->where('candidato_id', '=', Auth::user()->id);
        $vaga->isCandidato = ($vagaSelfInfo->where('statuscandidatovaga_id', '=', 2)->count() > 0);
        return view('vaga.view', compact('vaga'));
    }

    public function encontrar(Request $request)
    {
        $actuation = $request->input('filter');

        $unioesComuns = ["/\sd\w\s+/i"];
        $actuation = preg_replace('/\s+/', '%', preg_replace($unioesComuns, " ", $actuation));

        $own_vaga = Vaga::where('statusvaga_id', '=', 2)
            ->where(function ($q) use ($actuation) {
                $q->whereHas('actuation', function ($query) use ($actuation) {
                    $query->where('descricao', 'like', '%' . $actuation . '%');
                });
                $q->orWhereHas('empresa', function ($query) use ($actuation) {
                    $query->where('nome_fantasia', 'like', '%' . $actuation . '%');
                });
                $q->orWhere('titulo_vaga', 'like', '%' . $actuation . '%');
            })
            ->whereDoesntHave('candidatos', function ($query) {
                $query->where('candidato_id', '=', Auth::user()->id);
            })
            ->get();

        foreach ($own_vaga as $vaga) {
            $vaga->isCandidato = false;
        }

        return view('vaga.busca_vagas', compact('own_vaga'));
    }

    public function formById(Request $request)
    {
        $id = $request->input('id');

        $user = User::find($id);

        if ($user == null) {
            $user = new User;
        }

        return view('cadastro.usuarios._form', compact('user'));
    }

    public function salvar(Request $request)
    {
        $vagaId = $request->input('vagaId');
        $titulo_vaga = $request->input('titulo_vaga');
        $empresa = $request->input('empresa');
        $tiporecrutamento = $request->input('tiporecrutamento');
        $actuation = $request->input('actuation');
        $quantidade = $request->input('quantidade');
        $data_fechamento = $request->input('data_fechamento');
        $data_admissao = $request->input('data_admissao');
        $escolaridade = $request->input('escolaridade');
        $statusvaga = $request->input('statusvaga');
        $idade_minima = $request->input('idade_minima');
        $idade_maxima = $request->input('idade_maxima');
        $salario_inicial = $request->input('salario_inicial');
        $observacoes_vaga = $request->input('observacoes_vaga');
        $observacoes_candidato = $request->input('observacoes_candidato');
        $rp = $request->input('rp');
        $marcacaosexo = $request->input('marcacaosexo');
        $only_city = $request->input('only_city');
        $show_company = $request->input('show_company');
        $horario_trabalho = $request->input('horario_trabalho');
        $beneficios = $request->input('beneficios');

        $plano_pagamento = $request->input('plano_pagamento');

        $vaga = Vaga::find($vagaId);
        if ($vaga == null) {
            $vaga = new Vaga;
        }

        $vaga->titulo_vaga = $titulo_vaga;
        $vaga->empresa_id = $empresa;
        $vaga->tiporecrutamento =  $tiporecrutamento;
        $vaga->salario_inicial = $salario_inicial;
        $vaga->quantidade = $quantidade;
        $vaga->escolaridade_id = $escolaridade;
        $vaga->idade_min = $idade_minima;
        $vaga->idade_max = $idade_maxima;
        $vaga->statusvaga_id = $statusvaga;
        $vaga->actuation_id = $actuation;
        $vaga->data_fechamento = $data_fechamento;
        $vaga->data_admissao =  $data_admissao;
        $vaga->observacoes_vaga = $observacoes_vaga;
        $vaga->observacoes_candidato = $observacoes_candidato;
        $vaga->rp = $rp;
        $vaga->marcacaosexo = $marcacaosexo;
        $vaga->only_city = $only_city;
        $vaga->horario_trabalho = $horario_trabalho;
        $vaga->beneficios = $beneficios;
        $vaga->show_company = $show_company;

        //$vaga->plano_pagamento = $plano_pagamento;

        //VagaController::cadastroCliente($vaga);

        $vaga->save();

        //VagaController::gerarBoleto($vaga);

        return $vaga->id;
    }

    public function viewCandidatos($id)
    {
        $vaga = Vaga::find($id);
        return view('vaga.viewCandidatos', compact('vaga'));
    }

    public function toggleApiAvailable(Request $request)
    {
        $id = $request->input('id');
        $vaga = Vaga::find($id);
        $vaga->api_available = !$vaga->api_available;
        $vaga->save();
    }

    public function emitirRequisicao(Vaga $vaga)
    {
        $empresa = Empresa::find($vaga->empresa_id);
        $pdf = PDF::loadView('vaga.requisicao-pessoal', compact(['vaga', 'empresa']));
        return $pdf->stream();
    }

    /**
     *|_____________________________________
     *|
     *|     GERENCIAR A PARTE DE COBRANÇAS
     *|_____________________________________
     */

    public function cadastroCliente($vagas)
    {
        $empresa = Empresa::find($vagas->empresa_id);

        $clientExist = VagaController::buscarCliente($empresa->asaas_id);
        if ($clientExist != null) {
            return;
        }

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, self::API_URL . "customers");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);

        curl_setopt($ch, CURLOPT_POST, TRUE);

        curl_setopt($ch, CURLOPT_POSTFIELDS, "{
        \"name\": \"$empresa->razao_social\",
        \"email\": \"$empresa->email\",
        \"phone\": \"$empresa->telefone\",
        \"mobilePhone\": \"$empresa->celular\",
        \"cpfCnpj\": \"$empresa->cnpj\",
        \"postalCode\": \"$empresa->cep\",
        \"address\": \"$empresa->logradouro\",
        \"addressNumber\": \"$empresa->numero\",
        \"complement\": \"$empresa->complemento\",
        \"province\": \"$empresa->bairro\",
        \"externalReference\": \"$empresa->id\",
        \"notificationDisabled\": false,
        \"observations\": \" Cliente Novo \"
        }");

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type: application/json",
            "access_token: " . self::KEY
        ));

        $response = curl_exec($ch);

        curl_close($ch);

        $result = json_decode($response);

        $empresa->asaas_id = $result->id;
        $empresa->save();

        return;
    }

    public function buscarCliente($asaas_id)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::API_URL . "customers/{$asaas_id}");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type: application/json",
            "access_token: " . self::KEY
        ));

        $response = curl_exec($ch);
        curl_close($ch);

        $result = json_decode($response);

        return $result;
    }

    public function gerarBoleto(Vaga $vagas)
    {
        $empresa = Empresa::find($vagas->empresa_id);
        $plano = $vagas->plano_pagamento == 0 ? 'básico' : 'completo';
        $cobranca = Cobranca::where('vaga', $vagas->id)->get();
        if (isset($cobranca[0]->id)) {
            return;
        }

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, self::API_URL . "payments");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);

        curl_setopt($ch, CURLOPT_POST, TRUE);

        curl_setopt($ch, CURLOPT_POSTFIELDS, "{
        \"customer\": \"cus_000004729703\",
        \"billingType\": \"BOLETO\",
        \"dueDate\": \"" . Carbon::now()->addDays(30) . "\",
        \"value\": 100,
        \"description\": \" Boleto Referente a vaga de RP $vagas->rp. Cliente optou pelo plano $plano \",
        \"externalReference\": \"$vagas->id\",
        \"postalService\": false
        }");

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type: application/json",
            "access_token: " . self::KEY
        ));

        $response = curl_exec($ch);
        curl_close($ch);

        $result = json_decode($response);

        $cobranca = new Cobranca();

        $cobranca->asaas_id = $result->id;
        $cobranca->vaga = $result->externalReference;
        $cobranca->cliente = $result->customer;
        $cobranca->link_boleto = $result->bankSlipUrl;
        $cobranca->valor = $result->value;
        $cobranca->data_vencimento = $result->dueDate;
        $cobranca->status = $result->status;
        $cobranca->descricao = $result->description;

        $cobranca->save();

        return;
    }
}
