<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Curriculo extends Model
{
	public function getCreatedAtAttribute($value){
        try{
            if($value == null){
                return null;
            }
            return Carbon::parse($value)->format('d/m/Y');
        } catch (\Exception $e) {
            return $value;
        }
    }

    public function candidato(){
    	return $this->hasOne('App\User','id','user_id');
    }
    public function historico(){
    	return $this->hasOne('App\Models\Historicoactuation','id','historicoactuation_id');
    }
}
