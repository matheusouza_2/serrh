<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
	public function uf(){
        return $this->hasOne('App\Models\Uf', 'id', 'uf_id');
    }

    public function empresario()
    {
        return $this->hasOne('App\User', 'id', 'criador_id');
    }
    
    public function fullAddress(){
        $rua = $this->logradouro;
        $numero = $this->numero;
        $complemento = $this->complemento;
        $cidade = $this->cidade;
        $uf = $this->uf->sigla;

        $fullAddress = $rua;
        if($numero != null){
            $fullAddress = $fullAddress.", ".$numero;
        }

        $fullAddress = $fullAddress." - ".$cidade.",".$uf;

        if($complemento != null){
            $fullAddress = $fullAddress." (".$complemento.")";
        }

        return $fullAddress;
    }
}
