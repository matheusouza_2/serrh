<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Historicoactuation extends Model
{
    public function getDataEntradaAttribute($value){
        try{
            if($value == null){
                return null;
            }
            return Carbon::parse($value)->format('d/m/Y');    
        } catch (\Exception $e) {
            return $value;
        }        
    }

    public function getDataSaidaAttribute($value){
        try{
            if($value == null){
                return null;
            }
            return Carbon::parse($value)->format('d/m/Y');    
        } catch (\Exception $e) {
            return $value;
        }        
    }

	public function actuation(){
    	return $this->hasOne('App\Models\Actuation','id','actuation_id');
    }
    public function obterFilhos(){
    	return $this->hasMany('App\Models\Historicoactuation','pai')->orderBy('data_saida');
    }

    public function obterHistoricoCompleto(){
    	$histCompleto = array();
    	array_push($histCompleto, $this);
        foreach($histCompleto[0]->obterFilhos as $filho){
            array_push($histCompleto, $filho);    
        }
    	

    	return collect($histCompleto)->sortBy('data_saida');
    }

    public static function defineFather($listIds){
        $historicos = Historicoactuation::whereIn('id',$listIds)
            ->orderBy('data_saida')
            ->orderBy('data_entrada','desc')
            ->get();
        echo $historicos.PHP_EOL;
        foreach($historicos as $index=>$historico){
            if($index == 0){
                continue;
            }
            echo $historico.PHP_EOL;
            $historico->pai = $historicos->first()->id;
            $historico->save();
        }

        return $historicos->first()->id;

    }
}
