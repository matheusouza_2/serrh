<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    public function perfis(){
    	return $this->hasMany('App\Models\PerfisMenu','menu_id','id');
    }

    public function obterPai(){
    	return $this->belongsTo('App\Models\Menu','pai','id');
    }

    public function obterFilhos(){
    	return $this->hasMany('App\Models\Menu','pai');
    }
}
