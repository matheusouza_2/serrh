<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ocupacaovaga extends Model
{
    public function user(){
    	return $this->hasOne('App\User','id','candidato_id');
    }
}
