<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PerfisMenu extends Model
{
    public function perfil(){
    	return $this->hasOne('App\Models\Perfisusuario','id','perfisusuario_id');
    }

    public function menu(){
    	return $this->hasOne('App\Models\Menu','id','menu_id');
    }
}
