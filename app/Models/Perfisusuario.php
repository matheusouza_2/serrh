<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Perfisusuario extends Model
{
    public function menus(){
    	return $this->hasMany('App\Models\PerfisMenu','perfisusuario_id','id');
    }
}
