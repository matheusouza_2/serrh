<?php

namespace App\Models;

class Resultado
{
  public $erro = false;
  public $msgErro = "";
  public $retorno = "";

  public function __construct($erro,$msgErro,$retorno){
    $this->erro=$erro;
    $this->msgErro=$msgErro;
    $this->retorno=$retorno;
  }
}
