<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Vaga extends Model
{
    public function empresa(){
        return $this->hasOne('App\Models\Empresa', 'id', 'empresa_id');
    }

    public function actuation(){
        return $this->hasOne('App\Models\Actuation', 'id', 'actuation_id');
    }

    public function escolaridade(){
        return $this->hasOne('App\Models\Escolaridade', 'id', 'escolaridade_id');
    }

    public function status(){
        return $this->hasOne('App\Models\Statusvaga', 'id', 'statusvaga_id');
    }

    public function candidatos(){
        return $this->hasMany('App\Models\Ocupacaovaga');
    }

    public function getDataAdmissaoAttribute($value){
        try{
            if($value == null){
                return null;
            }
            return Carbon::parse($value)->format('d/m/Y');    
        } catch (\Exception $e) {
            return $value;
        }        
    }

    public function getDataFechamentoAttribute($value){
        try{
            if($value == null){
                return null;
            }
            return Carbon::parse($value)->format('d/m/Y');    
        } catch (\Exception $e) {
            return $value;
        }        
    }
}
