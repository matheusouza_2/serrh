<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class PasswordReset extends Notification
{
    use Queueable;

    /**
     * The password reset token.
     *
     * @var string
     */
    public $token;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Build the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        // return (new MailMessage)
        //     ->greeting('Olá')
        //     ->salutation('Até mais!')
        //     ->subject('Redefinição de senha')
        //     ->line('Está recebendo esse e-mail pois houve uma solicitação de redefinição de senha na sua conta.')
        //     ->action('Redefinir a senha', url('password/reset', $this->token))
        //     ->line('Se não foi você, pode desconsiderar o e-mail.');

        return (new MailMessage)->view(
            'emails.password', ['token' => $this->token]
        )->subject("Ser Online - Redefinição de Senha");
    }
}
