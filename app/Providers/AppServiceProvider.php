<?php

namespace App\Providers;

use Illuminate\Support\Facades\Auth;
use Carbon;

use App\Models\Uf;
use App\Models\Sexo;
use App\Models\Perfisusuario;
use App\Models\Escolaridade;
use App\Models\Actuation;
use App\Models\Civilstate;
use App\Models\Statusvaga;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        try {
            $ufs = Uf::all();
            view()->share('ufs', $ufs);

            $sexos = Sexo::all();
            view()->share('sexos', $sexos);

            $allPerfis = Perfisusuario::all();
            view()->share('allPerfis', $allPerfis);

            $perfis = Perfisusuario::where('id', '>', 2)->get();
            view()->share('perfis', $perfis);

            $escolaridades = Escolaridade::all();
            view()->share('escolaridades', $escolaridades);

            $actuations = Actuation::all();
            view()->share('actuations', $actuations);

            $civilstates = Civilstate::all();
            view()->share('civilstates', $civilstates);

            $allStatusVagas = Statusvaga::all();
            view()->share('allStatusVagas', $allStatusVagas);

            view()->share('isCandidato', (Auth::user() != null && Auth::user()->perfil->identificador == "Candidato"));
        } catch (\Exception $e) {
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
