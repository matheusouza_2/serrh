<?php

namespace App;

use Carbon\Carbon;
use App\Helpers\CustomFuncs;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\PasswordReset;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getDataNascimentoAttribute($value){
        try{
            if($value == null){
                return null;
            }
            return Carbon::parse($value)->format('d/m/Y');
        } catch (\Exception $e) {
            return $value;
        }
    }

    public function perfil(){
        return $this->hasOne('App\Models\Perfisusuario', 'id', 'perfisusuario_id');
    }

    public function uf(){
        return $this->hasOne('App\Models\Uf', 'id', 'uf_id');
    }

    public function curriculo(){
        return $this->hasOne('App\Models\Curriculo', 'user_id');
    }

    public function escolaridade(){
        return $this->hasOne('App\Models\Escolaridade', 'id', 'escolaridade_id');
    }

    public function sexo(){
        return $this->hasOne('App\Models\Sexo', 'id', 'sexo_id');
    }

    public function civilstate(){
        return $this->hasOne('App\Models\Civilstate', 'id', 'civilstate_id');
    }

    public function empresas(){
        return $this->hasMany('App\Models\Empresa', 'criador_id');
    }

    public function fullAddress(){
        $rua = $this->logradouro;
        $numero = $this->numero;
        $complemento = $this->complemento;
        $cidade = $this->cidade;
        $uf = $this->uf != null ? $this->uf->sigla : null;

        if($this->cep == null){
             $fullAddress = "Não registrado";
        } else {
            $fullAddress = $rua;
            if($numero != null){
                $fullAddress = $fullAddress.", ".$numero;
            }

            $fullAddress = $fullAddress." - ".$cidade.",".$uf;

            if($complemento != null){
                $fullAddress = $fullAddress." (".$complemento.")";
            }
        }


        return $fullAddress;
    }

    public function fullContato(){
        return CustomFuncs::formatarCelular($this->celular)." ".$this->email;
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new PasswordReset($token));
    }
}
