<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->string('login')->unique();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('cpf')->unique();
            $table->date('data_nascimento');
            $table->string('telefone')->nullable();
            $table->string('celular')->nullable();
            $table->string('cep')->nullable();
            $table->integer('uf_id')->nullable();
            $table->string('cidade')->nullable();
            $table->string('bairro')->nullable();
            $table->string('logradouro')->nullable();
            $table->string('numero')->nullable();
            $table->string('complemento')->nullable();
            $table->integer('sexo_id')->unsigned();
            $table->integer('perfisusuario_id')->unsigned();
            $table->integer('statususuario_id')->unsigned();
            $table->integer('escolaridade_id')->nullable();
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('sexo_id')->references('id')->on('sexos');
            $table->foreign('perfisusuario_id')->references('id')->on('perfisusuarios');
            $table->foreign('statususuario_id')->references('id')->on('statususuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
