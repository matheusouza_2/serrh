<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoricoactuationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historicoactuations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cargo');
            $table->integer('actuation_id')->unsigned();
            $table->string('empresa');
            $table->date('data_entrada');
            $table->date('data_saida')->nullable();
            $table->integer('pai')->nullable();
            $table->timestamps();

            $table->foreign('actuation_id')->references('id')->on('actuations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historicoactuations');
    }
}
