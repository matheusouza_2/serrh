<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurriculosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('curriculos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('historicoactuation_id')->unsigned();
            $table->text('objetivoProfissional')->nullable();
            $table->text('funcoesRealizadas')->nullable();
            $table->text('observacoesFinais')->nullable();
            $table->timestamps();

            $table->foreign('historicoactuation_id')->references('id')->on('historicoactuations');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('curriculos');
    }
}
