]<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('razao_social');
            $table->string('nome_fantasia');
            $table->string('email')->unique();
            $table->string('cnpj')->unique();
            $table->string('telefone')->nullable();
            $table->string('celular')->nullable();
            $table->string('cep');
            $table->integer('uf_id')->unsigned();
            $table->string('cidade');
            $table->string('bairro')->nullable();
            $table->string('logradouro')->nullable();
            $table->string('numero')->nullable();
            $table->string('complemento')->nullable();
            $table->integer('criador_id')->unsigned();
            $table->timestamps();

            $table->foreign('uf_id')->references('id')->on('ufs');
            $table->foreign('criador_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresas');
    }
}
