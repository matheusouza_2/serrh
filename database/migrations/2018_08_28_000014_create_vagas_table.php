<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVagasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vagas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo_vaga');
            $table->integer('empresa_id')->unsigned();
            $table->string('tiporecrutamento');
            $table->float('salario_inicial')->nullable();
            $table->integer('quantidade')->nullable();
            $table->integer('escolaridade_id')->unsigned();
            $table->integer('idade_min')->nullable();
            $table->integer('idade_max')->nullable();
            $table->integer('actuation_id')->unsigned();
            $table->date('data_admissao')->nullable();
            $table->date('data_fechamento')->nullable();
            $table->integer('statusvaga_id')->default(1)->unsigned();
            $table->text('observacoes_vaga')->nullable();
            $table->text('observacoes_candidato')->nullable();
            $table->timestamps();

            $table->foreign('empresa_id')->references('id')->on('empresas');
            $table->foreign('escolaridade_id')->references('id')->on('escolaridades');
            $table->foreign('actuation_id')->references('id')->on('actuations');
            $table->foreign('statusvaga_id')->references('id')->on('statusvagas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vagas');
    }
}
