<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOcupacaovagasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ocupacaovagas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vaga_id')->unsigned();
            $table->integer('candidato_id')->unsigned();
            $table->integer('statuscandidatovaga_id')->unsigned();
            $table->timestamps();

            $table->foreign('vaga_id')->references('id')->on('vagas');
            $table->foreign('candidato_id')->references('id')->on('users');
            $table->foreign('statuscandidatovaga_id')->references('id')->on('statuscandidatovagas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ocupacaovagas');
    }
}
