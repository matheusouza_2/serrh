<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnRpToVagaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vagas', function (Blueprint $table) {
            $table->string('rp')->nullable()->after('id');
            $table->text('beneficios')->nullable()->after('statusvaga_id');
            $table->text('horario_trabalho')->nullable()->after('statusvaga_id');
            $table->string('marcacaosexo')->nullable()->after('statusvaga_id');
            $table->boolean('only_city')->default(false)->after('statusvaga_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vagas', function (Blueprint $table) {
            //
        });
    }
}
