<?php

use Illuminate\Database\Seeder;

class ConfigurationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	if(count(DB::table('configurations')->get())==0){
            DB::table('configurations')->insert([ 'pagination' => '20', 'created_at' => Carbon\Carbon::now(), 'updated_at' => Carbon\Carbon::now() ]);
    	}
        
    }
}
