<?php

use Illuminate\Database\Seeder;

class EscolaridadesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(count(DB::table('escolaridades')->where('identificador','=','MedioIncompleto')->get())==0){
    		DB::table('escolaridades')->insert([ 
    				'identificador' => 'MedioIncompleto'
    			,	'descricao' 	=> 'Ensino Médio Incompleto'
    		]);
    	}
    	if(count(DB::table('escolaridades')->where('identificador','=','MedioCompleto')->get())==0){
    		DB::table('escolaridades')->insert([ 
    				'identificador' => 'MedioCompleto'
    			,	'descricao' 	=> 'Ensino Médio Completo'
    		]);
    	}
    	if(count(DB::table('escolaridades')->where('identificador','=','SuperiorIncompleto')->get())==0){
    		DB::table('escolaridades')->insert([ 
    				'identificador' => 'SuperiorIncompleto'
    			,	'descricao' 	=> 'Ensino Superior Incompleto'
    		]);
    	}
    	if(count(DB::table('escolaridades')->where('identificador','=','SuperiorCompleto')->get())==0){
    		DB::table('escolaridades')->insert([ 
    				'identificador' => 'SuperiorCompleto'
    			,	'descricao' 	=> 'Ensino Superior Completo'
    		]);
			}
			if(count(DB::table('escolaridades')->where('identificador','=','TecnicoCursando')->get())==0){
    		DB::table('escolaridades')->insert([ 
    				'identificador' => 'TecnicoCursando'
    			,	'descricao' 	=> 'Técnico Cursando'
    		]);
    	}
			if(count(DB::table('escolaridades')->where('identificador','=','TecnicoIncompleto')->get())==0){
    		DB::table('escolaridades')->insert([ 
    				'identificador' => 'TecnicoIncompleto'
    			,	'descricao' 	=> 'Técnico Incompleto'
    		]);
			}
			if(count(DB::table('escolaridades')->where('identificador','=','TecnicoCompleto')->get())==0){
    		DB::table('escolaridades')->insert([ 
    				'identificador' => 'TecnicoCompleto'
    			,	'descricao' 	=> 'Técnico Completo'
    		]);
    	}

    }
}
