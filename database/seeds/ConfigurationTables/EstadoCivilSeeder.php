<?php

use Illuminate\Database\Seeder;

class EstadoCivilSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(count(DB::table('civilstates')->where('identificador','=','Solteiro')->get())==0){
    		DB::table('civilstates')->insert([
    				'identificador' => 'Solteiro'
    			,	'descricao' 	=> 'Solteiro'
    		]);
    	}
    	if(count(DB::table('civilstates')->where('identificador','=','Casado')->get())==0){
    		DB::table('civilstates')->insert([
    				'identificador' => 'Casado'
    			,	'descricao' 	=> 'Casado'
    		]);
    	}
    	if(count(DB::table('civilstates')->where('identificador','=','Divorciado')->get())==0){
    		DB::table('civilstates')->insert([
    				'identificador' => 'Divorciado'
    			,	'descricao' 	=> 'Divorciado'
    		]);
    	}
    	if(count(DB::table('civilstates')->where('identificador','=','Viuvo')->get())==0){
    		DB::table('civilstates')->insert([
    				'identificador' => 'Viuvo'
    			,	'descricao' 	=> 'Viúvo'
    		]);
    	}

    }
}
