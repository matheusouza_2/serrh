<?php

use Illuminate\Database\Seeder;

class PerfisusuarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(count(DB::table('perfisusuarios')->where('identificador','=','Master')->get())==0){
            DB::table('perfisusuarios')->insert([ 
                    'identificador' => 'Master'
                ,   'descricao'     => 'Master'
            ]);
        }
        if(count(DB::table('perfisusuarios')->where('identificador','=','Administrador')->get())==0){
            DB::table('perfisusuarios')->insert([ 
                    'identificador' => 'Administrador'
                ,   'descricao'     => 'Administrador'
            ]);
        }
        if(count(DB::table('perfisusuarios')->where('identificador','=','Candidato')->get())==0){
    		DB::table('perfisusuarios')->insert([ 
    				'identificador' => 'Candidato'
    			,	'descricao' 	=> 'Candidato'
    		]);
    	}
        if(count(DB::table('perfisusuarios')->where('identificador','=','Empresario')->get())==0){
            DB::table('perfisusuarios')->insert([ 
                    'identificador' => 'Empresario'
                ,   'descricao'     => 'Empresário'
            ]);
        }
        if(count(DB::table('perfisusuarios')->where('identificador','=','Atendente')->get())==0){
            DB::table('perfisusuarios')->insert([ 
                    'identificador' => 'Atendente'
                ,   'descricao'     => 'Atendente'
            ]);
        }
    }
}
