<?php

use Illuminate\Database\Seeder;

class SexosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(count(DB::table('sexos')->where('identificador','=','Male')->get())==0){
    		DB::table('sexos')->insert([ 
    				'identificador' => 'Male'
    			,	'descricao' 	=> 'Masculino'
    		]);
    	}
    	if(count(DB::table('sexos')->where('identificador','=','Female')->get())==0){
    		DB::table('sexos')->insert([ 
    				'identificador' => 'Female'
    			,	'descricao' 	=> 'Feminino'
    		]);
    	}
    	if(count(DB::table('sexos')->where('identificador','=','NotInformed')->get())==0){
    		DB::table('sexos')->insert([ 
    				'identificador' => 'NotInformed'
    			,	'descricao' 	=> 'Não informado'
    		]);
    	}
    }
}
