<?php

use Illuminate\Database\Seeder;

class StatuscandidatovagasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(count(DB::table('statuscandidatovagas')->where('identificador','=','Interesse')->get())==0){
    		DB::table('statuscandidatovagas')->insert([ 
    				'identificador' => 'Interesse'
    			,	'descricao' 	=> 'Interessado na vaga'
    		]);
    	}
    	if(count(DB::table('statuscandidatovagas')->where('identificador','=','Candidato')->get())==0){
    		DB::table('statuscandidatovagas')->insert([ 
    				'identificador' => 'Candidato'
    			,	'descricao' 	=> 'Candidato à vaga'
    		]);
    	}
    	if(count(DB::table('statuscandidatovagas')->where('identificador','=','Contratado')->get())==0){
    		DB::table('statuscandidatovagas')->insert([ 
    				'identificador' => 'Contratado'
    			,	'descricao' 	=> 'Contratado para vaga'
    		]);
    	}
    }
}
