<?php

use Illuminate\Database\Seeder;

class StatususuarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(count(DB::table('statususuarios')->where('identificador','=','ativo')->get())==0){
    		DB::table('statususuarios')->insert([ 
    				'identificador' => 'ativo'
    			,	'descricao' 	=> 'Ativo'
    		]);
    	}
    	if(count(DB::table('statususuarios')->where('identificador','=','inativo')->get())==0){
    		DB::table('statususuarios')->insert([ 
    				'identificador' => 'inativo'
    			,	'descricao' 	=> 'Inativo'
    		]);
    	}
    }
}
