<?php

use Illuminate\Database\Seeder;

class StatusvagasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(count(DB::table('statusvagas')->where('identificador','=','Andamento')->get())==0){
    		DB::table('statusvagas')->insert([ 
    				'identificador' => 'Andamento'
    			,	'descricao' 	=> 'Em Andamento'
                ,   'created_at'    => Carbon\Carbon::now()
                ,   'updated_at'    => Carbon\Carbon::now()
    		]);
    	}
    	if(count(DB::table('statusvagas')->where('identificador','=','Aberto')->get())==0){
    		DB::table('statusvagas')->insert([ 
    				'identificador' => 'Aberto'
    			,	'descricao' 	=> 'Aberto'
                ,   'created_at'    => Carbon\Carbon::now()
                ,   'updated_at'    => Carbon\Carbon::now()
    		]);
    	}
    	if(count(DB::table('statusvagas')->where('identificador','=','Fechada')->get())==0){
    		DB::table('statusvagas')->insert([ 
    				'identificador' => 'Fechada'
    			,	'descricao' 	=> 'Fechada'
                ,   'created_at'    => Carbon\Carbon::now()
                ,   'updated_at'    => Carbon\Carbon::now()
    		]);
    	}

    }
}
