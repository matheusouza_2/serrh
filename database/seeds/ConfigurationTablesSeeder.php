<?php

use Illuminate\Database\Seeder;

class ConfigurationTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$this->call(UfsSeeder::class);
        $this->call(StatususuarioSeeder::class);
        $this->call(PerfisusuarioSeeder::class);
        $this->call(SexosSeeder::class);
        $this->call(EscolaridadesSeeder::class);
        $this->call(StatuscandidatovagasSeeder::class);
        $this->call(StatusvagasSeeder::class);
        $this->call(EstadoCivilSeeder::class);
        $this->call(ConfigurationsSeeder::class);
    }
}
