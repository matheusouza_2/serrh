<?php

use Illuminate\Database\Seeder;

class MenuAndUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$master = DB::table('perfisusuarios')->where('identificador','=','Master')->first()->id;
    	$administrador = DB::table('perfisusuarios')->where('identificador','=','Administrador')->first()->id;
    	$candidato = DB::table('perfisusuarios')->where('identificador','=','Candidato')->first()->id;
    	$empresario = DB::table('perfisusuarios')->where('identificador','=','Empresário')->first()->id;
    	$atendente = DB::table('perfisusuarios')->where('identificador','=','Atendente')->first()->id;

         if(count(DB::table('menus')->where('identificador','=','Cadastro')->get())==0){
    		$pai = DB::table('menus')->insertGetId([
    				'identificador' => 'Cadastro'
    			,	'descricao' => 'Cadastro'
    			,	'descricao_longa' 	=> 'Cadastro de informações'
    			,	'icon'		=> 'fa fa-users'
    			,	'rota' 	=> '#'
    			,	'pai' => null
    		]);

    		$cargo = DB::table('menus')->insertGetId([
    				'identificador' => 'Cargos'
    			,	'descricao' => 'Cargos'
    			,	'descricao_longa' 	=> 'Cadastro de cargos'
    			,	'icon'		=> 'far fa-circle'
    			,	'rota' 	=> '/cadastro/cargos'
    			,	'pai' => $pai
    		]);

    		$curriculo = DB::table('menus')->insertGetId([
    				'identificador' => 'Currículos'
    			,	'descricao' => 'Currículos'
    			,	'descricao_longa' 	=> 'Cadastro de currículos'
    			,	'icon'		=> 'far fa-circle'
    			,	'rota' 	=> '/cadastro/curriculos'
    			,	'pai' => $pai
    		]);

    		$empresa = DB::table('menus')->insertGetId([
    				'identificador' => 'Empresas'
    			,	'descricao' => 'Empresas'
    			,	'descricao_longa' 	=> 'Cadastro de empresas'
    			,	'icon'		=> 'far fa-circle'
    			,	'rota' 	=> '/cadastro/empresas'
    			,	'pai' => $pai
    		]);

    		// $funcionario = DB::table('menus')->insertGetId([
    		// 		'identificador' => 'Funcionarios'
    		// 	,	'descricao' => 'Funcionários'
    		// 	,	'descricao_longa' 	=> 'Cadastro de funcionários'
    		// 	,	'icon'		=> 'far fa-circle'
    		// 	,	'rota' 	=> '/cadastro/funcionarios'
    		// 	,	'pai' => $pai
    		// ]);

    		$usuario = DB::table('menus')->insertGetId([
    				'identificador' => 'Usuarios'
    			,	'descricao' => 'Usuários'
    			,	'descricao_longa' 	=> 'Cadastro de usuários'
    			,	'icon'		=> 'far fa-circle'
    			,	'rota' 	=> '/cadastro/usuarios'
    			,	'pai' => $pai
    		]);

    		$menuMaster = [$pai];
    		$menuAdministrador = [$pai];
    		// $menuCandidato = [$curriculo, $usuario];
    		$menuEmpresario = [$empresa];
    		$menuAtendente = [$pai];

    		foreach($menuMaster as $menu){
    			DB::table('perfis_menus')->insert([
    				'perfisusuario_id' => $master,
    				'menu_id' => $menu
    			]);
    		}

    		foreach($menuAdministrador as $menu){
    			DB::table('perfis_menus')->insert([
    				'perfisusuario_id' => $administrador,
    				'menu_id' => $menu
    			]);
    		}

    		// foreach($menuCandidato as $menu){
    		// 	DB::table('perfis_menus')->insert([
    		// 		'perfisusuario_id' => $candidato,
    		// 		'menu_id' => $menu
    		// 	]);
    		// }

    		foreach($menuEmpresario as $menu){
    			DB::table('perfis_menus')->insert([
    				'perfisusuario_id' => $empresario,
    				'menu_id' => $menu
    			]);
    		}

    		foreach($menuAtendente as $menu){
    			DB::table('perfis_menus')->insert([
    				'perfisusuario_id' => $atendente,
    				'menu_id' => $menu
    			]);
    		}


    	}

    	// if(count(DB::table('menus')->where('identificador','=','RecrutamentoSelecao')->get())==0){
    	// 	$pai = DB::table('menus')->insertGetId([
    	// 			'identificador' => 'RecrutamentoSelecao'
    	// 		,	'descricao' => 'Recrutamento & Seleção'
    	// 		,	'descricao_longa' 	=> 'Seção para recrutamento e seleção'
    	// 		,	'icon'		=> 'fas fa-book-open'
    	// 		,	'rota' 	=> '#'
    	// 		,	'pai' => null
    	// 	]);

    	// 	$requisicao = DB::table('menus')->insertGetId([
    	// 			'identificador' => 'RequisicaoPessoal'
    	// 		,	'descricao' => 'Requisição Pessoal'
    	// 		,	'descricao_longa' 	=> 'Formulário para requisição pessoal'
    	// 		,	'icon'		=> 'far fa-circle'
    	// 		,	'rota' 	=> '/recrutamentoSelecao/requsicaopessoal'
    	// 		,	'pai' => $pai
    	// 	]);

    	// 	$menuMaster = [$pai];
    	// 	$menuAdministrador = [$pai];
    	// 	$menuCandidato = [];
    	// 	$menuEmpresario = [];
    	// 	$menuAtendente = [$pai];

    	// 	foreach($menuMaster as $menu){
    	// 		DB::table('perfis_menus')->insert([
    	// 			'perfisusuario_id' => $master,
    	// 			'menu_id' => $menu
    	// 		]);
    	// 	}

    	// 	foreach($menuAdministrador as $menu){
    	// 		DB::table('perfis_menus')->insert([
    	// 			'perfisusuario_id' => $administrador,
    	// 			'menu_id' => $menu
    	// 		]);
    	// 	}

    	// 	foreach($menuCandidato as $menu){
    	// 		DB::table('perfis_menus')->insert([
    	// 			'perfisusuario_id' => $candidato,
    	// 			'menu_id' => $menu
    	// 		]);
    	// 	}

    	// 	foreach($menuEmpresario as $menu){
    	// 		DB::table('perfis_menus')->insert([
    	// 			'perfisusuario_id' => $empresario,
    	// 			'menu_id' => $menu
    	// 		]);
    	// 	}

    	// 	foreach($menuAtendente as $menu){
    	// 		DB::table('perfis_menus')->insert([
    	// 			'perfisusuario_id' => $atendente,
    	// 			'menu_id' => $menu
    	// 		]);
    	// 	}
    	// }

        if(count(DB::table('menus')->where('identificador','=','Vagas')->get())==0){
            $pai = DB::table('menus')->insertGetId([
                    'identificador' => 'Vagas'
                ,   'descricao' => 'Vagas'
                ,   'descricao_longa'   => 'Seção para vagas'
                ,   'icon'      => 'fa fa-user-plus'
                ,   'rota'  => '#'
                ,   'pai' => null
            ]);

            $gerenciar = DB::table('menus')->insertGetId([
                    'identificador' => 'GerenciarVagas'
                ,   'descricao' => 'Gerenciar Vagas'
                ,   'descricao_longa'   => 'Gerenciamento para vagas'
                ,   'icon'      => 'fa fa-cogs'
                ,   'rota'  => '/vagas'
                ,   'pai' => $pai
            ]);

            // $buscar = DB::table('menus')->insertGetId([
            //         'identificador' => 'Buscar'
            //     ,   'descricao' => 'Buscar Vagas'
            //     ,   'descricao_longa'   => 'Busca de vagas'
            //     ,   'icon'      => 'fa fa-search'
            //     ,   'rota'  => '/vagas/search'
            //     ,   'pai' => $pai
            // ]);

            $menuMaster = [$pai];
            $menuAdministrador = [$pai];
            $menuCandidato = [$pai];
            $menuEmpresario = [$pai];
            $menuAtendente = [$pai];

            foreach($menuMaster as $menu){
                DB::table('perfis_menus')->insert([
                    'perfisusuario_id' => $master,
                    'menu_id' => $menu
                ]);
            }

            foreach($menuAdministrador as $menu){
                DB::table('perfis_menus')->insert([
                    'perfisusuario_id' => $administrador,
                    'menu_id' => $menu
                ]);
            }

            foreach($menuCandidato as $menu){
                DB::table('perfis_menus')->insert([
                    'perfisusuario_id' => $candidato,
                    'menu_id' => $menu
                ]);
            }

            foreach($menuEmpresario as $menu){
                DB::table('perfis_menus')->insert([
                    'perfisusuario_id' => $empresario,
                    'menu_id' => $menu
                ]);
            }

            foreach($menuAtendente as $menu){
                DB::table('perfis_menus')->insert([
                    'perfisusuario_id' => $atendente,
                    'menu_id' => $menu
                ]);
            }
        }

        if(count(DB::table('menus')->where('identificador','=','AreaCandidato')->get())==0){
            $pai = DB::table('menus')->insertGetId([
                    'identificador' => 'AreaCandidato'
                ,   'descricao' => 'Área do Candidato'
                ,   'descricao_longa'   => 'Área própria para informações do candidato'
                ,   'icon'      => 'fa fa-user'
                ,   'rota'  => '#'
                ,   'pai' => null
            ]);

            $perfil = DB::table('menus')->insertGetId([
                    'identificador' => 'MeuPerfil'
                ,   'descricao' => 'Meu Perfil'
                ,   'descricao_longa'   => 'Perfil do usuário'
                ,   'icon'      => ''
                ,   'rota'  => '/cadastro/usuarios'
                ,   'pai' => $pai
            ]);

            $perfil = DB::table('menus')->insertGetId([
                    'identificador' => 'MeuCurriculo'
                ,   'descricao' => 'Meu Currículo'
                ,   'descricao_longa'   => 'Currículo do usuário'
                ,   'icon'      => ''
                ,   'rota'  => '/cadastro/curriculos'
                ,   'pai' => $pai
            ]);

            $menuCandidato = [$pai];

            foreach($menuCandidato as $menu){
                DB::table('perfis_menus')->insert([
                    'perfisusuario_id' => $candidato,
                    'menu_id' => $menu
                ]);
            }
        }
    }
}
