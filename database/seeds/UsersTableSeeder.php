<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(count(DB::table('users')->where('login','=','master')->get())==0){
    		DB::table('users')->insert([
    				'nome' => 'master'
    			,	'login' => 'master'
    			,	'password' 	=> Hash::make('master_rh2018')
    			,	'cpf'		=> '05081188085'
    			,	'email' 	=> 'teste@rhsystest.com'
    			,	'cep' => '07859340'
    			,	'uf_id' => 25
    			, 	'sexo_id' => 3
    			, 	'perfisusuario_id' => 1
    			, 	'statususuario_id' => 1
    			, 	'escolaridade_id' => 4
                ,   'data_nascimento' => '1980/01/01'
    		]);
    	}

    	if(count(DB::table('users')->where('login','=','administrador')->get())==0){
    		DB::table('users')->insert([
    				'nome' => 'administrador'
    			,	'login' => 'administrador'
    			,	'password' 	=> Hash::make('administrador_rh2018')
    			,	'cpf'		=> '42202415076'
    			,	'email' 	=> 'teste1@rhsystest.com'
    			,	'cep' => '07859340'
    			,	'uf_id' => 25
    			, 	'sexo_id' => 3
    			, 	'perfisusuario_id' => 2
    			, 	'statususuario_id' => 1
    			, 	'escolaridade_id' => 4
                ,   'data_nascimento' => '1980/01/01'
    		]);
    	}

    	if(count(DB::table('users')->where('login','=','candidato')->get())==0){
    		DB::table('users')->insert([
    				'nome' => 'candidato'
    			,	'login' => 'candidato'
    			,	'password' 	=> Hash::make('candidato_rh2018')
    			,	'cpf'		=> '08581177034'
    			,	'email' 	=> 'teste2@rhsystest.com'
    			,	'cep' => '07859340'
    			,	'uf_id' => 25
    			, 	'sexo_id' => 3
    			, 	'perfisusuario_id' => 3
    			, 	'statususuario_id' => 1
    			, 	'escolaridade_id' => 2
                ,   'data_nascimento' => '1980/01/01'
    		]);
    	}

    	if(count(DB::table('users')->where('login','=','empresario')->get())==0){
    		DB::table('users')->insert([
    				'nome' => 'empresario'
    			,	'login' => 'empresario'
    			,	'password' 	=> Hash::make('empresario_rh2018')
    			,	'cpf'		=> '72272479022'
    			,	'email' 	=> 'teste3@rhsystest.com'
    			,	'cep' => '07859340'
    			,	'uf_id' => 25
    			, 	'sexo_id' => 3
    			, 	'perfisusuario_id' => 4
    			, 	'statususuario_id' => 1
    			, 	'escolaridade_id' => 2
                ,   'data_nascimento' => '1980/01/01'
    		]);
    	}
    }
}
