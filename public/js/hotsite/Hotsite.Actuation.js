function Actuation() {
};

Actuation.prototype.filtrar = function(page){
	if (page==undefined){
    	page=1;
  	}

	token        = $('input[name="_token"]').val();
	name 		 = $('#name').val();

	var valores = {
		_token                	: token,
		name 					: name,
		page 					: page,
	}

	$.ajax({
	url: '/cadastro/actuations/filtrar',
		method: "POST",
		data: valores,
		error:function(data){
			alert('Erro');
			console.log(data.responseJSON.message);
		},
		beforeSend: function(){
			$('.overlay').show();
		},
		success: function(data){
			if (data.erro){
				
			} else {
				$('.overlay').hide();
				$("#result").html(data);
			}
		}
	});
}

Actuation.prototype.checkActuationSelect = function(element){
	if($(element).val() == "new"){
		$.ajax({
			url: '/cadastro/actuations/new/quickModal',
			method: "GET",
			error:function(data){
				alert('Erro');
				console.log(data.responseJSON.message);
			},
			success: function(data){
				if (data.erro){				
				} else {
					$("#modal-to-view").html(data).modal('show');
				    // $('.select2').select2();
				    // $('#dataNascimento').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' });
				    // $('input[name="dataEntrada[]"]').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' });
				    // $('input[name="dataSaida[]"]').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' });
				    // $('[data-mask]').inputmask();
				    // $('.textarea').wysihtml5();
				}
			}
		});
	}
}

Actuation.prototype.edit = function(id, originActuation){
	$.ajax({
		url: '/cadastro/actuations/'+id+'/quickModal',
		method: "GET",
		error:function(data){
			alert('Erro');
			console.log(data.responseJSON.message);
		},
		success: function(data){
			if (data.erro){				
			} else {
				$("#modal-to-view").html(data).modal('show');
				$("#originActuation").val(originActuation);
			}
		}
		});
}

Actuation.prototype.checkViability = function(btn){
	token        = $('input[name="_token"]').val();
	actuation       = $('#actuation').val();
	lastValue 	= $('#descricao').val();

	var valores = {
		_token                	: token,
		actuation           		: actuation,
		lastValue 				: lastValue,
	}

	btnPrevious = $(btn).html();

	$.ajax({
	url: '/cadastro/actuations/viability',
		method: "POST",
		data: valores,
		error:function(data){
			alert('Erro');
			console.log(data.responseJSON.message);
		},
		beforeSend: function(){
			$('.overlay').show();
		},
		success: function(data){
			retorno = JSON.parse(data);
			if(retorno.length == 0){
				$(btn).removeClass('btn-primary').addClass('btn-success').html("<i class='fa fa-check'></i> Disponível");
				$("#btnActuationSave").prop('disabled',false);
			} else {
				$(btn).removeClass('btn-primary').addClass('btn-danger').html("<i class='fa fa-times'></i> Indisponível");
				$('span.aviso').html("Já existe área semelhante como: " + retorno[0].descricao);
				$("#btnActuationSave").prop('disabled',true);
			}
			
		}
	});
}

Actuation.prototype.viabilityChange = function(){
	$("#viabilityButton").removeClass('btn-success').removeClass('btn-danger').addClass('btn-primary').html("<i class='fa fa-search'></i> Verificar disponibilidade");
	$('span.aviso').html("");
	$("#btnActuationSave").prop('disabled',true);
}

Actuation.prototype.salvar = function(){
	token       = $('input[name="_token"]').val();
	actuation       = $('#actuation').val();
	id 			= $('#id').val();
	fromModal 	= $('#modalActuation').val() != null;
	originActuation = $('#originActuation').val();

	var valores = {
		_token                	: token,
		actuation           		: actuation,
		id 						: id,
	}
	
	$.ajax({
	url: '/cadastro/actuations/salvar',
		method: "POST",
		data: valores,
		error:function(data){
			alert('Erro');
			console.log(data.responseJSON.message);
		},
		success: function(data){
			retorno = JSON.parse(data);
			if(retorno.length == 0){
				alert('Erro');
			} else {
				if(fromModal){
					$("#modal-to-view").html("").modal('hide');
					$("select[name='actuation[]'] option:selected[value='new']").parent().append("<option value='"+retorno.id+"' selected>"+retorno.descricao+"</option>").change();
					$("select[name='actuation[]'] option:selected[value!='new']").parent().append("<option value='"+retorno.id+"'>"+retorno.descricao+"</option>").change();
				}
				if(originActuation != null){
					hotsite.actuation.filtrar();
				}
			}			
		}
	});
}

Actuation.prototype.changeStatus = function(id){
	$.ajax({
		url: '/cadastro/actuations/'+id+'/changeStatus',
		method: "GET",
		error:function(data){
			alert('Erro');
			console.log(data.responseJSON.message);
		},
		success: function(data){
			if (data.erro){				
			} else {
				hotsite.actuation.filtrar();
			}
		}
		});
}

Actuation.prototype.delete = function(id){
	if(confirm("Deseja excluir esse atuação?")){
		$.ajax({
		url: '/cadastro/actuations/'+id+'/deletar',
		method: "GET",
		error:function(data){
			alert('Erro');
			console.log(data.responseJSON.message);
		},
		success: function(data){
			if (data.erro){				
			} else {
				hotsite.actuation.filtrar();
			}
		}
		});
	}
	
}


Hotsite.prototype.actuation = new Actuation();
