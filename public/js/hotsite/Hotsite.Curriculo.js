function Curriculo() {}

Curriculo.prototype.filtrar = function(page) {
    if (page == undefined) {
        page = 1;
    }
    token = $('input[name="_token"]').val();
    cargo = $("#cargo").val();
    usuario = $("#usuario").val();
    actuations = $("#actuations").val();
    startRange = hotsite.dataBrasilparaDataDB($("#data-inicial").val());
    endRange = hotsite.dataBrasilparaDataDB($("#data-final").val());
    lastActuationFlag = $("#lastActuation").is(":checked") ? 1 : 0;

    var valores = {
        _token: token,
        actuations: actuations,
        lastActuationFlag: lastActuationFlag,
        startRange: startRange,
        endRange: endRange,
        page: page,
        cargo: cargo,
        usuario: usuario
    };
    $.ajax({
        url: "/cadastro/curriculos/filtrar",
        method: "POST",
        data: valores,
        error: function(data) {
            alert("Erro");
            console.log(data.responseJSON.message);
        },
        beforeSend: function() {
            $(".overlay").show();
        },
        success: function(data) {
            if (data.erro) {
            } else {
                $(".overlay").hide();
                $("#result").html(data);
            }
        }
    });
};

Curriculo.prototype.loadCurriculo = function($id) {
    $.ajax({
        url: "/cadastro/curriculos/" + id + "/loadCurriculo",
        method: "GET",
        error: function(data) {
            alert("Erro");
            console.log(data.responseJSON.message);
        },
        success: function(data) {
            if (data.erro) {
            } else {
                $("#curriculo").html(data);
                $(".select2").select2();
                $("#dataNascimento").inputmask("dd/mm/yyyy", {
                    placeholder: "dd/mm/yyyy"
                });
                $('input[name="dataEntrada[]"]').inputmask("dd/mm/yyyy", {
                    placeholder: "dd/mm/yyyy"
                });
                $('input[name="dataSaida[]"]').inputmask("dd/mm/yyyy", {
                    placeholder: "dd/mm/yyyy"
                });
                $("[data-mask]").inputmask();
                $(".textarea").wysihtml5();
            }
        }
    });
};

Curriculo.prototype.novoHistorico = function() {
    $.ajax({
        url: "/cadastro/curriculos/moreHistory",
        method: "GET",
        error: function(data) {
            alert("Erro");
            console.log(data.responseJSON.message);
        },
        success: function(data) {
            if (data.erro) {
            } else {
                $("#historico").append(data);
                $("button[name='removeHistory']")
                    .prop("disabled", false)
                    .attr("title", "");
                $('input[name="dataEntrada[]"]').inputmask("dd/mm/yyyy", {
                    placeholder: "dd/mm/yyyy"
                });
                $('input[name="dataSaida[]"]').inputmask("dd/mm/yyyy", {
                    placeholder: "dd/mm/yyyy"
                });
                $(".select2").select2();
            }
        }
    });
};

Curriculo.prototype.minusHistorico = function(element) {
    $(
        $(element)
            .parentsUntil(".row")
            .get(1)
    )
        .parent()
        .remove();
    if ($("button[name='removeHistory']").length == 1) {
        $("button[name='removeHistory']")
            .prop("disabled", true)
            .attr("title", "Necessário ao menos um histórico");
    }
};

Curriculo.prototype.salvarCurriculo = function() {
    token = $('input[name="_token"]').val();
    hotsite.user.salvar(true);
    userId = $("#userId").val();
    console.log(userId);
    // Curriculo data
    objetivoProfissional = $("#objetivoProfissional").val();
    funcoesRealizadas = $("#funcoesRealizadas").val();
    observacoesFinais = $("#observacoesFinais").val();
    curriculoId = $("#curriculoId").val();
    extracurricular = $("#extracurricular").val();

    // Historico data
    empresas = $('input[name="empresa[]"]');
    dataEntrada = $('input[name="dataEntrada[]"]');
    dataSaida = $('input[name="dataSaida[]"]');
    actuations = $('select[name="actuation[]"]');
    cargos = $('input[name="cargo[]"');
    historicoId = $('input[name="historicoId[]"]');

    actuationsValores = [];
    cargoValores = [];
    empresasValores = [];
    dataEntradaValores = [];
    dataSaidaValores = [];
    historicoIdValores = [];

    $.each(empresas, function(index, value) {
        empresasValores[index] = $(value).val();
    });

    $.each(dataEntrada, function(index, value) {
        dataEntradaValores[index] = hotsite.dataBrasilparaDataDB(
            $(value).val()
        );
    });

    $.each(dataSaida, function(index, value) {
        dataSaidaValores[index] = hotsite.dataBrasilparaDataDB($(value).val());
    });

    $.each(actuations, function(index, value) {
        actuationsValores[index] = $(value).val();
    });

    $.each(historicoId, function(index, value) {
        historicoIdValores[index] = $(value).val();
    });

    $.each(cargos, function(index, value) {
        cargoValores[index] = $(value).val();
    });

    var valores = {
        _token: token,
        objetivoProfissional: objetivoProfissional,
        funcoesRealizadas: funcoesRealizadas,
        observacoesFinais: observacoesFinais,
        empresasValores: empresasValores,
        dataEntradaValores: dataEntradaValores,
        dataSaidaValores: dataSaidaValores,
        actuationsValores: actuationsValores,
        cargoValores: cargoValores,
        userId: userId,
        curriculoId: curriculoId,
        historicoIdValores: historicoIdValores,
        extracurricular: extracurricular
    };
    console.log(valores);
    $.ajax({
        url: "/cadastro/curriculos/salvar",
        method: "POST",
        data: valores,
        error: function(data) {
            alert("Erro");
            console.log(data.responseJSON.message);
        },
        // beforeSend: function(){
        // 	$('.overlay').show();
        // },
        success: function(data) {
            if (data.erro) {
            } else {
                // $('.overlay').hide();
                window.location.replace("/cadastro/curriculos");
            }
        }
    });
};

Hotsite.prototype.curriculo = new Curriculo();
