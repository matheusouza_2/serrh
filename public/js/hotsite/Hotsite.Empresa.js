function Empresa() {
};

Empresa.prototype.filtrar = function(page){
	if (page==undefined){
    	page=1;
  	}
	token        	= $('input[name="_token"]').val();
	nome_fantasia 	= $('#nome_fantasia').val();
	razao_social 	= $('#razao_social').val();
	cnpj 		 	= hotsite.limparCaracteres($('#cnpj').val());

	var valores = {
		_token                	: token,
		nome_fantasia 			: nome_fantasia,
		razao_social			: razao_social,
		cnpj 					: cnpj,
		page 					: page,
	}

	$.ajax({
	url: '/cadastro/empresas/filtrar',
		method: "POST",
		data: valores,
		error:function(data){
			alert('Erro');
			console.log(data.responseJSON.message);
		},
		beforeSend: function(){
			$('.overlay').show();
		},
		success: function(data){
			if (data.erro){
				
			} else {
				$('.overlay').hide();
				$("#result").html(data);
			}
		}
	});
}

Empresa.prototype.findEmpresa = function(){
	token        = $('input[name="_token"]').val();
	id       	 = $('#userChoose').val();

	var valores = {
		_token                	: token,
		id           			: id,
	}

	$.ajax({
	url: '/cadastro/users/formById',
		method: "POST",
		data: valores,
		error:function(data){
			alert('Erro');
			console.log(data.responseJSON.message);
		},
		beforeSend: function(){
			$('.overlay').show();
		},
		success: function(data){
			if (data.erro){
				
			} else {
				$('.overlay').hide();
				$("#userInfo").html(data);
				$("#userInfo").addClass("filled");
				hotsite.curriculo.loadCurriculo($('#userId'));				
			}
		}
	});
}

Empresa.prototype.salvar = function(){
	token        	= $('input[name="_token"]').val();
	// Empresa data
	empresaId		= $('#empresaId').val();
	razaoSocial		= $('#razao_social').val();
	nomeFantasia 	= $('#nome_fantasia').val();
	empresarioId	= $('#empresario').val();
	cnpj 			= hotsite.limparCaracteres($('#cnpj').val());
	telefone		= hotsite.limparCaracteres($('#telefone').val());
	cep				= hotsite.limparCaracteres($('#cep').val());
	logradouro		= $('#logradouro').val();
	numero			= $('#numero').val();
	complemento 	= $('#complemento').val();
	bairro			= $('#bairro').val();
	cidade			= $('#cidade').val();
	uf				= $('#uf').val();
	email			= $('#email').val();
	contato 		= $('#contato').val();

	var notAllFilled = hotsite.checkValidation();	

	if(notAllFilled){
		return "Erro";
	}

	var valores = {
		_token          : token,
		empresaId		: empresaId,
		nomeFantasia 	: nomeFantasia,
		razaoSocial		: razaoSocial,
		telefone 		: telefone,
		cep 			: cep, 
		logradouro 		: logradouro,
		numero			: numero,
		complemento		: complemento,
		bairro			: bairro,
		cidade 			: cidade,
		uf 				: uf, 
		cnpj 			: cnpj,
		empresarioId 	: empresarioId,
		email 			: email,
		contato 		: contato,
	}

	$.ajax({
	url: '/cadastro/empresas/salvar',
		method: "POST",
		data: valores,
		error:function(data){
			alert('Erro');
			console.log(data.responseJSON.message);
		},
		// beforeSend: function(){
		// 	$('.overlay').show();
		// },
		success: function(data){
			if (data.errors){
				hotsite.showGeneralModal('error', 'CNPJ existente', data.errors[0]);
			} else {
				window.location.replace("/cadastro/empresas");
			}
		}
	});
}






Hotsite.prototype.empresa = new Empresa();
