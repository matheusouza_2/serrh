function Hotsite() {

};

Hotsite.prototype.checkValidation = function () {
  notAllFilled = false;
  $.each($('[required]'), function (index, value) {
    if ($(value).val() == null || $(value).val() == '') {
      $(value).parent().addClass('required');
      notAllFilled = true;
    }
  });

  return notAllFilled;
}

Hotsite.prototype.checkPasswordConfirmation = function (value1, value2, confirmation) {
  console.log("Verificando " + confirmation + " dos valores " + value1 + " e " + value2);
  return (!confirmation || (value1 != null && value1 != '' && value2 != null && value2 != '' && value1 === value2));
}

Hotsite.prototype.showGeneralModal = function (type, title, body) {
  var $modal = $('#general-modal');
  $modal.find('.modal-title').html(title);
  $modal.find('.modal-body').html(body);
  $modal.removeClass('modal-success').removeClass('modal-warning').removeClass('modal-danger');
  if (type == "success") {
    $modal.addClass('modal-success');
  } else if (type == "error") {
    $modal.addClass('modal-danger');
  } else {
    $modal.addClass('modal-warning');
  }

  $modal.modal('show');
}

Hotsite.prototype.buscaCep = function (elementid, btn) {
  cep = hotsite.limparCaracteres($('#' + elementid).val());
  cepValidacao = cep.replace(/\_/g, '');

  if (cepValidacao != "") {
    $.ajax(
      {
        url: '/cep/' + cep + '/encontracep',
        method: "GET",
        error: function () {
          console.log("Teste")
        },
        beforeSend: function () {
          $(btn).html("<i class='fa fa-refresh fa-spin'></i>")
        },
        success: function (data) {
          result = JSON.parse(data);
          $(btn).html('<i class="fa fa-search"></i> Buscar CEP');
          if (result.erro) {
            $('#' + elementid).parentsUntil(".form-group").parent().addClass('required');
            $('#' + elementid + 'Error').show();
          } else {
            if (result.logradouro == '' || result.logradouro == undefined) {
              $('#logradouro').prop('disabled', false)
            }
            if (result.bairro == '' || result.bairro == undefined) {
              $('#bairro').prop('disabled', false)
            }
            if (result.cidade == '' || result.cidade == undefined) {
              $('#cidade').prop('disabled', false)
            }
            if (result.uf == '' || result.uf == undefined) {
              $('#uf').prop('disabled', false)
            }
            $('#logradouro').val(result.logradouro);
            $('#bairro').val(result.bairro);
            $('#cidade').val(result.localidade);
            $("#uf").val($("#uf option:contains(" + result.uf + ")").val()).change();

            $('#' + elementid).parentsUntil(".form-group").parent().removeClass('required');
            $('#' + elementid + 'Error').hide();
          }
        }
      });
  }
};

Hotsite.prototype.buscaCnpj = function (elementid, btn) {
  cnpj = hotsite.limparCaracteres($('#' + elementid).val());
  cnpjValidacao = cnpj.replace(/\_/g, '');
  if (cnpjValidacao != "") {
    $(btn).html("<i class='fa fa-refresh fa-spin'></i>")
    $.getJSON("http://www.receitaws.com.br/v1/cnpj/" + cnpj + "?callback=?", function (data) {
          $(btn).html('<i class="fa fa-search"></i> Buscar CNPJ');
          if (data.erro) {
            $('#' + elementid).parentsUntil(".form-group").parent().addClass('required');
            $('#' + elementid + 'Error').show();
          } else {
            if (data.logradouro == '' || data.logradouro == undefined) {
              $('#logradouro').prop('readonly', false)
            }
            if (data.bairro == '' || data.bairro == undefined) {
              $('#bairro').prop('readonly', false)
            }
            if (data.cidade == '' || data.cidade == undefined) {
              $('#cidade').prop('readonly', false)
            }
            if (data.uf == '' || data.uf == undefined) {
              $('#uf').prop('readonly', false)
            }
            $('#razao_social').val(data.nome);
            $('#nome_fantasia').val(data.fantasia);
            $('#cnpj').val(data.cnpj);
            $('#cep').val(data.cep);
            $('#logradouro').val(data.logradouro);
            $('#numero').val(data.numero);
            $('#bairro').val(data.bairro);
            $('#cidade').val(data.municipio);
            $('#contato').val(data.telefone);
            $("#uf").val($("#uf option:contains(" + data.uf + ")").val()).change();

            $('#' + elementid).parentsUntil(".form-group").parent().removeClass('required');
            $('#' + elementid + 'Error').hide();
          }
      });
  }
}

Hotsite.prototype.limparCaracteres = function (value) {
  return value.replace(/\./g, '').replace(/\//g, '').replace(/\-/g, '').replace(/\(/g, '').replace(/\)/g, '').replace(/\-/g, '').replace(/ /g, '');
}

Hotsite.prototype.dataBDParaDataBrasil = function (data) {
  if (data == null || data == "") {
    return "";
  }

  data = data.split(' ')[0];
  return data.split('-')[2] + "/" + data.split('-')[1] + "/" + data.split('-')[0];
};

Hotsite.prototype.dataBrasilparaDataDB = function (data) {
  if (data == '') {
    return data;
  } else {
    return data.split('/')[2] + "-" + data.split('/')[1] + "-" + data.split('/')[0];
  }
}

var hotsite = new Hotsite();