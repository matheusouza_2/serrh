function User() {}

User.prototype.filtrar = function(page) {
    if (page == undefined) {
        page = 1;
    }
    token = $('input[name="_token"]').val();
    name = $("#name").val();
    cpf = hotsite.limparCaracteres($("#cpf").val());
    perfil = $("#perfis").val();

    var valores = {
        _token: token,
        name: name,
        cpf: cpf,
        perfil: perfil,
        page: page
    };

    $.ajax({
        url: "/cadastro/users/filtrar",
        method: "POST",
        data: valores,
        error: function(data) {
            alert("Erro");
            console.log(data.responseJSON.message);
        },
        beforeSend: function() {
            $(".overlay").show();
        },
        success: function(data) {
            if (data.erro) {
            } else {
                $(".overlay").hide();
                $("#result").html(data);
            }
        }
    });
};

User.prototype.findUser = function() {
    token = $('input[name="_token"]').val();
    id = $("#userChoose").val();

    var valores = {
        _token: token,
        id: id
    };

    $.ajax({
        url: "/cadastro/users/formById",
        method: "POST",
        data: valores,
        error: function(data) {
            alert("Erro");
            console.log(data.responseJSON.message);
        },
        beforeSend: function() {
            $(".overlay").show();
        },
        success: function(data) {
            if (data.erro) {
            } else {
                $(".overlay").hide();
                $("#userInfo").html(data);
                $("#userInfo").addClass("filled");
                hotsite.curriculo.loadCurriculo($("#userId"));
            }
        }
    });
};

User.prototype.activePassword = function(element) {
    $("#password").prop("disabled", !$(element).is(":checked"));
    $("#confirmPassword").prop("disabled", !$(element).is(":checked"));
};

User.prototype.salvar = function(isFromCurriculo) {
    token = $('input[name="_token"]').val();
    // User data
    userId = $("#userId").val();
    nome = $("#nome").val();
    email = $("#email").val();
    cpf = hotsite.limparCaracteres($("#cpf").val());
    dataNascimento = hotsite.dataBrasilparaDataDB($("#dataNascimento").val());
    telefone = hotsite.limparCaracteres($("#telefone").val());
    celular = hotsite.limparCaracteres($("#celular").val());
    cep = hotsite.limparCaracteres($("#cep").val());
    logradouro = $("#logradouro").val();
    numero = $("#numero").val();
    complemento = $("#complemento").val();
    bairro = $("#bairro").val();
    cidade = $("#cidade").val();
    uf = $("#uf").val();
    login = $("#login").val();
    senha = $("#password:not([disabled])").val();
    confirmacao = $("#confirmPassword:not([disabled])").val();
    changeSenha = $("#confirmChange").is(":checked");
    sexo = $("#sexo").val();
    perfil = $("#perfil").val();
    escolaridade = $("#escolaridade").val();
    civilstate = $("#civilstate").val();
    formacao = $("#formacao").val();
    console.log(civilstate);
    var notAllFilled = hotsite.checkValidation();
    var passwordCheck = hotsite.checkPasswordConfirmation(
        senha,
        confirmacao,
        changeSenha
    );

    if (!passwordCheck) {
        $("#confirmPassword:not([disabled])")
            .parent()
            .addClass("required");
        $("#password:not([disabled])")
            .parent()
            .addClass("required");
    }

    if (notAllFilled || !passwordCheck) {
        return "Erro";
    }

    var valores = {
        _token: token,
        userId: userId,
        nome: nome,
        email: email,
        dataNascimento: dataNascimento,
        telefone: telefone,
        celular: celular,
        cep: cep,
        logradouro: logradouro,
        numero: numero,
        complemento: complemento,
        bairro: bairro,
        cidade: cidade,
        uf: uf,
        login: login,
        senha: senha,
        confirmacao: confirmacao,
        cpf: cpf,
        sexo: sexo,
        perfil: perfil,
        escolaridade: escolaridade,
        formacao: formacao,
        civilstate: civilstate
    };

    $.ajax({
        url: "/cadastro/users/salvar",
        method: "POST",
        data: valores,
        error: function(data) {
            alert("Erro");
            console.log(data.responseJSON.message);
        },
        // beforeSend: function(){
        // 	$('.overlay').show();
        // },
        success: function(data) {
            if (data.erro) {
            } else {
                if (isFromCurriculo) {
                    $("#userId").val(parseInt(data));
                } else {
                    window.location.replace("/cadastro/usuarios");
                }
            }
        }
    });
};

User.prototype.changePassword = function() {
    token = $('input[name="_token"]').val();
    senha = $("#password").val();
    confirmacaoSenha = $("#confirmPassword").val();

    var passwordCheck = hotsite.checkPasswordConfirmation(
        senha,
        confirmacaoSenha,
        true
    );
    if (!passwordCheck) {
        $("#confirmPassword:not([disabled])")
            .parent()
            .addClass("required");
        $("#password:not([disabled])")
            .parent()
            .addClass("required");
        return "Error";
    }

    $.ajax({
        url: "/alterarsenha",
        method: "POST",
        data: {
            _token: token,
            senha: senha,
            confirmacaoSenha: confirmacaoSenha
        },
        error: function(data) {
            alert("Erro");
            console.log(data.responseJSON.message);
        },
        // beforeSend: function(){
        // 	$('.overlay').show();
        // },
        success: function(data) {
            if (data.erro) {
            } else {
                window.location.replace("/home");
            }
        }
    });
};

Hotsite.prototype.user = new User();
