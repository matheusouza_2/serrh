function Vaga() { }

Vaga.prototype.filtrar = function (page) {
    if (page == undefined) {
        page = 1;
    }

    token = $('input[name="_token"]').val();
    actuation = $("#actuation").val();
    status = $("#statusvaga").val();
    empresa = $("#empresas").val();
    name = $("#name").val();

    var valores = {
        _token: token,
        actuation: actuation,
        status: status,
        empresa: empresa,
        page: page,
        name: name
    };

    $.ajax({
        url: "/vagas/filtrar",
        method: "POST",
        data: valores,
        error: function (data) {
            alert("Erro");
            console.log(data.responseJSON.message);
        },
        beforeSend: function () {
            $(".overlay").show();
        },
        success: function (data) {
            if (data.erro) {
            } else {
                $(".overlay").hide();
                $("#result").html(data);
            }
        }
    });
};

Vaga.prototype.filtrarIntegrada = function (page) {
    if (page == undefined) {
        page = 1;
    }

    token = $('input[name="_token"]').val();
    name = $("#name").val();
    rp = $("#rp").val();

    var valores = {
        _token: token,
        rp: rp,
        name: name,
        page: page
    };

    $.ajax({
        url: "/vagasintegradas/filtrar",
        method: "POST",
        data: valores,
        error: function (data) {
            alert("Erro");
            console.log(data.responseJSON.message);
        },
        beforeSend: function () {
            $(".overlay").show();
        },
        success: function (data) {
            if (data.erro) {
            } else {
                $(".overlay").hide();
                $("#result").html(data);
            }
        }
    });
};

Vaga.prototype.filtrarCandidato = function () {
    token = $('input[name="_token"]').val();
    candidatos = $("#candidatos").val();
    vagaId = $("#vaga_id").val();

    var valores = {
        _token: token,
        candidatos: candidatos,
        vagaId: vagaId
    };

    $.ajax({
        url: "/vagas/filtrarCandidato",
        method: "POST",
        data: valores,
        error: function (data) {
            alert("Erro");
            console.log(data.responseJSON.message);
        },
        beforeSend: function () {
            $(".overlay").show();
        },
        success: function (data) {
            if (data.erro) {
            } else {
                $(".overlay").hide();
                $("#candidatosFilter").html(data);
            }
        }
    });
};

Vaga.prototype.candidatar = function (id) {
    token = $('input[name="_token"]').val();

    var valores = {
        _token: token,
        id: id
    };

    $.ajax({
        url: "/vagas/candidatar",
        method: "POST",
        data: valores,
        error: function (data) {
            alert("Erro");
            console.log(data.responseJSON.message);
        },
        beforeSend: function () {
            $(".overlay").show();
        },
        success: function (data) {
            if (data.erro) {
            } else {
                window.location.replace("/vagas");
            }
        }
    });
};

Vaga.prototype.salvar = function (isFromCurriculo) {
    token = $('input[name="_token"]').val();
    // Vaga data
    vagaId = $("#vagaId").val();
    titulo_vaga = $("#titulo_vaga").val();
    empresa = $("#empresa").val();
    tiporecrutamento = $("#tiporecrutamento").val();
    actuation = $('select[name="actuation[]"]').val();
    quantidade = $("#quantidade").val();
    data_fechamento = hotsite.dataBrasilparaDataDB($("#data_fechamento").val());
    data_admissao = hotsite.dataBrasilparaDataDB($("#data_admissao").val());
    statusvaga = $("#statusvaga").val();
    escolaridade = $("#escolaridade").val();
    idade_minima = $("#idade_minima").val();
    idade_maxima = $("#idade_maxima").val();
    salario_inicial = $("#salario_inicial").val();
    observacoes_vaga = $("#observacoes_vaga").val();
    observacoes_candidato = $("#observacoes_candidato").val();
    show_company = $("#show_company").prop('checked') ? 1 : 0;

    salario_inicial = salario_inicial.replace(",", ".");

    rp = $("#rp").val();
    marcacaosexo = $("#marcacaosexo").val();
    only_city = $("#only_city").prop('checked') ? 1 : 0;
    horario_trabalho = $("#horario_trabalho").val();
    beneficios = $("#beneficios").val();
    plano_pagamento = $('#plano_pagamento').val();

    var notAllFilled = hotsite.checkValidation();

    if (notAllFilled) {
        return "Erro";
    }

    var valores = {
        _token: token,
        vagaId: vagaId,
        titulo_vaga: titulo_vaga,
        empresa: empresa,
        tiporecrutamento: tiporecrutamento,
        actuation: actuation,
        quantidade: quantidade,
        data_fechamento: data_fechamento,
        data_admissao: data_admissao,
        escolaridade: escolaridade,
        idade_minima: idade_minima,
        idade_maxima: idade_maxima,
        salario_inicial: salario_inicial,
        observacoes_vaga: observacoes_vaga,
        observacoes_candidato: observacoes_candidato,
        statusvaga: statusvaga,
        rp: rp,
        marcacaosexo: marcacaosexo,
        only_city: only_city,
        show_company: show_company,
        horario_trabalho: horario_trabalho,
        beneficios: beneficios,
        plano_pagamento: plano_pagamento
    };
    console.log(valores);
    $.ajax({
        url: "/vagas/salvar",
        method: "POST",
        data: valores,
        error: function (data) {
            alert("Erro");
            console.log(data.responseJSON.message);
        },
        // beforeSend: function(){
        // 	$('.overlay').show();
        // },
        success: function (data) {
            if (data.erro) {
            } else {
                window.location.replace("/vagas");
            }
        }
    });
};

Vaga.prototype.toggleApiAvailable = function (id) {
    token = $('input[name="_token"]').val();

    var valores = {
        _token: token,
        id: id
    };

    $.ajax({
        url: "/vagasintegradas/toggleApiAvailable",
        method: "POST",
        data: valores,
        error: function (data) {
            alert("Erro");
            console.log(data.responseJSON.message);
        },
        beforeSend: function () {
        },
        success: function (data) {
            if (data.erro) {
            } else {
                hotsite.vaga.filtrarIntegrada();
            }
        }
    });
};

Hotsite.prototype.vaga = new Vaga();
