@extends('layouts.appexterno')
<style>
    .login-form{
        background-color: rgba(255, 255, 255, 0.4);
        padding: 10px;
    }
    .login-form{
        position: relative;
    }
    .login-form label{
       margin-top: 12px;
    }
    .login-form input, .login-form .input-group-addon{
        background: rgba(255, 255, 255, 0.1);
        border: 0;
        border-bottom: 1px solid #ccc;
    }
</style>
@section('content')
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    {{-- <b>SER</b>ONLINE --}}
    <img src={{ asset('img/logo.png') }} alt="Logo" class="img img-responsive img-fluid">
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <div class="login-box-msg">Login</div>

    <form action="{{ route('login') }}" method="post">
        @csrf
      <div class="form-group has-feedback">
        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="E-mail" required autofocus>
        <span class="fa fa-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Senha" required>
        @if ($errors->has('login'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('login') }}</strong>
            </span>
        @endif
        <span class="fa fa-lock form-control-feedback"></span>
      </div>
      <div class="row">
<!--         <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox"> Remember Me
            </label>
          </div>
        </div> -->
        <!-- /.col -->
        <div class="col-xs-6">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Logar</button>
        </div>
        <div class="col-xs-6" style="padding-top: 7px">
            <a href="/password/reset"><i class="fa fa-question-circle"></i> Esqueci a senha</a>
        </div>
        <!-- /.col -->
      </div>
      <div class="row">
          <div class="col-xs-12 col-md-12">
            Não possui cadastro? <a href="/register">Comece por aqui!</a>
          </div>
      </div>
    </form>

    <!-- <div class="social-auth-links text-center">
      <p>- OR -</p>
      <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using
        Facebook</a>
      <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using
        Google+</a>
    </div> -->
    <!-- /.social-auth-links -->

    <!-- <a href="#">I forgot my password</a><br>
    <a href="register.html" class="text-center">Register a new membership</a> -->

  </div>
  <!-- /.login-box-body -->
</div>

<!--  -->
<!-- <div class="box box-solid box-login">
    <div class="box-header without-border">
        <h1 class="box-title"><b>Sistema</b> RH</h1>
    </div>

    <form method="POST" action="{{ route('login') }}" class="form-horizontal" aria-label="{{ __('Login') }}">
        @csrf
        <div class="form-group row">
            <label for="login" class="col-sm-2 col-form-label text-md-right">{{ __('Login') }}</label>

            <div class="col-md-10">
                <input id="login" type="text" class="form-control{{ $errors->has('login') ? ' is-invalid' : '' }}" name="login" value="{{ old('login') }}" required autofocus>

                @if ($errors->has('login'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('login') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <label for="password" class="col-md-2 col-form-label text-md-right">{{ __('Senha') }}</label>

            <div class="col-md-10">
                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="row">
            <div class="col-md-8"></div>
            <div class="col-md-4">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                    <label class="form-check-label" for="remember">
                        {{ __('Lembrar de mim') }}
                    </label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <button type="submit" class="btn btn-primary btn-send">
                    {{ __('Entrar') }}
                </button>

                <a class="btn btn-link" href="{{ route('password.request') }}">
                    {{ __('Esqueceu a senha?') }}
                </a>
            </div>
        </div>
        <div style="margin-top: 10px">
            Não possui cadastro, <a href="/cadastro">registre-se aqui</a>.
        </div>
    </form>

</div> -->
@endsection
