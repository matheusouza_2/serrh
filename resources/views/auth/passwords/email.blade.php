@extends('layouts.appexterno')
<style>
    .login-form{
        background-color: rgba(255, 255, 255, 0.4);
        padding: 10px;
    }
    .login-form{
        position: relative;
    }
    .login-form label{
       margin-top: 12px;
    }
    .login-form input, .login-form .input-group-addon{
        background: rgba(255, 255, 255, 0.1);
        border: 0;
        border-bottom: 1px solid #ccc;
    }
</style>
@section('content')
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <b>SER</b>ONLINE
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    @if (request()->session()->has('status'))
        <span class="valid-feedback" style="color: green" role="alert">
            <strong>Enviamos o link para seu e-mail.</strong>
        </span>
        <br/>
        <a href="{{ url('/') }}">Retornar a tela de login</a>
    @else
        <div class="login-box-msg">Digite o e-mail para enviarmos o link de alteração de senha.</div>
        <form method="POST" action="{{ route('password.email') }}" aria-label="{{ __('Reset Password') }}">
            @csrf

            <div class="form-group row">
                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-mail') }}</label>

                <div class="col-md-8">
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif

                    


                </div>
            </div>

            <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                    <button type="submit" class="btn btn-primary">
                        {{ __('Enviar link por e-mail') }}
                    </button>
                </div>
            </div>
        </form>
    @endif
     

  </div>
  <!-- /.login-box-body -->
</div>
@endsection
