@extends('layouts.appexterno')
<style>
    /*.explanation{
        background: rgba(255,255,255,0.2);
        padding: 10px;
        margin: 10px;
        color: #666;
    }
    .explanation .title{
        font-size: 2.5em;
        text-align: center;
        margin-bottom: 10px;
    }
    .explanation ul{
        list-style: none;
        display: flex;
        justify-content: space-between;
    }
    .explanation li {
        display: inherit;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        text-align: center
    }
    .explanation li p{
        margin: 0;
    }
    .explanation li p:first-child{
        font-size: 3em;
        border: 5px solid #666;
        border-radius: 50%;
        width: 1.75em;
    }
    .explanation li p:nth-child(2){
        font-size: 1.5em;
    }
    .explanation li p:last-child{
        font-style: italic;
        font-size: 0.9em;
    }*/
    .register-box{
        width: 50% !important;
        min-width: 360px;
    }
</style>
@section('content')
<!-- <section class="content-header">
  <h1>
    Cadastro
    <small>Crie seu cadastro</small>
<<<<<<< HEAD
  </h1>  
=======
  </h1>
>>>>>>> 101b385b7dfcec3291831129b2bb46da27b29b0f
</section>
<section class="explanation">
    <div class="title">Crie seu cadastro em 3 passos</div>
    <div class="body">
        <ul>
            <li>
                <p>1</p>
                <p>Informações da conta</p>
                <p>Preenchimento de informações essenciais para que possa usufruir da nossa plataforma.</p>
            </li>
            <li>
                <p>2</p>
                <p>Informações residenciais</p>
                <p>Preenchimento de informações para que nossa plataforma possa encontrar as melhores oportunidades próximas a você.</p>
            </li>
            <li>
                <p>3</p>
                <p>Informações pessoais</p>
                <p>Preencha dados para encontrarmos vagas que se adequem a você.</p>
            </li>
        </ul>
    </div>
</section> -->
<!-- <div class="box box-solid box-register">
    <div class="box-header without-border">
        <h1 class="box-title"><b>Sistema</b> RH - Cadastro</h1>
    </div>
    <div id="parte1">
        <div class="box-subtitle">Informações pessoais</div>
        <form method="POST" action="{{ route('register') }}" class="form-horizontal" aria-label="{{ __('Register') }}">
            @csrf
<<<<<<< HEAD
            
=======

>>>>>>> 101b385b7dfcec3291831129b2bb46da27b29b0f
            <div class="row">
                <div class="form-group row">
                    <label for="login" class="col-md-4 col-form-label text-md-right">{{ __('Login') }}</label>

                    <div class="col-md-8">
                        <input id="login" type="text" class="form-control{{ $errors->has('login') ? ' is-invalid' : '' }}" name="login" value="{{ old('login') }}" required autofocus>

                        @if ($errors->has('login'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('login') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nome') }}</label>

                    <div class="col-md-8">
                        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                        @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-mail') }}</label>

                    <div class="col-md-8">
                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Senha') }}</label>

                    <div class="col-md-8">
                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirmação senha') }}</label>

                    <div class="col-md-8">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                    </div>
                </div>
            </div>

            <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                    <button type="submit" class="btn btn-primary btn-send">
                        {{ __('Registrar') }}
                    </button>
                </div>
            </div>
        </form>
    </div>
</div> -->
<!-- </section> -->
<body class="hold-transition register-page">
    <div class="register-box">
        <div class="register-logo">
            <b>SER</b>Online
        </div>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade active in" id="home" role="tabpanel" aria-labelledby="home-tab">
                <div class="register-box-body">
                    <p class="login-box-msg"><strong style="font-size: 1.3em">Candidate-se a vagas!</strong> <br/> Comece pelo seu cadastro.</p>
                    <div class="error-register-area alert alert-danger alert-dismissible" style="display: {{ $errors->count() > 0 ? 'block' : 'none' }}">
                        <h4><i class="fa fa-warning"></i> Ops! Algo está errado...</h4>
                        @foreach($errors->all() as $error)
                            <p>{{ $error }}</p>
                        @endforeach
                    </div>
                    <form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}">

                        @csrf        

                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group has-feedback">
                                            <label>Nome Completo</label>
                                            <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group has-feedback">
                                            <label>E-mail</label>
                                            <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group has-feedback">
                                            <label>Telefone</label>
                                            <input type="text" class="form-control" data-inputmask='"mask": "(99) 99999-9999", "clearIncomplete" : true' data-mask id="telefone" name="telefone" value="{{ old('telefone') }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <!-- <div class="col-md-5">
                                        <div class="form-group has-feedback">
                                            <label>Login</label>
                                            <input type="text" class="form-control" pid="login" name="login" value="{{ old('login') }}">
                                        </div>
                                    </div> -->
                                    <div class="col-md-4">
                                        <div class="form-group has-feedback">
                                            <label>CPF</label>
                                            <input type="text" class="form-control" d="cpf" name="cpf"  data-inputmask='"mask": "999.999.999-99", "clearIncomplete" : true' data-mask value="{{ old('cpf') }}">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group has-feedback">
                                            <label>Data de Nascimento</label>
                                            <input type="text" class="form-control" id="data_nascimento" name="data_nascimento" value="{{ old('data_nascimento') }}">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group has-feedback">
                                            <label for="empresario">Empresário</label>
                                            <br>
                                            <input type="radio" name="empresario" value="y" id="">
                                            <label for="">Sim</label>
                                            <input type="radio" name="empresario" id="" class="ml-4" checked>
                                            <label for="">Não</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group has-feedback">
                                            <label>Senha</label>
                                            <input type="password" class="form-control"id="password" name="password">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group has-feedback">
                                            <label>Confirme a senha</label>
                                            <input type="password" class="form-control" id="password_confirmation" name="password_confirmation">
                                        </div>
                                    </div>
                                </div>  
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-8">
                                <!-- <div class="checkbox icheck">
                                    <label>
                                    <input type="checkbox"> I agree to the <a href="#">terms</a>
                                    </label>
                                </div> -->
                            </div>
                            <!-- /.col -->
                            <div class="col-xs-4">
                                <button type="submit" class="btn btn-primary btn-block btn-flat">Cadastrar-se</button>
                            </div>
                            <!-- /.col -->
                        </div>
                    </form>
                    <a href="/login" class="text-center">Já tenho cadastro</a>
                </div>
                <!-- /.form-box -->            
            </div>
        </div>
    </div>
    <!-- /.register-box -->
</body>
@endsection

@section('scripts')
    <script src="{{ asset('js/hotsite/Hotsite.Empresa.js') }}"></script>
    <script>
        $('[data-mask]').inputmask();
        $('#data_nascimento').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' });
    </script>
@endsection
