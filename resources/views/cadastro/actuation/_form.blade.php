<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label>Nome</label>
            <div class="input-group">
                <input type="text" class="form-control" name="actuation" id="actuation" placeholder="Nova área de atuação" required onkeyup="hotsite.actuation.viabilityChange()" value="{{ $actuation->descricao }}"/> 
                <input type="hidden" id="id" name="id" value="{{ $actuation->id }}" />
                <span class="input-group-btn">
                    <button class="btn btn-primary" id="viabilityButton" onclick="hotsite.actuation.checkViability(this)"><i class="fa fa-search"></i> Verificar disponibilidade</button>
                </span>
            </div>
            <span class="aviso"></span>                              
        </div>
    </div>
</div>
