<table class="table table-striped table-responsive">
    <thead>
        <tr>
          <th style="width: 10px">ID</th>
          <th>Área de Atuação</th>
          <th>Status</th>
          <th></th>
        </tr>
    </thead>
    <tbody id="result">
    	@foreach($actuations as $actuation)
			<tr>
				<td>{{$actuation->id}}</td>
				<td>{{$actuation->descricao}}</td>
				<td>
					@if($actuation->aprovada)
						<i class="fa fa-check-circle text-success"></i> Aprovado
					@else
						<i class="fa fa-times-circle text-danger"></i> Não aprovado
					@endif
				</td>
				<td>
					<a href="javascript:void(0)" onclick="hotsite.actuation.edit({{ $actuation->id }}, true)">
						<i class="fa fa-edit"></i>
					</a>
					<a href="javascript:void(0)" onclick="hotsite.actuation.changeStatus({{ $actuation->id }})">
						@if($actuation->aprovada)
							<i class="fa fa-times"></i>
						@else
							<i class="fa fa-check"></i>				
						@endif
					</a>
					<a href="javascript:void(0)" onclick="hotsite.actuation.delete({{ $actuation->id }})">
						<i class="fa fa-trash"></i>
					</a>
				</td>
			</tr>
		@endforeach
    </tbody>
</table>
<div class="pagination-area">
{{ $actuations->links() }}
</div>

<script>
  $(document).ready(function() {
    $('.pagination a').on('click', function (event) {
        event.preventDefault();
        if ( $(this).attr('href') != '#' ) {
            url = $(this).attr('href');
            page = url.split('page=')[1];
            $('#pagina').val(page);
            hotsite.actuation.filtrar(page);
        }
    });
  });
</script>