@extends('layouts.appinterno')

@section('content')
<section class="content-header">
  <h1>
    Área de atuação
    <small>
        @if(Auth::user()->perfil->identificador == "Candidato")
        Meu currículo
        @else
        Todas áreas
        @endif
    </small>
</h1>  
</section>
<section class="content">
    @if(Auth::user()->perfil->identificador == "Candidato")

    @else
    @csrf
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="box box-primary collapsed-box">
                <div class="box-header with-border">
                    <h3 class="box-title">Filtros</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Área de atuação</label>
                                <input type="text" class="form-control" name="name" id="name" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="col-md-12">
                        <button class="btn btn-primary pull-right" onclick="hotsite.actuation.filtrar()">
                            <i class="fa fa-search"></i> Filtrar
                        </button>
                    </div>
                </div>

            </div>
        </div>
    </div>
<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Lista de áreas de atuação</h3>
                <div class="box-tools pull-right">
                    <a href="javascript:void(0)" onclick="hotsite.actuation.edit('new', true)" class="btn btn-box-tool btn-box-tool-add"><i class="fa fa-plus"></i> Nova área
                    </a>
                </div>
            </div>
            <div class="box-body no-padding">
                <div class="overlay" style="display: block">
                    <i class="fa fa-refresh fa-spin"></i>
                </div>
                <div id="result"></div>
            </div>
            <!-- <div class="box-footer clearfix"> -->
                <!-- <ul class="pagination pagination-sm no-margin pull-right">
                    <li><a href="#">&laquo;</a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">&raquo;</a></li>
                </ul> -->
            <!-- </div> -->
        </div>
    </div>
</div>

@endif
</section>

@endsection


@section('scripts')
<script src="{{ asset('js/hotsite/Hotsite.Actuation.js') }}"></script>
<script>
    $('.select2').select2();
    hotsite.actuation.filtrar();
</script>
@endsection
