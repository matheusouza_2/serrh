<div class="modal-dialog">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title">Inclusão rápida de área de atuação</h4>
    </div>
    <div class="modal-body">
      @include('cadastro.actuation._form')
    </div>
    <div class="modal-footer">
      <input type="hidden" id="modalCargo" name="modalCargo" value="modal"/>
      <input type="hidden" id="originCargo" name="originCargo" value=""/>
      <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
      <button type="button" class="btn btn-primary btn-flat" id="btnCargoSave" onclick="hotsite.actuation.salvar()" disabled>Criar área</button>
    </div>
  </div>
  <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->