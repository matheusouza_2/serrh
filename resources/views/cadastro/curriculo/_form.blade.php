@extends('layouts.appinterno')

@section('content')
<section class="content-header">
  <h1>
    Currículo
    <small>Formulário</small>
    <div class="pull-right">
        <a class="btn btn-flat btn-primary" href="{{ URL::previous() }}"><i class="fa fa-arrow-left"></i> Voltar</a>
    </div>
</h1>
</section>
<section class="content">
    @csrf

    @if(session('warning'))
        <div class="row">
            <div class="col-md-12">
                <div style="
                    padding: 1em;
                    margin-bottom: 0.75em;
                    background-color: #FDD835;
                    border-bottom: 4px solid #CCAE28;
                    border-radius: 5px;
                    display: flex;
                    align-items: center;
                ">
                    <i class="fa fa-exclamation-triangle" style="font-size: 1.25em"></i>
                    <span style="margin-left: 1em">{{session('warning')}}</span>
                </div>
            </div>
        </div>
    @endif

    @include('cadastro.curriculo._formUser')
    @include('cadastro.curriculo._formCurriculoBase')

    <div class="row">
        <div class="col-md-12">
            <button class="btn btn-primary pull-right" onclick="hotsite.curriculo.salvarCurriculo()">
                <i class="fa fa-save"></i> Salvar
            </button>
        </div>
    </div>
</section>

@endsection


@section('scripts')
    <script src="{{ asset('js/hotsite/Hotsite.Curriculo.js') }}"></script>
    <script src="{{ asset('js/hotsite/Hotsite.User.js') }}"></script>
    <script src="{{ asset('js/hotsite/Hotsite.Actuation.js') }}"></script>
    <script>
        $('.select2').select2();
        $('#userChoose').change();
    </script>
@endsection
