<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Objetivo Profissional</label>
                    <input type="text" class="form-control" name="objetivoProfissional" id="objetivoProfissional" value="{{ $user->curriculo->objetivoProfissional }}" required/>
                </div>
            </div>
        </div>
        <input type="hidden" name="curriculoId" id="curriculoId" value="{{ $user->curriculo->id }}" />
        <div class="row">
            <div class="col-md-12">
                <h4>Histórico de empresas e áreas de atuação
                    <div class="box-tools pull-right">
                        <a href="javascript:void(0)" class="btn btn-box-tool" onclick="hotsite.curriculo.novoHistorico()"><i class="fa fa-plus"></i> Novo histórico
                        </a>
                    </div>
                </h4>
            </div>
            <div class="col-md-12" id="historico">
                @php $allHistory = $user->curriculo->historico->obterHistoricoCompleto(); @endphp
                @foreach($allHistory as $historico)
                    <div class="row" name="historicoInfo[]">
                        <input type="hidden" name="historicoId[]" value="{{ $historico->id }}" />
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Empresa</label>
                                <input type="text" class="form-control" name="empresa[]" value="{{ $historico->empresa }}" placeholder="Empresa" required/>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Cargo</label>
                                <input type="text" class="form-control" name="cargo[]" placeholder="Cargo" value="{{ $historico->cargo }}"/>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <label>Área de atuação</label>
                            <!-- <input type="text" class="form-control" name="actuation[]" value="{{ $historico->empresa }}" required/> -->
                            <select class="form-control select2" name="actuation[]" data-placeholder="Cargo" style="width: 100%;" onchange="hotsite.actuation.checkActuationSelect(this)" required >
                                <option></option>
                                <!-- <option value="new">+ Novo actuation</option> -->
                                @foreach($actuations as $actuation)
                                    <option value="{{$actuation->id}}" {{ $historico->actuation_id == $actuation->id ? 'selected' : '' }}>{{$actuation->descricao}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Data Entrada</label>
                                <input type="text" class="form-control" name="dataEntrada[]" value="{{ $historico->data_entrada }}" required/>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Data Saída</label>
                                <input type="text" class="form-control" name="dataSaida[]" value="{{ $historico->data_saida }}"/>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group">
                                <label>Retirar</label>
                                <button href="javascript:void(0)" name="removeHistory" class="btn btn-circle btn-danger"
                                    onclick="hotsite.curriculo.minusHistorico(this)"
                                    {{ count($allHistory) == 1 ? 'disabled' : '' }}
                                    title="{{ count($allHistory) == 1 ? 'Necessário ao menos um histórico' : '' }}">
                                    <i class="fa fa-minus-circle"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Funções realizadas</label>
                    <form>
                        <textarea class="textarea" id="funcoesRealizadas" name="funcoesRealizadas" placeholder="Descreva aqui as funções já realizadas"
                      style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" value="">{{ $user->curriculo->funcoesRealizadas }}</textarea>
                    </form>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Extracurriculares</label>
                    <form>
                        <textarea class="textarea" id="extracurricular" name="extracurricular" placeholder="Descreva aqui as atividades extracurriculares"
                      style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" value="">{{ $user->curriculo->extracurricular }}</textarea>
                    </form>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Observações Finais</label>
                    <input type="text" class="form-control" id="observacoesFinais" name="observacoesFinais"  value="{{ $user->curriculo->observacoesFinais }}"/>
                </div>
            </div>
        </div>


    </div>
</div>
