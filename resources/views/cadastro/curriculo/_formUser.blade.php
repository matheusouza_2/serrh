@php
    $isCandidato = (Auth::user() != null && Auth::user()->perfil->identificador == "Candidato");
@endphp
<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Informações do usuário</h3>
                <div class="box-tools pull-right">
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Candidato</label>
                            <select class="form-control select2" name="userChoose" id="userChoose" data-placeholder="Escolha um candidato" style="width: 100%;" onchange="hotsite.user.findUser()" {{ $inicial == null ? '' : 'disabled' }}>
                                <option></option>
                                <option value="new" {{ $inicial == null ? 'selected' : '' }}>+ Novo candidato</option>
                                @if($isCandidato)
                                    <option value="{{ $main->id }}" selected>{{ $main->nome }} - {{ CustomFuncs::formatarCpf($main->cpf) }}</option>
                                @else
                                    @foreach($users as $user)
                                        <option value="{{$user->id}}" {{ $main->id == $user->id ? 'selected' : '' }}>{{$user->nome}} - {{ CustomFuncs::formatarCpf($user->cpf) }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div id="userInfo"></div>
            </div>
        </div>
    </div>
</div>
