<table class="table table-striped table-responsive">
    <thead>
        <tr>
          <th style="width: 10px">ID</th>
          <th>Nome</th>
          <th>Objetivo Profissional</th>
          <th>Último cargo e área de atuação</th>
          <th>Contato</th>
          <th>Endereço</th>
          <th>Data de Criação</th>
          <th></th>
        </tr>
    </thead>
    <tbody>

    	@if(count($curriculos) == 0)
			<tr><td colspan="7" class="text-center">Não encontrado nenhum currículo</td></tr>
		@else
			@foreach($curriculos as $curriculo)
			<tr>
				<td>{{$curriculo->id}}</td>
				<td>{{$curriculo->candidato->nome}}</td>
				<td>{{$curriculo->objetivoProfissional}}</td>
				<td>{{$curriculo->historico->obterHistoricoCompleto()->first()->cargo}} - {{$curriculo->historico->obterHistoricoCompleto()->first()->actuation == null ? 'Não definido' : $curriculo->historico->obterHistoricoCompleto()->first()->actuation->descricao}}</td>
				<td>{{$curriculo->candidato->fullContato()}}</td>
				<td>{{$curriculo->candidato->fullAddress()}}</td>
				<td>{{ $curriculo->created_at }}</td>
				<td>
					<a href="/cadastro/curriculos/{{ $curriculo->candidato->id }}/edicao">
						<i class="fa fa-edit"></i>
					</a>
					<a href="/cadastro/curriculos/{{ $curriculo->id }}/visualizar">
						<i class="fa fa-eye"></i>
					</a>
				</td>
			</tr>
			@endforeach
		@endif
    </tbody>
</table>

<div class="pagination-area">
{{ $curriculos->links() }}
</div>



<script>
	$(document).ready(function() {
	    $('.pagination a').on('click', function (event) {
	        event.preventDefault();
	        if ( $(this).attr('href') != '#' ) {
	            url = $(this).attr('href');
	            page = url.split('page=')[1];
	            $('#pagina').val(page);
	            hotsite.curriculo.filtrar(page);
	        }
	    });
	  });
</script>

