<div class="row" name="historicoInfo[]">
    <input type="hidden" name="historicoId[]" value="" />
    <div class="col-md-3">
        <div class="form-group">
            <label>Empresa</label>
            <input type="text" class="form-control" name="empresa[]" placeholder="Empresa" required/>
        </div>                                    
    </div>
    <div class="col-md-2">
        <div class="form-group">
            <label>Cargo</label>
            <input type="text" class="form-control" name="cargo[]" placeholder="Cargo"/>
        </div>                                    
    </div>
    <div class="col-md-2">
        <label>Área de atuação</label>
        <select class="form-control select2" name="actuation[]" data-placeholder="Área de atuação" style="width: 100%;" onchange="hotsite.actuation.checkActuationSelect(this)" required >
            <option></option>
            <!-- <option value="new">+ Nova área de atuação</option> -->
            @foreach($actuations as $actuation)
                <option value="{{$actuation->id}}">{{$actuation->descricao}}</option>
            @endforeach
        </select>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            <label>Data Entrada</label>
            <input type="text" class="form-control" name="dataEntrada[]" required/>
        </div>                                    
    </div>
    <div class="col-md-2">
        <div class="form-group">
            <label>Data Saída</label>
            <input type="text" class="form-control" name="dataSaida[]"/>
        </div>                                    
    </div>
    <div class="col-md-1">
        <div class="form-group">
            <label>Retirar</label>
            <button href="javascript:void(0)" name="removeHistory" class="btn btn-circle btn-danger" 
                onclick="hotsite.curriculo.minusHistorico(this)" >
                <i class="fa fa-minus-circle"></i>
            </button>
        </div>
    </div>
</div>