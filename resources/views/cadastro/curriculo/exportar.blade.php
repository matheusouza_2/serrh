<!DOCTYPE html>
<html>
<head>
    <title>Hi there</title>
    <style>
        html, body {
            font-size: 12px;
            font-family: 'Arial', sans-serif;
            position: relative;
        }
        .wrapper {
            background: #fff;
            /* position: absolute;
            top: 0;
            bottom: 0;
            left: 50%;
            transform: translateX(-50%); */
        }
        .wrapper > div {
            margin-top: 3em;
            font-size: 10px;
        }
        .logo {
            width: 100%;
            position: relative;
            height: 75px;
        }
        .logo img{
            height: 75px;
            position: absolute;
            transform: translateX(-50%);
            top: 0;
            left: 50%;
        }
        .header{
            text-align: center;
            margin-top: 5px !important;
        }
        .name{
            font-size: 28px;
            font-weight: bold;
            text-transform: uppercase;
        }
        .professionalGoal {
            border: 1px solid #000;
        }
        .academic > span, .extracurricular > span, .experience > span, .qualifications > span, .funcoesRealizadas > span {
            text-decoration: underline;
            font-weight: bold;
            font-size: 12px;
        }
        .experience div:first-of-type{
            margin-top: 1em;
        }
        .experience-item {
            margin-top: 2em;
        }
        .experience-item div:first-child {
            font-size: 11px;
        }


    </style>
</head>
<body>
    <div class="wrapper">
        <div class="logo">
            <img src={{ public_path().'/img/logo.jpeg' }} alt="Logo" height="75px">
            {{-- <img src="{{ public_path('img/logo.jpeg') }}"> --}}
        </div>
        <div class="header">
            <span class="name">{{$curriculo->candidato->nome}}</span>
            <div class="info">
                <div>Endereço: {{$curriculo->candidato->logradouro == null ? 'Não cadastrado' : $curriculo->candidato->logradouro}}, {{$curriculo->candidato->numero != null ? $curriculo->candidato->numero : 'S/N'}} </div>
                @if($curriculo->candidato->cep === null)
                <div>Dados de endereço não cadastrado</div>
                @else
                <div>{{$curriculo->candidato->bairro}} – {{$curriculo->candidato->cidade}} CEP: {{CustomFuncs::formatarCep($curriculo->candidato->cep)}}  - {{$curriculo->candidato->uf->sigla}} </div>
                @endif
                <div>Nascimento: {{$curriculo->candidato->data_nascimento}} Estado Civil: {{$curriculo->candidato->civilstate_id == null ? 'Não informado' : $curriculo->candidato->civilstate->descricao}}</div>
                <div>Tel. Res.: {{$curriculo->candidato->telefone == null ? 'Não cadastrado' : CustomFuncs::formatarCelular($curriculo->candidato->telefone)}} / Celular: {{$curriculo->candidato->celular == null ? 'Não cadastrado' : CustomFuncs::formatarCelular($curriculo->candidato->celular)}}</div>
                <div>E-mail: {{$curriculo->candidato->email}}</div>
            </div>
        </div>
        <div class="professionalGoal">Objetivo profissional: {{$curriculo->objetivoProfissional}}</div>
        <div class="academic">
            <span>Formação acadêmica</span>
            <ul>
                <li>
                    @if($curriculo->candidato->escolaridade_id == null)
                        @if($curriculo->candidato->formacao == null)
                            Não informado
                        @else
                            {{$curriculo->candidato->formacao}}
                        @endif
                    @else
                        @if($curriculo->candidato->formacao == null)
                            {{$curriculo->candidato->escolaridade->descricao}}
                        @else
                            {{$curriculo->candidato->formacao}}
                        @endif
                    @endif
                </li>
            </ul>
        </div>
        <div class="extracurricular">
            <span>Extra curricular</span>
            @if($curriculo->extracurricular == null)
            <div>Não informado</div>
            @else
            <div>{!! $curriculo->extracurricular !!}</div>
            @endif
        </div>
        <div class="experience">
            <span>Experiência e Trajetória Profissional</span>
            @foreach($curriculo->historico->obterHistoricoCompleto() as $historico)
                <div class="experience-item">
                    <div>Empresa: <strong>{{$historico->empresa}}</strong></div>
                    <div>Cargo: {{$historico->cargo}}</div>
                    <div>Período: {{$historico->data_entrada}} até {{$historico->data_saida == null ? 'Atual' : $historico->data_saida}}</div>
                </div>
            @endforeach
        </div>
        <div class="funcoesRealizadas">
            <span>Funções realizadas</span>
            @if($curriculo->funcoesRealizadas == null)
            <div>Não informado</div>
            @else
            <div>{!! $curriculo->funcoesRealizadas !!}</div>
            @endif
        </div>
        <div class="qualifications">
            <span>Qualificações Profissionais</span>
            @if($curriculo->observacoesFinais == null)
            <div>Não informado</div>
            @else
            <div>{!! $curriculo->observacoesFinais !!}</div>
            @endif
        </div>
    </div>
</body>
</html>
