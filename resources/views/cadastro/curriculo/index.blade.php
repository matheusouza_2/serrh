@extends('layouts.appinterno')

@section('content')
<section class="content-header">
  <h1>
    Currículo
    <small>
        @if(Auth::user()->perfil->identificador == "Candidato")
        Meu currículo
        @else
        Todos currículos
        @endif
    </small>
</h1>
</section>
<section class="content">
    @csrf
    <div class="row" style="display: {{ Auth::user()->perfil->identificador == 'Candidato' ? 'none' : '' }}">
        <div class="col-xs-12 col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Filtros</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Nome</label>
                                <input type="text" class="form-control pull-right" id="usuario" name="usuario">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Cargo</label>
                                <input type="text" class="form-control pull-right" id="cargo" name="cargo">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Área de atuação</label>
                                <select class="form-control select2" name="actuations[]" id="actuations" multiple="multiple" data-placeholder="Escolha as áreas de atuação"
                                style="width: 100%;">
                                    @foreach($actuations as $actuation)
                                        <option value="{{$actuation->id}}">{{$actuation->descricao}}</option>
                                    @endforeach
                                </select>
                                <small class="pull-right" class="checkbox">
                                <input type="checkbox" name="lastActuation" id="lastActuation" />
                                Considerar apenas a última área</small>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Data inicial de criação</label>
                                <input type="text" class="form-control pull-right" id="data-inicial" name="data-inicial">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Data final de criação</label>
                                <input type="text" class="form-control pull-right" id="data-final" name="data-final">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="col-md-12">
                        <button class="btn btn-primary pull-right" onclick="hotsite.curriculo.filtrar()">
                            <i class="fa fa-search"></i> Filtrar
                        </button>
                    </div>
                </div>

            </div>
        </div>
    </div>
<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">{{ Auth::user()->perfil->identificador == "Candidato" ? 'Meu currículo' : 'Lista de Currículos' }}</h3>
                <div class="box-tools pull-right">
                    @if(Auth::user()->perfil->descricao == "Candidato" && Auth::user()->curriculo == null)
                    <a href="/cadastro/curriculos/{{ Auth::user()->id }}/edicao" class="btn btn-box-tool btn-box-tool-add"><i class="fa fa-plus"></i> Novo currículo
                    </a>
                    @elseif(Auth::user()->perfil->descricao != "Candidato")
                    <a href="/cadastro/curriculos/new/edicao" class="btn btn-box-tool btn-box-tool-add"><i class="fa fa-plus"></i> Novo currículo
                    </a>
                    @endif
                </div>
            </div>
            <div class="box-body no-padding">
                <div class="overlay" style="display: block">
                    <i class="fa fa-refresh fa-spin"></i>
                </div>
                <div id="result"></div>
            </div>
            <!-- <div class="box-footer clearfix"> -->
                <!-- <ul class="pagination pagination-sm no-margin pull-right">
                    <li><a href="#">&laquo;</a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">&raquo;</a></li>
                </ul> -->
            <!-- </div> -->
        </div>
    </div>
</div>
</section>

@endsection


@section('scripts')
<script src="{{ asset('js/hotsite/Hotsite.Curriculo.js') }}"></script>
<script>
    $('.select2').select2();
    $('#data-inicial').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' });
    $('#data-final').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' });
    hotsite.curriculo.filtrar();
</script>
@endsection
