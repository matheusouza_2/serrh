@extends('layouts.appinterno')

@section('content')
<style>
    #curriculo-view input{
        background-color: #fff;
        border: 0;
        border-bottom: 1px solid #ccc;
        cursor: text;
    }
</style>
<section class="content-header">
  <h1>
    Currículo
    <small>{{ $curriculo->candidato->nome }}</small>
    <div class="pull-right">
        <a class="btn btn-flat btn-primary" href="/cadastro/curriculos"><i class="fa fa-arrow-left"></i> Voltar</a>
        <a class="btn btn-flat btn-primary" href="/cadastro/curriculos/{{$curriculo->id}}/exportar"><i class="fa fa-file-pdf-o"></i> Exportar</a>
    </div>
</h1>
</section>
<section class="content" id="curriculo-view">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Informações do usuário</h3>
                    <div class="box-tools pull-right">
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label>Candidato</label>
                                <input type="text" class="form-control" disabled value="{{ $curriculo->candidato->nome }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>CPF</label>
                                <input type="text" class="form-control" disabled value="{{ CustomFuncs::formatarCpf($curriculo->candidato->cpf) }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-7">
                            <div class="form-group">
                                <label>E-mail</label>
                                <input type="text" class="form-control" disabled value="{{ $curriculo->candidato->email }}">
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label>Sexo</label>
                                <input type="text" class="form-control" disabled value="{{ $curriculo->candidato->sexo->descricao }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Telefone</label>
                                <input type="text" class="form-control" disabled value="{{ CustomFuncs::formatarTelefone($curriculo->candidato->telefone) }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Celular</label>
                                <input type="text" class="form-control" disabled value="{{ CustomFuncs::formatarCelular($curriculo->candidato->celular) }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Estado Civil</label>
                                <input type="text" class="form-control" disabled value="{{ $curriculo->candidato->civilstate_id == null ? 'Não informado' : $curriculo->candidato->civilstate->descricao }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Endereço</label>
                                <input type="text" class="form-control" disabled value="{{ $curriculo->candidato->fullAddress() }}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Informações do Currículo</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Objetivo Profissional</label>
                                <input type="text" class="form-control" name="objetivoProfissional" id="objetivoProfissional" value="{{ $curriculo->objetivoProfissional }}" required/>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="curriculoId" id="curriculoId" value="{{ $curriculo->id }}" />
                    <div class="row">
                        <div class="col-md-12">
                            <h4>Histórico de empresas e áreas de atuação
                            </h4>
                        </div>
                        <div class="col-md-12" id="historico">
                            @php $allHistory = $curriculo->historico->obterHistoricoCompleto(); @endphp
                            @foreach($allHistory as $historico)
                                <div class="row" name="historicoInfo[]">
                                    <input type="hidden" name="historicoId[]" value="{{ $historico->id }}" />
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label>Empresa</label>
                                            <input type="text" class="form-control" name="empresa[]" value="{{ $historico->empresa }}" placeholder="Empresa" required/>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <label>Área de atuação</label>
                                        <input type="text" class="form-control" name="actuation[]" value="{{ $historico->actuation->descricao }}" required/>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Data Entrada</label>
                                            <input type="text" class="form-control" name="dataEntrada[]" value="{{ $historico->data_entrada }}" required/>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Data Saída</label>
                                            <input type="text" class="form-control" name="dataSaida[]" value="{{ $historico->data_saida == null ? 'Área atual' : $historico->data_saida }}"/>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Funções realizadas</label>
                                <form>
                                    <div style="width: 100%; padding: 10px; border-bottom: 1px solid #ccc">{!! $curriculo->funcoesRealizadas !!}</div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Atividades extracurriculares</label>
                                <form>
                                    <div style="width: 100%; padding: 10px; border-bottom: 1px solid #ccc">{!! $curriculo->extracurricular !!}</div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Observações Finais</label>
                                <input type="text" class="form-control" id="observacoesFinais" name="observacoesFinais"  value="{{ $curriculo->observacoesFinais }}"/>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>

</section>

@endsection


@section('scripts')

@endsection
