<input type="hidden" id="empresaId" value="{{ $empresa->id }}" />
<div class="row">
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-6 col-lg-6">
                <div class="input-group">
                    <label>CNPJ</label>
                    <div class="input-group">
                        <input type="text" class="form-control" name="cnpj" id="cnpj" value="{{ $empresa->cnpj }}" data-inputmask='"mask": "99.999.999/9999-99", "clearIncomplete" : true' data-mask {{ $empresa->cnpj == null ? '' : 'disabled' }} required/>
                        <span class="input-group-btn">
                            <button class="btn btn-primary" onclick="hotsite.buscaCnpj('cnpj', this)"><i class="fa fa-search"></i> Buscar CNPJ</button>
                        </span>
                    </div>
                </div>                                    
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Razão Social</label>
                    <input type="text" class="form-control" name="razao_social" id="razao_social" placeholder="Razão social" value="{{ $empresa->razao_social }}" required/>
                </div>                                    
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Nome Fantasia</label>
                    <input type="text" class="form-control" name="nome_fantasia" id="nome_fantasia" placeholder="Nome Fantasia" value="{{ $empresa->nome_fantasia }}" required/>
                </div>                                    
            </div>
        </div>
         <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Contato</label>
                    <input type="text" class="form-control" name="contato" id="contato" value="{{ $empresa->contato }}" />
                </div>                                    
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Empresário</label>
                    <select class="form-control select2" name="empresario" id="empresario" data-placeholder="Empresário" style="width: 100%;" required>
                        <option></option>
                        @if(Auth::user()->perfil->identificador == "Empresario")
                            <option value="{{Auth::user()->id}}" selected>{{Auth::user()->nome}}</option>
                        @else
                            @foreach($empresarios as $empresario)
                                <option value="{{$empresario->id}}" {{ $empresa->criador_id == $empresario->id ? 'selected' : '' }}>{{$empresario->nome}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>                                    
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <div class="form-group">
                    <label>Email</label>
                    <input type="email" class="form-control" name="email" id="email" placeholder="E-mail" value="{{ $empresa->email }}" required/>
                </div>                                    
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Telefone</label>
                    <input type="text" class="form-control" name="telefone" id="telefone" value="{{ $empresa->telefone }}" data-inputmask='"mask": "(99) 9999-9999", "clearIncomplete" : true' data-mask/>
                </div>                                    
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>CEP <i class="fa fa-question-circle" title="CEP incorreto" id="cepError" style="display: none"></i></label>
                    <div class="input-group">
                        <input type="text" class="form-control" name="cep" id="cep" value="{{ $empresa->cep }}" data-inputmask='"mask": "99999-999", "clearIncomplete" : true' data-mask required/>
                        <span class="input-group-btn">
                            <button class="btn btn-primary" onclick="hotsite.buscaCep('cep', this)"><i class="fa fa-search"></i> Buscar CEP</button>
                        </span>
                    </div>
                </div>                                    
            </div>
        </div>
        <div class="row">
            <div class="col-md-7">
                <div class="form-group">
                    <label>Logradouro <i class="fa fa-question-circle" title="Preenchido pelo CEP"></i></label>
                    <input type="text" class="form-control" name="logradouro" id="logradouro" value="{{ $empresa->logradouro }}" required disabled/>
                </div>                                    
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Número</label>
                    <input type="text" class="form-control" name="numero" id="numero" value="{{ $empresa->numero }}" required/>
                </div>                                    
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Complemento</label>
                    <input type="text" class="form-control" name="complemento" id="complemento" value="{{ $empresa->complemento }}" />
                </div>                                    
            </div>
        </div>
        <div class="row">
            <div class="col-md-5">
                <div class="form-group">
                    <label>Bairro <i class="fa fa-question-circle" title="Preenchido pelo CEP"></i></label>
                    <input type="text" class="form-control" name="bairro" id="bairro" value="{{ $empresa->bairro }}" disabled required/>
                </div>                                    
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Cidade <i class="fa fa-question-circle" title="Preenchido pelo CEP"></i></label>
                    <input type="text" class="form-control" name="cidade" id="cidade" value="{{ $empresa->cidade }}" disabled required/>
                </div>                              
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>UF <i class="fa fa-question-circle" title="Preenchido pelo CEP"></i></label>
                    <select class="form-control select2" name="uf" id="uf" data-placeholder="UF" style="width: 100%;" disabled>
                        <option value=""></option>
                        @foreach($ufs as $uf)
                            <option value="{{$uf->id}}" {{ $empresa->uf_id == $uf->id ? 'selected' : '' }}>{{$uf->sigla}}</option>
                        @endforeach
                    </select>
                </div>                              
            </div>
        </div>
    </div>
</div>
