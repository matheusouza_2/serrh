<table class="table table-striped table-responsive">
    <thead>
        <tr>
          <th style="width: 10px">ID</th>
          <th>CNPJ</th>
          <th>Nome</th>
          <th>Responsável/Telefone da Empresa</th>
          <th>Endereço</th>
          <th></th>
        </tr>
    </thead>
    <tbody>
    	@if(count($empresas) == 0)
			<tr><td colspan="6" style="text-align: center">Não foi encontrado nenhum registro</td></tr>
		@else
			@foreach($empresas as $empresa)
			<tr>
				<td>{{$empresa->id}}</td>
				<td>{{CustomFuncs::formatarCnpj($empresa->cnpj)}}</td>
				<td>{{$empresa->razao_social}}</td>
				<td>{{$empresa->nome_fantasia}}/{{CustomFuncs::formatarTelefone($empresa->telefone)}}</td>
				<td>{{$empresa->fullAddress()}}</td>
				<td>
					<a href="/cadastro/empresa/{{ $empresa->id }}/edicao">
						<i class="fa fa-edit"></i>
					</a>
				</td>
			</tr>
			@endforeach
		@endif
    </tbody>
</table>

<div class="pagination-area">
{{ $empresas->links() }}
</div>

<script>
	$(document).ready(function() {
	    $('.pagination a').on('click', function (event) {
	        event.preventDefault();
	        if ( $(this).attr('href') != '#' ) {
	            url = $(this).attr('href');
	            page = url.split('page=')[1];
	            $('#pagina').val(page);
	            hotsite.empresa.filtrar(page);
	        }
	    });
	  });
</script>