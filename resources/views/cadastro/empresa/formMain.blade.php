@extends('layouts.appinterno')
 
@section('content')
<section class="content-header">
  <h1>
    Empresa
    <small>Formulário</small>
    <div class="pull-right">
        <a class="btn btn-flat btn-primary" href="{{ URL::previous() }}"><i class="fa fa-arrow-left"></i> Voltar</a>
    </div>
</h1>  
</section>
<section class="content">
    @csrf
    <div class="row">
	    <div class="col-xs-12 col-md-12">
	        <div class="box box-primary">
	            <div class="box-header with-border">
	                <h3 class="box-title">Informações do empresa</h3>
	                <div class="box-tools pull-right">
	                </div>
	            </div>
	            <div class="box-body">
	    			@include('cadastro.empresa._form')
	    		</div>
	        </div>
	    </div>
	</div>
    <div class="row">
        <div class="col-md-12">
            <button class="btn btn-primary pull-right" onclick="hotsite.empresa.salvar()">
                <i class="fa fa-save"></i> Salvar
            </button>
        </div>
    </div>
</section>

@endsection


@section('scripts')
    <script src="{{ asset('js/hotsite/Hotsite.Empresa.js') }}"></script>
    <script>
        $('.select2').select2();
	    $('[data-mask]').inputmask();
    </script>
@endsection
