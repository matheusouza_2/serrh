@extends('layouts.appinterno')

@section('content')
<section class="content-header">
  <h1>
    Empresas
    <small>
        @if(Auth::user()->perfil->identificador == "Candidato")
        Meu currículo
        @else
        Todas empresas
        @endif
    </small>
</h1>  
</section>
<section class="content">
    @if(Auth::user()->perfil->identificador == "Candidato")

    @else
    @csrf
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="box box-primary collapsed-box">
                <div class="box-header with-border">
                    <h3 class="box-title">Filtros</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Razão Social</label>
                                <input type="text" class="form-control" name="razao_social" id="razao_social" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Nome Fantasia</label>
                                <input type="text" class="form-control" name="nome_fantasia" id="nome_fantasia" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>CNPJ</label>
                                <input type="text" class="form-control" name="cnpj" id="cnpj"  data-inputmask='"mask": "99.999.999/9999-99"' data-mask/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="col-md-12">
                        <button class="btn btn-primary pull-right" onclick="hotsite.empresa.filtrar()">
                            <i class="fa fa-search"></i> Filtrar
                        </button>
                    </div>
                </div>

            </div>
        </div>
    </div>
<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Lista de empresas</h3>
                <div class="box-tools pull-right">
                    <a href="/cadastro/empresa/new/edicao" class="btn btn-box-tool btn-box-tool-add"><i class="fa fa-plus"></i> Nova empresa
                    </a>
                </div>
            </div>
            <div class="box-body no-padding">
                <div class="overlay" style="display: block">
                    <i class="fa fa-refresh fa-spin"></i>
                </div>
                <div id="result"></div>
            </div>
            <!-- <div class="box-footer clearfix"> -->
                <!-- <ul class="pagination pagination-sm no-margin pull-right">
                    <li><a href="#">&laquo;</a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">&raquo;</a></li>
                </ul> -->
            <!-- </div> -->
        </div>
    </div>
</div>

@endif
</section>

@endsection


@section('scripts')
<script src="{{ asset('js/hotsite/Hotsite.Empresa.js') }}"></script>
<script>
    $('.select2').select2();
    $('[data-mask]').inputmask();
    hotsite.empresa.filtrar();
</script>
@endsection
