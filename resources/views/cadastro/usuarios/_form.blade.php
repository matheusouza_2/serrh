@php
    $isCandidato = (Auth::user() != null && Auth::user()->perfil->identificador == "Candidato");
@endphp
<input type="hidden" id="userId" value="{{ $user->id }}" />
<div class="row">
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Nome</label>
                    <input type="text" class="form-control" name="nome" id="nome" placeholder="Nome completo" value="{{ $user->nome }}" required/>
                </div>
            </div>
        </div>
        <div class="row">
            <!-- <div class="col-md-6">
                <div class="form-group">
                    <label>Login</label>
                    <input type="text" class="form-control" name="login" id="login" placeholder="Login" value="{{ $user->login }}" {{ $user->login == null ? '' : 'disabled' }} required/>
                </div>
            </div> -->
            <div class="col-md-6">
                <div class="form-group">
                    <label>CPF</label>
                    <input type="text" class="form-control" name="cpf" id="cpf" placeholder="CPF" value="{{ $user->cpf }}" data-inputmask='"mask": "999.999.999-99", "clearIncomplete" : true' data-mask {{ $user->cpf == null ? '' : 'disabled' }} required/>
                </div>
            </div>
        </div>
        @if(!$isCandidato)
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label>Senha</label>
                    <input type="password" class="form-control" name="password" id="password" placeholder="Senha" {{ $user->password != null ? 'disabled' : '' }}/>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Confirme a senha</label>
                    <input type="password" class="form-control" name="confirmPassword" id="confirmPassword" placeholder="Confirme a senha" {{ $user->password != null ? 'disabled' : '' }}/>
                </div>
            </div>
            <div class="col-md-4">
                <div class="checkbox" style="margin-top: 25px">
                  <label><input type="checkbox" id="confirmChange" name="confirmChange" value="" onchange="hotsite.user.activePassword(this)" {{ $user->password == null ? 'disabled checked' : '' }}>Alterar senha?</label>
                </div>
            </div>
        </div>
        @endif
        <div class="row">
            <div class="col-md-8">
                <div class="form-group">
                    <label>Email</label>
                    <input type="email" class="form-control" name="email" id="email" placeholder="E-mail" value="{{ $user->email }}" required/>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Estado Civil</label>
                    <select class="form-control select2" name="civilstate" id="civilstate" style="width: 100%;">
                        <option value="nd" {{ $user->civilstate_id == '' ? 'selected' : '' }}>Não informado</option>
                        {{info($user->civilstate_id)}}
                        @foreach($civilstates as $civilstate)
                            <option value="{{$civilstate->id}}" {{ $user->civilstate_id == $civilstate->id ? 'selected' : '' }}>{{$civilstate->descricao}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label>Sexo</label>
                    <select class="form-control select2" name="sexo" id="sexo" data-placeholder="Sexo" style="width: 100%;" required>
                        @foreach($sexos as $sexo)
                            <option value="{{$sexo->id}}" {{ $user->sexo_id == $sexo->id ? 'selected' : '' }}>{{$sexo->descricao}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Data de Nascimento</label>
                    <input type="text" class="form-control" name="dataNascimento" id="dataNascimento" value="{{ $user->data_nascimento }}" required />
                </div>
            </div>
            @if(!$isCandidato)
            <div class="col-md-4">
                <div class="form-group">
                    <label>Perfil</label>
                    <select class="form-control select2" name="perfil" id="perfil" data-placeholder="Perfil" style="width: 100%;" required>
                        @foreach($perfis as $perfil)
                            <option value="{{$perfil->id}}" {{ $user->perfisusuario_id == $perfil->id ? 'selected' : '' }}>{{$perfil->descricao}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            @else
                <input type="hidden" id="perfil" id="perfil" value="{{ $perfis->where('identificador','=','Candidato')->first()->id }}" />
            @endif
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Escolaridade</label>
                    <select class="form-control select2" name="escolaridade" id="escolaridade" data-placeholder="Escolaridade" style="width: 100%;" required>
                        @foreach($escolaridades as $escolaridade)
                            <option value="{{$escolaridade->id}}" {{ $user->escolaridade_id == $escolaridade->id ? 'selected' : '' }}>{{$escolaridade->descricao}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Formação</label>
                    <input type="text" class="form-control" name="formacao" id="formacao" value="{{ $user->formacao }}" />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Telefone</label>
                    <input type="text" class="form-control" name="telefone" id="telefone" value="{{ $user->telefone }}" data-inputmask='"mask": "(99) 9999-9999", "clearIncomplete" : true' data-mask/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Celular</label>
                    <input type="text" class="form-control" name="celular" id="celular" value="{{ $user->celular }}" data-inputmask="'mask': '(99) 99999-9999', 'clearIncomplete' : true" data-mask/>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>CEP <i class="fa fa-question-circle" title="CEP incorreto" id="cepError" style="display: none"></i></label>
                    <div class="input-group">
                        <input type="text" class="form-control" name="cep" id="cep" value="{{ $user->cep }}" data-inputmask='"mask": "99999-999", "clearIncomplete" : true' data-mask required/>
                        <span class="input-group-btn">
                            <button class="btn btn-primary" onclick="hotsite.buscaCep('cep', this)"><i class="fa fa-search"></i> Buscar CEP</button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-7">
                <div class="form-group">
                    <label>Logradouro <i class="fa fa-question-circle" title="Preenchido pelo CEP"></i></label>
                    <input type="text" class="form-control" name="logradouro" id="logradouro" value="{{ $user->logradouro }}" required disabled/>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Número</label>
                    <input type="text" class="form-control" name="numero" id="numero" value="{{ $user->numero }}" required/>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Complemento</label>
                    <input type="text" class="form-control" name="complemento" id="complemento" value="{{ $user->complemento }}" />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5">
                <div class="form-group">
                    <label>Bairro <i class="fa fa-question-circle" title="Preenchido pelo CEP"></i></label>
                    <input type="text" class="form-control" name="bairro" id="bairro" value="{{ $user->bairro }}" disabled required/>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Cidade <i class="fa fa-question-circle" title="Preenchido pelo CEP"></i></label>
                    <input type="text" class="form-control" name="cidade" id="cidade" value="{{ $user->cidade }}" disabled required/>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>UF <i class="fa fa-question-circle" title="Preenchido pelo CEP"></i></label>
                    <select class="form-control select2" name="uf" id="uf" data-placeholder="UF" style="width: 100%;" disabled>
                        <option value=""></option>
                        @foreach($ufs as $uf)
                            <option value="{{$uf->id}}" {{ $user->uf_id == $uf->id ? 'selected' : '' }}>{{$uf->sigla}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>
