<table class="table table-striped table-responsive">
    <thead>
        <tr>
          <th style="width: 10px">ID</th>
          <th>Nome</th>
          <th>CPF</th>
          <th>Contato</th>
          <th>Endereço</th>
          <th>Perfil</th>
          <th></th>
        </tr>
    </thead>
    <tbody>
    	@foreach($users as $user)
			<tr>
				<td>{{$user->id}}</td>
				<td>{{$user->nome}}</td>
				<td>{{CustomFuncs::formatarCpf($user->cpf)}}</td>
				<td>{{$user->fullContato()}}</td>
				<td>{{$user->fullAddress()}}</td>
				<td>{{$user->perfil->descricao}}</td>
				<td>
					<a href="/cadastro/usuarios/{{ $user->id }}/edicao">
						<i class="fa fa-edit"></i>
					</a>
				</td>
			</tr>
			@endforeach
    </tbody>
</table>

<div class="pagination-area">
{{ $users->links() }}
</div>



<script>
	$(document).ready(function() {
	    $('.pagination a').on('click', function (event) {
	        event.preventDefault();
	        if ( $(this).attr('href') != '#' ) {
	            url = $(this).attr('href');
	            page = url.split('page=')[1];
	            $('#pagina').val(page);
	            hotsite.user.filtrar(page);
	        }
	    });
	  });
</script>