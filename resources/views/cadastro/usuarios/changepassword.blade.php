@extends('layouts.appinterno')

@section('content')
<section class="content-header">
  <h1>
    Usuário
    <small>
        Alterar minha senha
    </small>
</h1>  
</section>
<section class="content">
    @csrf
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Alterar senha</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Senha</label>
                                <input type="password" class="form-control" name="password" id="password" placeholder="Digite sua senha" value="" required/>
                            </div>                        
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Confirmar senha</label>
                                <input type="password" class="form-control" name="confirmPassword" id="confirmPassword" placeholder="Confirme a senha" value="" required/>
                            </div>                        
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <button class="btn btn-primary pull-right" onclick="hotsite.user.changePassword()">Alterar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection


@section('scripts')
<script src="{{ asset('js/hotsite/Hotsite.User.js') }}"></script>
<script>
    $('.select2').select2();
    $('[data-mask]').inputmask();
    hotsite.user.filtrar();
</script>
@endsection
