@extends('layouts.appinterno')

@section('content')
<section class="content-header">
  <h1>
    Usuário
    <small>
        @if(Auth::user()->perfil->identificador == "Candidato")
            Meu perfil
        @else
            Todos usuários
        @endif
    </small>
</h1>  
</section>
<section class="content">
    @csrf
    <div class="row" style="display: {{ Auth::user()->perfil->identificador == 'Candidato' ? 'none' : '' }}">
        <div class="col-xs-12 col-md-12">
            <div class="box box-primary collapsed-box">
                <div class="box-header with-border">
                    <h3 class="box-title">Filtros</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Nome</label>
                                <input type="text" class="form-control" name="name" id="name" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>CPF</label>
                                <input type="text" class="form-control" name="cpf" id="cpf"  data-inputmask='"mask": "999.999.999-99"' data-mask/>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Perfil</label>
                                <select class="form-control select2" name="perfis[]" id="perfis" multiple="multiple" data-placeholder="Escolha os perfis"
                                style="width: 100%;">
                                    @foreach($perfis as $perfil)
                                        <option value="{{$perfil->id}}">{{$perfil->descricao}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="col-md-12">
                        <button class="btn btn-primary pull-right" onclick="hotsite.user.filtrar()">
                            <i class="fa fa-search"></i> Filtrar
                        </button>
                    </div>
                </div>

            </div>
        </div>
    </div>
<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">{{ Auth::user()->perfil->identificador == "Candidato" ? 'Meu perfil' : 'Lista de usuários' }}</h3>
                <div class="box-tools pull-right">
                    @if(Auth::user()->perfil->identificador != "Candidato")
                    <a href="/cadastro/usuarios/new/edicao" class="btn btn-box-tool btn-box-tool-add"><i class="fa fa-plus"></i> Novo usuário
                    </a>
                    @endif
                    
                </div>
            </div>
            <div class="box-body no-padding">
                <div class="overlay" style="display: block">
                    <i class="fa fa-refresh fa-spin"></i>
                </div>
                <div id="result"></div>
            </div>
            <!-- <div class="box-footer clearfix"> -->
                <!-- <ul class="pagination pagination-sm no-margin pull-right">
                    <li><a href="#">&laquo;</a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">&raquo;</a></li>
                </ul> -->
            <!-- </div> -->
        </div>
    </div>
</div>
</section>

@endsection


@section('scripts')
<script src="{{ asset('js/hotsite/Hotsite.User.js') }}"></script>
<script>
    $('.select2').select2();
    $('[data-mask]').inputmask();
    hotsite.user.filtrar();
</script>
@endsection
