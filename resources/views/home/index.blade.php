@extends('layouts.appinterno')

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Página Inicial
        <small></small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
    </section>
    <!-- /.content -->
@endsection

@section('scripts')
    @if (Auth::user()->celular == '')
        <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
        <script>
            Swal.fire({
                title: 'Você não possui um numero de celular cadastrado',
                text: `Olá, identificamos que você não possui um celular cadastrado na plataforma, sendo assim caso você seja selecionado para uma entrevista de emprego nos
                        não temos como entrar em contato, para resolver essa questão vá até "Área do candidato > Meu perfil" e altere seu perfil colocando um numero de celular válido`,
                icon: 'warning'
            })
        </script>
    @endif

@endsection

