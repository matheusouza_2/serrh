<ul class="dropdown-menu">
              <!-- User image -->
  <li class="user-header">
    <img src="{{asset('img/boxed-bg.jpg')}}" class="img-circle" alt="User Image">

    <p>
      {{Auth::user()->nome}} - {{Auth::user()->perfil->descricao}}
      <!-- <small>Membro desde </small> -->
    </p>
  </li>
  <!-- Menu Body -->
  <li class="user-body">
    <div class="row">
      <!-- <div class="col-xs-4 text-center">
        <a href="#">Followers</a>
      </div>
      <div class="col-xs-4 text-center">
        <a href="#">Sales</a>
      </div>
      <div class="col-xs-4 text-center">
        <a href="#">Friends</a>
      </div> -->
    </div>
    <!-- /.row -->
  </li>
  <!-- Menu Footer-->
  <li class="user-footer">
    <div class="pull-left">
      <a href="/alterarsenha" class="btn btn-default btn-flat">Alterar senha</a>
    </div>
    <div class="pull-right">
      <a href="/logout" class="btn btn-default btn-flat">Sair</a>
    </div>
  </li>
</ul>