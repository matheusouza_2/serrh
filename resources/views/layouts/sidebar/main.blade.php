<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    @if(Auth::user() != null)
    <div class="user-panel">
      <div class="pull-left image">
        <img src="{{asset('img/boxed-bg.jpg')}}" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p>{{Auth::user()->nome}}</p>
        <a href="#">{{Auth::user()->perfil->descricao}}</a>
      </div>
    </div>
    <!-- search form -->
      <!-- <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form> -->
      @endif
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
      @if(Auth::user() != null)
        @php
          $menusPermitidos = Auth::user()->perfil
            ->menus()->with('menu')
            ->whereHas('menu', function($query) {
              $query->whereNull('pai');
            })->get()
            ->sortBy('menu.pai')->sortBy('menu.id');

          $menusPermitidosSemRoot = Auth::user()->perfil
            ->menus()->with('menu')
            ->whereHas('menu', function($query) use($menusPermitidos) {
              $query->whereNotNull('pai')
                ->whereNotIn('id',$menusPermitidos->pluck('id'));
            })->get()
            ->sortBy('menu.pai')->sortBy('menu.id');
        @endphp

        @php $alreadyFound = array(); $last = null; @endphp

        @foreach($menusPermitidosSemRoot as $menu)
          @if($last != null && $last != $menu->menu->pai)
            </ul></li>
          @endif

          @if($last == null)
            @php $last = $menu->menu->pai; @endphp
          @endif

          @if(in_array($menu->menu->pai,$alreadyFound))
            <li class="{{str_contains(URL::current(),$menu->menu->rota) ? 'active' : ''}}"><a href="{{$menu->menu->rota}}"><i class="fa fa-circle-o"></i> {{$menu->menu->descricao}}</a></li>
          @else
            <li class="treeview">
              <a href="#">
                <i class="{{$menu->menu->obterPai->icon}}"></i> <span>{{$menu->menu->obterPai->descricao}}</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li class="{{str_contains(URL::current(),$menu->menu->rota) ? 'active' : ''}}"><a href="{{$menu->menu->rota}}"><i class="fa fa-circle-o"></i> {{$menu->menu->descricao}}</a></li>

            @php array_push($alreadyFound, $menu->menu->pai); @endphp
          @endif

          @if($loop->last)
            </ul></li>
          @endif
        @endforeach

        @foreach($menusPermitidos as $menu)
          <li class="treeview">
            <a href="#">
              <i class="{{$menu->menu->icon}}"></i> <span>{{$menu->menu->descricao}}</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              @foreach($menu->menu->obterFilhos as $filho)
                <li class="{{str_contains(URL::current(),$filho->rota) ? 'active' : ''}}"><a href="{{$filho->rota}}"><i class="fa fa-circle-o"></i> {{$filho->descricao}}</a></li>
              @endforeach
            </ul>
          </li>
        @endforeach
      @endif
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
