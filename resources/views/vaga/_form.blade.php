<input type="hidden" id="vagaId" value="{{ $vaga->id }}" />
<div class="row">
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-2" style="padding: 0; padding-left: 15px">
                <div class="form-group">
                    <label>RP</label>
                    <input type="text" class="form-control" name="rp" id="rp" value="{{ $vaga->rp }}" required/>
                </div>
            </div>
            <div class="col-md-10">
                <div class="form-group">
                    <label>Título da Vaga</label>
                    <input type="text" class="form-control" name="titulo_vaga" id="titulo_vaga" placeholder="Título da Vaga" value="{{ $vaga->titulo_vaga }}" required/>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Empresa</label>
                    <select class="form-control select2" name="empresa" id="empresa" data-placeholder="Escolha uma empresa" style="width: 100%;" required>
                        <option></option>
                        @foreach($empresas as $empresa)
                            <option value="{{$empresa->id}}" {{ $vaga->empresa_id == $empresa->id ? 'selected' : '' }}>{{$empresa->nome_fantasia}} - {{ CustomFuncs::formatarCnpj($empresa->cnpj) }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Tipo Recrutamento</label>
                    <input type="text" class="form-control" name="tiporecrutamento" id="tiporecrutamento" value="{{ $vaga->tiporecrutamento }}" placeholder="Tipo de recrutamento" required/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Área de atuação</label>
                     <select class="form-control select2" name="actuation[]" data-placeholder="Área de atuação" style="width: 100%;" onchange="hotsite.actuation.checkActuationSelect(this)" required >
                        <option></option>
                        <option value="new">+ Nova área</option>
                        @foreach($actuations as $actuation)
                            <option value="{{$actuation->id}}" {{ $vaga->actuation_id == $actuation->id ? 'selected' : '' }}>{{$actuation->descricao}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Masculino/Feminino</label>
                    <select class="form-control" id="marcacaosexo" name="marcacaosexo">
                        <option value="" {{ $vaga->marcacaosexo == null ? 'selected' : '' }}>Ambos</option>
                        <option value="Feminino" {{ $vaga->marcacaosexo == 'Feminino' ? 'selected' : '' }}>Feminino</option>
                        <option value="Masculino" {{ $vaga->marcacaosexo == 'Masculino' ? 'selected' : '' }}>Masculino</option>                        
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Candidato apenas da cidade?</label>
                    <input type="checkbox" id="only_city" name="only_city" {{$vaga->only_city ? 'checked' : ''}}>
                </div>
                
                <div class="form-group">
                    <label>Mostrar Empresa na RP ?</label>
                    <input type="checkbox" id="show_company" name="show_company" {{ $vaga->show_company ? 'checked' : ''}}>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label>Quantidade</label>
                    <input type="number" class="form-control" name="quantidade" id="quantidade" value="{{ $vaga->quantidade }}" min="1" required/>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Data de Fechamento:</label>

                    <div class="input-group date">
                        <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right datepicker" id="data_fechamento" name="data_fechamento" readonly="readonly" value="{{ $vaga->data_fechamento }}">
                </div>
                <!-- /.input group -->
              </div>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <label>Data Prevista da Admissão</label>

                    <div class="input-group date">
                        <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right datepicker" id="data_admissao" name="data_admissao" readonly="readonly" value="{{ $vaga->data_admissao }}">
                </div>
                <!-- /.input group -->
              </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Escolaridade Mínima</label>
                    <select class="form-control select2" name="escolaridade" id="escolaridade" data-placeholder="Escolaridade" style="width: 100%;" required>
                        <option></option>
                        @foreach($escolaridades as $escolaridade)
                            <option value="{{$escolaridade->id}}" {{ $vaga->escolaridade_id == $escolaridade->id ? 'selected' : '' }}>{{$escolaridade->descricao}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Idade Mínima</label>
                    <input type="text" class="form-control" name="idade_minima" id="idade_minima" placeholder="Idade Mínima" value="{{ $vaga->idade_min }}"/>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Idade Máxima</label>
                    <input type="text" class="form-control" name="idade_maxima" id="idade_maxima" placeholder="Idade Máxima" value="{{ $vaga->idade_max }}"/>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Salário Inicial</label>
                    <div class="input-group">
                        <span class="input-group-addon">R$</span>
                        <input type="text" class="form-control" name="salario_inicial" id="salario_inicial" style="width:100%" value="{{ number_format($vaga->salario_inicial, 2, ",","") }}" required />
                    </div>

                </div>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <label>Status da Vaga</label>
                    <select class="form-control select2" name="statusvaga" id="statusvaga" data-placeholder="Filtre por empresas"
                            style="width: 100%;">
                        @foreach($allStatusVagas as $status)
                            <option value="{{$status->id}}" {{$status->id == $vaga->statusvaga_id ? 'selected' : ''}}>{{$status->descricao}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-5">
                <div class="form-group">
                    <label>Plano de Publicação</label>
                    <select class="form-control select2" data-placeholder="Selecione um Plano" name="plano_pagamento" id="plano_pagamento" required>
                        <option value="0" {{ $vaga->plano_pagamento == 0?"selected":"" }}>Básico (Publicar a vaga e aguardar candidatos para a mesma.)</option>
                        <option value="1" {{ $vaga->plano_pagamento == 1?"selected":"" }}>Completo (Publicar a vaga e buscar por curriculos correspondentes a mesma.)</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Observações da Vaga</label>
                    <form>
                        <textarea class="textarea" id="observacoes_vaga" name="observacoes_vaga" placeholder="Coloque informações a respeito da vaga, atribuições, responsabilidades e etc."
                      style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" value="">{{ $vaga->observacoes_vaga }}</textarea>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Observaçõs do Candidato</label>
                    <form>
                        <textarea class="textarea" id="observacoes_candidato" name="observacoes_candidato" placeholder="Descreva aqui observações a respeito do candidato, as características pessoais, perfil profissional e etc..."
                      style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" value="">{{ $vaga->observacoes_candidato }}</textarea>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Horário de trabalho</label>
                    <form>
                        <textarea class="textarea" id="horario_trabalho" name="horario_trabalho" placeholder="Informe o horário de trabalho"
                      style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" value="">{{ $vaga->horario_trabalho }}</textarea>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Benefícios</label>
                    <form>
                        <textarea class="textarea" id="beneficios" name="beneficios" placeholder="Descreva os benefícios da vaga"
                      style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" value="">{{ $vaga->beneficios }}</textarea>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

