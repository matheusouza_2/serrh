<table class="table table-striped table-responsive">
    <thead>
        <tr>
          <th></th>
          <th style="width: 10px">RP</th>
          <th>Vaga</th>
          <th>Empresa</th>
          <th>Área de atuação</th>
          <th>Quantidade</th>
          <th>Candidatos</th>
          <th>Data Fechamento</th>
          <th>Status da vaga</th>
          <th>Data da criação</th>
          <th></th>
        </tr>
    </thead>
    <tbody>
    	@if(count($vagas) == 0)
			<tr><td colspan="10" style="text-align: center">Não foi encontrado nenhum registro</td></tr>
		@else
			@foreach($vagas as $vaga)
			<tr>
        <td><a href="{{ route('vagas_requisicao', [$vaga->id]) }}" title="Imprimir Requisição de Pessoal"><i class="fa fa-file"></i></a></td>
				<td>{{$vaga->rp}}</td>
				<td>{{$vaga->titulo_vaga}}</td>
				<td>{{$vaga->empresa->nome_fantasia}}</td>
				<td>{{$vaga->actuation->descricao}}</td>
				<td>{{$vaga->quantidade}}</td>
				<td>
					{{ $vaga->candidatos->count() }}
					@if($vaga->candidatos->count() > 0)
						<a href="vagas/{{ $vaga->id }}/candidatos"><i class="fa fa-eye"></i></a>
					@endif
				</td>
				<td>{{$vaga->data_fechamento}}</td>
                <td>{{ $vaga->status->descricao }}</td>
                <td>{{Carbon\Carbon::parse($vaga->created_at)->format('d/m/Y')}}</td>
				<td>
					<a href="/vagas/{{ $vaga->id }}/edicao">
						<i class="fa fa-edit"></i>
					</a>
                </td>

			</tr>
			@endforeach
		@endif
    </tbody>
</table>

<div class="pagination-area">
{{ $vagas->links() }}
</div>

<script>
  $(document).ready(function() {
    $('.pagination a').on('click', function (event) {
        event.preventDefault();
        if ( $(this).attr('href') != '#' ) {
            url = $(this).attr('href');
            page = url.split('page=')[1];
            $('#pagina').val(page);
            hotsite.vaga.filtrar(page);
        }
    });
  });
</script>
