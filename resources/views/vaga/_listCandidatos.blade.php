<table class="table table-striped table-responsive">
    <thead>
        <tr>
          <th>Nome</th>
          <th>Área de atuação</th>
          <th>E-mail de contato</th>
          <th></th>
        </tr>
    </thead>
    <tbody>
    	@if(count($filtrados) == 0)
			<tr><td colspan="10" style="text-align: center">Não foi encontrado nenhum registro</td></tr>
		@else
			@foreach($filtrados as $candidato)
			<tr>
				<td>{{ $candidato->user->nome }}</td>
				<td>{{
                    $candidato->user->curriculo->historico
                    ->obterHistoricoCompleto()->first()
                    ->actuation->descricao
                }}</td>
				<td>{{ $candidato->user->email }}</td>
				<td><a style="color: #fff" class="btn btn-{{ $candidato->statuscandidatovaga_id == 2 ? 'primary' : 'success' }} btn-flat" style="width:100%; padding: 15px; color: #666" href="/cadastro/curriculos/{{ $candidato->user->curriculo->id }}/visualizar"><i class="fa fa-eye"></i> Visualizar currículo</a></td>
			</tr>
			@endforeach
		@endif
    </tbody>
</table>
