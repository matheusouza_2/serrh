@extends('layouts.appinterno')

@section('content')
<section class="content-header">
  <h1>
    Veja as vagas abertas
    <small>
    </small>
</h1>  
</section>

<section class="content">
  {{-- @include('vaga.card_vagas') --}}
  @include('vaga.candidato_vagas')
</section>


@endsection

@section('scripts')
	<script src="{{ asset('js/hotsite/Hotsite.Vaga.js') }}"></script>
@endsection