<table class="table table-striped table-responsive">
    <thead>
        <tr>
          <th></th>
          <th>Vaga</th>
          <th>Empresa</th>
          <th>Área de atuação</th>
          <th>Data Fechamento</th>
          <th>Status da vaga</th>
          <th></th>
        </tr>
    </thead>
    <tbody>
    	@if(count($own_vaga) == 0)
			<tr><td colspan="10" style="text-align: center">Não foi encontrado nenhum registro</td></tr>
		@else
			@foreach($own_vaga as $vaga)
			<tr>
        @csrf
        <td><a href="/vagas/{{ $vaga->id }}/view" title="Ver detalhes" ><i class="fa fa-eye"></i></a></td>
				<td>{{$vaga->titulo_vaga}}</td>
				<td>{{ $vaga->show_company == 0?"Ser Rh":$vaga->empresa->nome_fantasia }}</td>
				<td>{{$vaga->actuation->descricao}}</td>
				<td>{{$vaga->data_fechamento == null ? 'Não informado' : $vaga->data_fechamento}}</td>
                <td>{{ $vaga->status->descricao }}</td>
				<td>
          @if(!$vaga->isCandidato)
            <button class="btn btn-primary btn-flat" style="width:100%; padding: 5px" onclick="hotsite.vaga.candidatar({{ $vaga->id }})"  {{ Auth::user()->curriculo == null ? 'disabled' : '' }} title="{{ Auth::user()->curriculo == null ? 'Necessário cadastrar o currículo' : '' }}">Candidatar-se</button>
          @else
          <button class="btn btn-success btn-flat" style="width:100%; padding: 5px" {{ Auth::user()->curriculo == null ? 'disabled' : '' }} title="{{ Auth::user()->curriculo == null ? 'Necessário cadastrar o currículo' : '' }}">Já se candidatou</button>
          @endif
                </td>

			</tr>
			@endforeach
		@endif
    </tbody>
</table>

<script>
  $(document).ready(function() {
    $('.pagination a').on('click', function (event) {
        event.preventDefault();
        if ( $(this).attr('href') != '#' ) {
            url = $(this).attr('href');
            page = url.split('page=')[1];
            $('#pagina').val(page);
            hotsite.vaga.filtrar(page);
        }
    });
  });
</script>
