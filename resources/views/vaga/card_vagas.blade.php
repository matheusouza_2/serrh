@if($own_vaga->count() == 0)
    <div class="text-center"><h3>Nenhuma vaga encontrada</h3></div>
@else
    @csrf

    @foreach($own_vaga->chunk(3) as $chunked)
    <div class="row">
        @foreach($chunked as $myVaga)
            <div class="col-md-4 vaga">
              <!-- Widget: user widget style 1 -->
              <div class="box box-widget widget-user-2">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-blue">
                  <!-- /.widget-user-image -->
                  <div class="widget-user-image">

                  </div>
                  <div class="widget-user-header-visualize">
                      <a href="/vagas/{{ $myVaga->id }}/view" title="Ver detalhes" ><i class="fa fa-eye"></i></a>
                  </div>
                  <h3 class="widget-user-username">{{ $myVaga->titulo_vaga }}</h3>
                  <h4 class="widget-user-desc">{{ $myVaga->actuation->descricao }}</h4>
                  <!-- <h5 class="widget-user-desc"><i class="fa fa-building"></i> {{ $myVaga->empresa->nome_fantasia }}</h5> -->
                  <h6 class="widget-user-desc"> Prazo da vaga: {{ $myVaga->data_fechamento }}</h6>
                </div>
                <div class="box-footer no-padding">
                  <ul class="nav nav-stacked">
                    <li>
                        <a href="#">Salário Inicial
                            <span class="pull-right badge bg-blue">
                                @if($myVaga->salario_inicial == null)
                                    Não definido
                                @else
                                    R$ {{ $myVaga->salario_inicial }},00
                                @endif
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="#">Quantidade
                            <span class="pull-right badge bg-aqua">
                                @if($myVaga->quantidade == null)
                                    Não definido
                                @else
                                    {{ $myVaga->quantidade }} vagas
                                @endif
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="#">Escolaridade Mínima
                            <span class="pull-right badge bg-green">
                                @if($myVaga->escolaridade_id == null)
                                    Não definido
                                @else
                                    {{ $myVaga->escolaridade->descricao }}
                                @endif
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="#">Idade
                            <span class="pull-right badge bg-red">
                                @if($myVaga->idade_min == null)
                                    @if($myVaga->idade_max == null)
                                        Não definido
                                    @else
                                        Até {{ $myVaga->idade_max }}
                                    @endif
                                @else
                                    @if($myVaga->idade_max == null)
                                        Mínimo de {{ $myVaga->idade_min }}
                                    @else
                                        De {{ $myVaga->idade_min }} até {{ $myVaga->idade_max }}
                                    @endif
                                @endif
                            </span>
                        </a>
                    </li>
                    @if(!$myVaga->isCandidato)
                    <li>
                        <button class="btn btn-primary btn-flat" style="width:100%; padding: 15px" onclick="hotsite.vaga.candidatar({{ $myVaga->id }})"  {{ Auth::user()->curriculo == null ? 'disabled' : '' }} title="{{ Auth::user()->curriculo == null ? 'Necessário cadastrar o currículo' : '' }}">Candidatar-se</a>
                    </li>
                    @endif
                  </ul>
                </div>
              </div>
              <!-- /.widget-user -->
            </div>
        @endforeach
        </div>
    @endforeach
@endif
