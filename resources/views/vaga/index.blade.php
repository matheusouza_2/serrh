@extends('layouts.appinterno')

@section('content')
<section class="content-header">
  <h1>
    Vaga
    <small>
        @if(Auth::user()->perfil->identificador == "Candidato")
            Minhas vagas
        @else
            Todas vagas
        @endif
    </small>
</h1>
</section>
<section class="content">
    @if(Auth::user()->perfil->identificador == "Candidato")
       @include('vaga.candidato_vagas')
       <div class="row">
            <form method="get" action="{{ url('/vagas/encontrar') }}">
                <div class="row" style="padding: 10px">
                    <div class="col-md-10 col-md-offset-1 text-center"><h3>Procurando por mais vagas?</h3></div>
                    <div class="col-md-10 col-md-offset-1">
                        <div class="input-group">
                            <input type="text" class="form-control" name="filter" id="filter" style="border-radius: 10px 0 0 10px;">
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-primary"  style="border-radius: 0 10px 10px 0;">
                                    <i class="fa fa-search"></i> Buscar novas vagas
                                </button>
                            </span>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    @else
        @csrf
        <div class="row">
            <div class="col-xs-12 col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Filtros</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Nome da vaga</label>
                                    <input type="text" class="form-control" name="name" id="name" />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Área de atuação</label>
                                    <input type="text" class="form-control" name="actuation" id="actuation" />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Empresas</label>
                                    <select class="form-control select2" name="empresas[]" id="empresas" multiple="multiple" data-placeholder="Filtre por empresas"
                                style="width: 100%;">
                                    @foreach($empresas as $empresa)
                                        <option value="{{$empresa->id}}">{{$empresa->nome_fantasia}} - {{ CustomFuncs::formatarCnpj($empresa->cnpj) }}</option>
                                    @endforeach
                                </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Status da Vaga</label>
                                    <select class="form-control select2" name="statusvaga" id="statusvaga" data-placeholder="Filtre por empresas"
                                style="width: 100%;">
                                    @foreach($allStatusVagas as $status)
                                        <option value="{{$status->id}}">{{$status->descricao}}</option>
                                    @endforeach
                                </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <div class="col-md-12">
                            <button class="btn btn-primary pull-right" onclick="hotsite.vaga.filtrar()">
                                <i class="fa fa-search"></i> Filtrar
                            </button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Lista de vagas</h3>
                        <div class="box-tools pull-right">
                            <a href="/vagas/new/edicao" class="btn btn-box-tool btn-box-tool-add"><i class="fa fa-plus"></i> Nova vaga
                            </a>
                        </div>
                    </div>
                    <div class="box-body no-padding">
                        <div class="overlay" style="display: block">
                            <i class="fa fa-refresh fa-spin"></i>
                        </div>
                        <div id="result"></div>
                    </div>
                    <div class="box-footer clearfix">
                        <!-- <ul class="pagination pagination-sm no-margin pull-right">
                            <li><a href="#">&laquo;</a></li>
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">&raquo;</a></li>
                        </ul> -->
                    </div>
                </div>
            </div>
        </div>
    @endif
</section>

@endsection


@section('scripts')
<script src="{{ asset('js/hotsite/Hotsite.Vaga.js') }}"></script>
<script>
    $('.select2').select2();
    @if(Auth::user()->perfil->identificador != "Candidato")
    hotsite.vaga.filtrar();
    @endif
</script>
@endsection
