<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Requisição de Pessoal</title>
        
        <style type="text/css">
            .linha_fina {
                padding-left: 60px;
                font-size: 24px;
                vertical-align: middle;
            }
                
            .linha_botton {
                padding: 5px;
                font-size: 16px;
                vertical-align: middle;
                border-bottom-width: thin;
                border-bottom-style: solid;
            }
            .footer {
                position: fixed; 
                bottom: -45px; 
                left: 0px; 
                margin-left: 3em;
                right: 0px;
                height: 50px;
            }
            tr.border_bottom td {
                border:1pt solid black;
            }
        </style>

        <style type="text/css">
            .right {
                text-align: right;
            }
            .left {
                text-align: left;
            }
            .center{
                text-align: center;
            }
        </style>
    </head>
        
    <body>
        
        <div style="">
        
            <table>
                <tr style="font-size:12px">
                    <td width=20% ><img src="img/img-requisicao.png" width="200" height="80" /></td>
                    
                    <td>
                        <div class="linha_fina" style="text-align:center;">
                            <b> Requisição de Pessoal</b> 
                        </div>
                    </td>
                </tr>
            </table>
        
            
            <table width="100%" border="1" cellpadding="4" cellspacing="0">
                <tr>
                    <td colspan="2" width="60%"><b>Cargo Solicitado: </b>{{ $vaga->titulo_vaga }}</td>
                    <td width="40%" colspan="2"><b>Nº da RP: </b>{{ $vaga->rp }}</td>
                </tr>
           
                <tr>
                    <td colspan="2" width="43%"><b>Data Emissão: </b>{{ date('d/m/Y', strtotime(\Carbon\Carbon::now())) }}</td>
                    <td width="18%" colspan="2"><b>Tipo: </b>{{ $vaga->tiporecrutamento }} </td>
                </tr>
                @if ($vaga->show_company == 1)
                <tr>
                    <td colspan="4"><b>Empresa: </b>{{ $empresa->razao_social }}</td>
                </tr>
                           
                <tr>
                    <td colspan="4"><b>Endereço: </b>{{ $empresa->fullAddress() }}</td>
                </tr>

                <tr>
                    <td colspan="2" ><b>Solicitante: </b> {{ $empresa->empresario['nome'] }}</td>
                    <td colspan="2" ><b>CNPJ: </b> {{ preg_replace("/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/", "\$1.\$2.\$3/\$4-\$5", $empresa->cnpj) }}</td>
                </tr>

                <tr>
                    <td colspan="2" ><b>Telefone: </b> {{ preg_replace("/(\d{2})(\d{4})(\d{4})/", "(\$1) \$2-\$3", $empresa->telefone) }}</td>
                    <td colspan="2" ><b>Email: </b> {{ $empresa->email }}</td>
                </tr>
                @endif
            </table>
            <br>
        
            <table width="100%" style="font-size: 12pt;" cellpadding="4" cellspacing="0">
                <tr style="background-color:#bfbfbf">
                    <td>
                        <b>DESCRIÇÃO DAS PRINCIPAIS ATRIBUIÇÕES / RESPONSABILIDADES DO CARGO/ O QUE IRÁ FAZER</b><br>
                    </td>
                </tr>

                <tr class="border_bottom">
                    <td>{!! $vaga->observacoes_vaga !!}</td>
                </tr>
            </table>
            
        
            <table width="100%" style="font-size: 12pt" cellpadding="4" cellspacing="0">
                <tr style="background-color: #bfbfbf">
                    <td><b>CARACTERISTICAS PESSOAIS/ PERFIL PROFISSIONAL / COMO  O CANDIDATO DEVE SER</b></td>
                </tr>

                <tr class="border_bottom">
                    <td>{!! $vaga->observacoes_candidato !!}</td>
                </tr>
            </table>
           <br>
                    
            <table width='100%' border='1' cellpadding='4' cellspacing='0'>
                
                <tr>
                    <td><b>Beneficios:</b></td>
                    <td><b>Horário de Trabalho:</b></td>
                </tr>

                <tr>
                    <td>{!! $vaga->beneficios !!}</td>
                    <td>{!! $vaga->horario_trabalho !!}</td>
                </tr>

                                 
            </table>
        
            <br>
        
            <table width="100%" cellpadding="4" cellspacing="0">
                <tr class="border_bottom">	
                    <td colspan="4" class="center"><b>DADOS DA VAGA</b></td>
                </tr>

                <tr class="border_bottom">
                    <td colspan="2"><b>O candidato pode ser de outra cidade: </b>{{ $vaga->only_city == 0?"Sim":"Não" }}</td>
                    <td colspan="2"><b>MASC/FEM: </b>{{ $vaga->marcacaosexo }}</td>
                </tr>

                <tr class="border_bottom">
                    <td colspan="4"><b>Qtd de Vagas:</b>{{ $vaga->quantidade }}</td>
                </tr>

                <tr class="border_bottom">
                    <td colspan="2"><b>Escolaridade: </b>{{ $vaga->escolaridade['descricao'] }}</td>
                    <td colspan="2"><b>Idade: </b>{{ $vaga->idade_max }}</td>
                </tr>

                <tr class="border_bottom">
                    <td colspan="4"><b>Salário Oferecido: </b>R$ {{ str_replace('.', ',', $vaga->salario_inicial) }}</td>
                </tr>
                
            </table>
            
            <br><br><br><br>
            <table width="100%" cellpadding="4" cellspacing="0">
                <tr>
                    <td width='20%' colspan="1" style='text-align:center;'></td>
                    <td width='60%' colspan="2" style='text-align:center;'><hr>Assinatura da empresa Contratante</td>
                    <td width='20%' colspan="1" style='text-align:center;'></td>
                </tr>
            </table>
        
            <br>
            
            <table width="100%" border="1" cellpadding="4" cellspacing="0">
                <tr style="background-color: #bfbfbf">
                    <td colspan="4"><b>PARA USO DA SER RH</b></td>
                </tr>
                <tr>
                    <td colspan="4" height="10%"><b>TESTES: </b></td>
                </tr>
                <tr>
                    <td colspan="4" height="10%"><b>OBSERVAÇÕES: </b></td>
                </tr>
            </table>
        </div>
    </body>
</html>