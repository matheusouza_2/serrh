@extends('layouts.appinterno')

@section('content')
 @csrf
<section class="content-header">
  <h1>
    {{$vaga->rp == null ? '' : $vaga->rp." - "}}{{ $vaga->titulo_vaga }}
    <small>
		{{ $vaga->actuation->descricao }}
    </small>
    <div class="pull-right">
        <a class="btn btn-flat btn-primary" href="{{ session()->previousUrl() }}"><i class="fa fa-arrow-left"></i> Voltar</a>
    </div>
</h1>
</section>

<section class="content">
	<div class="row">
	    <div class="col-xs-12 col-md-12">
	        <div class="box box-primary">
	        	<div class="box-header with-border">
	                <h3 class="box-title">Informações detalhadas da vaga</h3>
	                <div class="box-tools pull-right">
	                	@if($vaga->isCandidato)
	                	<span class="badge bg-green">Já candidato</span>
	                	@else
                        <button href="javascript:void(0)" class="btn btn-box-tool" onclick="hotsite.vaga.candidatar({{ $vaga->id }})" {{ Auth::user()->curriculo == null ? 'disabled' : '' }}><i class="fa fa-plus"></i> Candidatar-se
                        </button>
                        @endif
                    </div>
	            </div>
				<div class="box-body">
				    <div class="col-xs-12 col-md-12">

		                <div class="row">
		                	<div class="col-md-6">
		                		<dl class="dl-horizontal">
		                			<dt>Salário inicial</dt>
		                			<dd>
		                				@if($vaga->salario_inicial == null)
			                                Não definido
			                            @else
			                                R$ {{ number_format($vaga->salario_inicial, 2, ",", ".") }}
			                            @endif
	                        		</dd>

		                			<dt>Quantidade de vagas</dt>
		                			<dd>
		                				@if($vaga->quantidade == null)
			                                Não definido
			                            @else
			                                {{ $vaga->quantidade }}
			                            @endif
		                			</dd>

		                			<dt>Escolaridade Mínima</dt>
		                			<dd>
		                				@if($vaga->escolaridade_id == null)
			                                Não definido
			                            @else
			                                {{ $vaga->escolaridade->descricao }}
			                            @endif
		                			</dd>

		                			<dt>Idade solicitada</dt>
		                			<dd>
		                				@if($vaga->idade_min == null)
			                                @if($vaga->idade_max == null)
			                                    Não definido
			                                @else
			                                    Até {{ $vaga->idade_max }}
			                                @endif
			                            @else
			                                @if($vaga->idade_max == null)
			                                    Mínimo de {{ $vaga->idade_min }}
			                                @else
			                                    De {{ $vaga->idade_min }} até {{ $vaga->idade_max }}
			                                @endif
			                            @endif
		                			</dd>

		                			<dt>Fim das inscrições</dt>
		                			<dd>{{ $vaga->data_fechamento }}</dd>

		                			<dt>Previsão de admissão</dt>
													<dd>{{ $vaga->data_admissao }}</dd>
		                		</dl>
		                	</div>
		                	<div class="col-md-6">
		                		<dl class="dl-horizontal">
		                			<!-- <dt>Nome da empresa</dt>
		                			<dd>{{ $vaga->empresa->nome_fantasia }}</dd>-->

		                			{{-- <dt>Localização</dt>
		                			<dd>{{ $vaga->empresa->cidade }} - {{ $vaga->empresa->uf->nome }}</dd>

		                			<dt>Contato</dt>
		                			<dd>{{ $vaga->empresa->contato == null ? "Não há contato" : $vaga->empresa->contato}}</dd> --}}

<!-- 		                			<dt>Telefone contato</dt>
		                			<dd>{{ CustomFuncs::formatarTelefone($vaga->empresa->telefone) }}</dd>

		                			<dt>E-mail</dt>
		                			<dd>{{ $vaga->empresa->email }}</dd> -->
		                		</dl>
		                	</div>
		                </div>

						@if($vaga->observacoes_vaga != null)
						<div class="row">
						    <div class="col-xs-12 col-md-12">
						        <div class="box box-success">
						            <div class="box-header">
						                <h3 class="box-title">Observações da vaga</h3>
						            </div>
						            <div class="box-body">
						            	{!! $vaga->observacoes_vaga !!}
						            </div>
						        </div>
						    </div>
						</div>
						@endif

						@if($vaga->observacoes_candidato != null)
						<div class="row">
						    <div class="col-xs-12 col-md-12">
						        <div class="box box-success">
						            <div class="box-header with-border">
						                <h3 class="box-title">Observações do candidato</h3>
						            </div>
						            <div class="box-body">
						            	{!! $vaga->observacoes_candidato !!}
						            </div>
						        </div>
						    </div>
						</div>
						@endif

						
						<div class="row">
						    <div class="col-xs-6 col-md-6">
						        <div class="box box-success">
						            <div class="box-header">
						                <h3 class="box-title">Horário de trabalho</h3>
						            </div>
						            <div class="box-body">
													@if($vaga->horario_trabalho != null)
													{!! $vaga->horario_trabalho !!}
													@else
													Não informado
													@endif
						            </div>
						        </div>
								</div>
								<div class="col-xs-6 col-md-6">
						        <div class="box box-success">
						            <div class="box-header">
						                <h3 class="box-title">Benefícios</h3>
						            </div>
						            <div class="box-body">
													@if($vaga->beneficios != null)
													{!! $vaga->beneficios !!}
													@else
													Não informado
													@endif
						            </div>
						        </div>
						    </div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>


@endsection

@section('scripts')
	<script src="{{ asset('js/hotsite/Hotsite.Vaga.js') }}"></script>
@endsection
