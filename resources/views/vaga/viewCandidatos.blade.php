@extends('layouts.appinterno')

@section('content')
 @csrf
<section class="content-header">
  <h1>
    {{ $vaga->titulo_vaga }}
    <small>
		Candidatos
    </small>
    <div class="pull-right">
        <a class="btn btn-flat btn-primary" href="{{ url()->previous() }}"><i class="fa fa-arrow-left"></i> Voltar</a>
    </div>
</h1>
</section>

<section class="content">
  <div class="row">
    @csrf
    <input type="hidden" name="vaga_id" id="vaga_id" value="{{ $vaga->id }}" />
    <div class="col-xs-12 col-md-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Filtros</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Candidato</label>
                            <select class="form-control select2" name="candidatos" id="candidatos" data-placeholder="Escolha os candidatos" multiple="multiple"
                            style="width: 100%;">
                            <option value="">Todos</option>
                                @foreach($vaga->candidatos as $candidato)
                                    <option value="{{$candidato->user->id}}">{{$candidato->user->nome}} - {{CustomFuncs::formatarCpf($candidato->user->cpf)}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="col-md-12">
                    <button class="btn btn-primary pull-right" onclick="hotsite.vaga.filtrarCandidato()">
                        <i class="fa fa-search"></i> Filtrar
                    </button>
                </div>
            </div>

        </div>
    </div>
  </div>
	<div id="candidatosFilter">
   Nenhum candidato encontrado
  </div>
</section>


@endsection

@section('scripts')
	<script src="{{ asset('js/hotsite/Hotsite.Vaga.js') }}"></script>
  <script>
    $('.select2').select2({
      allowClear: true
    });
    hotsite.vaga.filtrarCandidato();
  </script>
@endsection
