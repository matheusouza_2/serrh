@extends('layouts.appinterno')

@section('content')
<section class="content-header">
  <h1>
    Vaga Integradas
    <small>
        Ao serrh 
    </small>
</h1>
</section>
<section class="content">
        @csrf
        <div class="row">
            <div class="col-xs-12 col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Filtros</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Nome da vaga</label>
                                    <input type="text" class="form-control" name="name" id="name" />
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>RP</label>
                                    <input type="text" id="rp" name="rp" class="form-control" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <div class="col-md-12">
                            <button class="btn btn-primary pull-right" onclick="hotsite.vaga.filtrarIntegrada()">
                                <i class="fa fa-search"></i> Filtrar
                            </button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="row" id="result">
            
        </div>
</section>

@endsection


@section('scripts')
<script src="{{ asset('js/hotsite/Hotsite.Vaga.js') }}"></script>
<script>
    $('.select2').select2();
    hotsite.vaga.filtrarIntegrada();
</script>
@endsection
