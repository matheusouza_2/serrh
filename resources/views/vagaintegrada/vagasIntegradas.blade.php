<div class="col-xs-12 col-md-6">
  <div class="box box-primary">
      <div class="box-header with-border">
          <h3 class="box-title">Vagas integradas</h3>
      </div>
      <div class="box-body no-padding">
          <div id="resultado">
            <table class="table table-striped table-responsive">
              <thead>
                  <tr>
                    <th style="width: 10px">RP</th>
                    <th>Vaga</th>
                    <th>Empresa</th>
                    <th>Área de atuação</th>
                    <th></th>
                  </tr>
              </thead>
              <tbody>
                @if(count($vagas) == 0)
                <tr><td colspan="10" style="text-align: center">Não foi encontrado nenhum registro</td></tr>
              @else
                @foreach($vagasIntegradas as $vaga)
                <tr>
                  <td>{{$vaga->rp}}</td>
                  <td>{{$vaga->titulo_vaga}}</td>
                  <td>{{$vaga->empresa->nome_fantasia}}</td>
                  <td>{{$vaga->actuation->descricao}}</td>
                  <td>
                    <i class="fa fa-times-circle-o text-danger" aria-hidden="true"  style="cursor: pointer" onClick="hotsite.vaga.toggleApiAvailable({{$vaga->id}})"></i>
                  </td>
                </tr>
                @endforeach
              @endif
              </tbody>
            </table>
          </div>
      </div>
      <div class="box-footer clearfix">
      </div>
  </div>
</div>

<div class="col-xs-12 col-md-6">
  <div class="box box-primary">
      <div class="box-header with-border">
          <h3 class="box-title">Lista de vagas</h3>
      </div>
      <div class="box-body no-padding">
          <div id="resultado">
            <table class="table table-striped table-responsive">
              <thead>
                  <tr>
                    <th style="width: 10px">RP</th>
                    <th>Vaga</th>
                    <th>Empresa</th>
                    <th>Área de atuação</th>
                    <th></th>
                  </tr>
              </thead>
              <tbody>
                @if(count($vagas) == 0)
                <tr><td colspan="10" style="text-align: center">Não foi encontrado nenhum registro</td></tr>
              @else
                @foreach($vagas as $vaga)
                <tr>
                  <td>{{$vaga->rp}}</td>
                  <td>{{$vaga->titulo_vaga}}</td>
                  <td>{{$vaga->empresa->nome_fantasia}}</td>
                  <td>{{$vaga->actuation->descricao}}</td>
                  <td>
                      <i class="fa fa-check-circle-o text-success" aria-hidden="true" style="cursor: pointer" onClick="hotsite.vaga.toggleApiAvailable({{$vaga->id}})"></i>
                  </td>
                </tr>
                @endforeach
              @endif
              </tbody>
            </table>

            <div class="pagination-area">
            {{ $vagas->links() }}
            </div>
          </div>
      </div>
      <div class="box-footer clearfix">
      </div>
  </div>
</div>

<script>
$(document).ready(function() {
  $('.pagination a').on('click', function (event) {
      event.preventDefault();
      if ( $(this).attr('href') != '#' ) {
          url = $(this).attr('href');
          page = url.split('page=')[1];
          $('#pagina').val(page);
          hotsite.vaga.filtrarIntegrada(page);
      }
  });
});
</script>
