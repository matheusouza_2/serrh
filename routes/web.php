<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/logout', 'Auth\LoginController@logout');
Route::get('/alterarsenha', 'UserController@alterarsenhaIndex');

Route::post('/alterarsenha', 'UserController@alterarsenha');
 
Route::get('/', 'HomeController@index')->middleware('auth');
Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');

Route::post('/cadastrar', 'Auth\LoginController@cadastro');

Route::get('/cadastro/curriculos', 'CurriculoController@index');
Route::post('/cadastro/curriculos/filtrar', 'CurriculoController@filtrar');
Route::get('/cadastro/curriculos/{id}/edicao', 'CurriculoController@formulario');
Route::get('/cadastro/curriculos/{id}/visualizar', 'CurriculoController@visualizar');
Route::get('/cadastro/curriculos/{id}/exportar', 'CurriculoController@exportar');
Route::get('/cadastro/curriculos/moreHistory', 'CurriculoController@novoHistorico');
Route::post('/cadastro/curriculos/salvar', 'CurriculoController@salvar');
Route::get('/cadastro/curriculos/{id}/loadCurriculo', 'CurriculoController@loadCurriculo');

Route::get('/cadastro/usuarios', 'UserController@index');
Route::post('/cadastro/users/filtrar', 'UserController@filtrar');
Route::post('/cadastro/users/salvar', 'UserController@salvar');
Route::post('/cadastro/users/formById', 'UserController@formById');
Route::get('/cadastro/usuarios/{id}/edicao', 'UserController@formulario');

Route::get('/cadastro/empresas', 'EmpresaController@index');
Route::post('/cadastro/empresas/filtrar', 'EmpresaController@filtrar');
Route::post('/cadastro/empresas/salvar', 'EmpresaController@salvar');
Route::post('/cadastro/empresas/formById', 'EmpresaController@formById');
Route::get('/cadastro/empresa/{id}/edicao', 'EmpresaController@formulario');

Route::get('/cadastro/actuations', 'ActuationController@index');
Route::post('/cadastro/actuations/filtrar', 'ActuationController@filtrar');
Route::get('/cadastro/actuations/{id}/quickModal', 'ActuationController@quickModal');
Route::post('/cadastro/actuations/viability', 'ActuationController@viability');
Route::post('/cadastro/actuations/salvar', 'ActuationController@salvar');
Route::get('/cadastro/actuations/{id}/changeStatus', 'ActuationController@changeStatus');
Route::get('/cadastro/actuations/{id}/deletar', 'ActuationController@deletar');

Route::get('/cep/{cep}/encontracep', 'CepController@encontraCep');

Route::get('/vagas', 'VagaController@index');
Route::get('/vagas/{id}/edicao', 'VagaController@formulario');
Route::post('/vagas/salvar', 'VagaController@salvar');
Route::post('/vagas/filtrar', 'VagaController@filtrar');
Route::post('/vagas/filtrarCandidato', 'VagaController@filtrarCandidatos');
Route::get('vagas/{id}/view', 'VagaController@view');
Route::get('/vagas/encontrar', 'VagaController@encontrar');
Route::post('/vagas/candidatar', 'OcupacaovagaController@candidatar');
Route::get('vagas/{id}/candidatos', 'VagaController@viewCandidatos');

Route::get('/vagas/requisicao-pessoal/vaga/{vaga}', 'VagaController@emitirRequisicao')->name('vagas_requisicao');

Route::get('/vagasintegradas', 'VagaController@indexVagaIntegrada');
Route::post('/vagasintegradas/filtrar', 'VagaController@filtrarIntegrada');
Route::post('/vagasintegradas/toggleApiAvailable', 'VagaController@toggleApiAvailable');

Route::get('/teste/{vagas}', 'VagaController@gerarBoleto');
